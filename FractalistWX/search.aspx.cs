﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Tool;

namespace FractalistWX
{
    public partial class search : System.Web.UI.Page
    {
        public string searchtxt,typestr;
        public int type = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            searchtxt = Tools.SafeSql(Tools.CheckVar(Request.Form["searchtxt"], ""),0);
            if (!string.IsNullOrEmpty(searchtxt))
            {
                if (!Tools.QuickValidate(@"^(\w|[\u4E00-\u9FA5])*$", searchtxt))
                {
                    Response.Write("<script language='javascript'>alert('内容不合法');window.history.go(-1);</script>");
                }
            }
            type = Tools.CheckVar(Request.Form["type"], 0);
            if (type == 0)
            {
                typestr = "信息";
            }
            else if (type == 1)
            {
                typestr = "新闻";
            }
            else if (type == 2)
            {
                typestr = "图片";
            }
            else if (type == 3)
            {
                typestr = "视频";
            }
        }
    }
}