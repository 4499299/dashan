﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Text;
using FractalistWX.Tool;
using System.IO;
using FractalistWX.Dal;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace FractalistWX
{
    public partial class Ajax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string action = Request["action"];
                switch (action)
                {
                    case "review":
                        Review();
                        break;
                    case "jobjoin":
                        JobJoin();
                        break;
                    case "mjobfrom":
                        MJobJoin();
                        break;
                    case "joinfrom":
                        JoinFrom();
                        break;
                    case "SearchList":
                        SearchList();
                        break;
                    case "mjoinfrom":
                        MJoinFrom();
                        break;
                }
            }
        }
        private void SearchList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request.Form["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request.Form["pagesize"], 10);
            int type = Tools.CheckVar(Request.Form["type"], 0);
            string searchtxt = Tools.SafeSql(Tools.CheckVar(Request.Form["searchtxt"], ""), 0);
            List<string> list = new List<string>();
            StringBuilder strSql = new StringBuilder();
            if (string.IsNullOrEmpty(searchtxt))
            {
                strHtml = "{";
                strHtml += "\"total\":\"0\",\"totalpage\":\"0\",\"list\":[]}";               
            }
            else
            {
                if (type == 0)
                {
                    int count = 0;
                    StringBuilder strSqlcount = new StringBuilder();
                    strSql.Append(@"select * from (select id,title,pic,islink,linkurl,'' as video,1 as type from wechat_news where newstype=0 UNION select id,title,pic,islink,linkurl,'' as video,2 as type from wechat_pic where pictype=0 UNION select id,title,pic,islink,linkurl,video,3 as type from wechat_video where videotype=0
) as aa ");
                    strSql.Append(" where  title like @title  order by id desc  limit @pageindex, @pagesize");
                    MySqlParameter[] parameters = {				
                  new MySqlParameter("@pageindex", MySqlDbType.Int32),
                  new MySqlParameter("@pagesize", MySqlDbType.Int32),
                  new MySqlParameter("@title", MySqlDbType.VarChar,200)
                };
                    parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
                    parameters[1].Value = pagesize;
                    parameters[2].Value = "%" + searchtxt + "%";

                    strSqlcount.Append(@"select count(id) from  (select id,title,pic,islink,linkurl,'' as video,1 as type from wechat_news where newstype=0 UNION select id,title,pic,islink,linkurl,'' as video,2 as type from wechat_pic where pictype=0 UNION select id,title,pic,islink,linkurl,video,3 as type from wechat_video where videotype=0
) as aa  ");
                    strSqlcount.Append(" where  title  like @title ");
                    MySqlParameter[] parameterscount = {
				  new MySqlParameter("@title", MySqlDbType.VarChar,2000),
                };
                    parameterscount[0].Value = "%" + searchtxt + "%";

                    count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
                    int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
                    strHtml = "{";
                    strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
                    strHtml += "[";
                    DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strHtml += "{\"type\":" + dt.Rows[i]["type"].ToString() + ",\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\",\"video\":\"" + dt.Rows[i]["video"].ToString() + "\"},";
                    }
                    strHtml = strHtml.TrimEnd(',');
                    strHtml += "]}";
                }
                if (type == 1)
                {
                    int count = 0;
                    StringBuilder strSqlcount = new StringBuilder();
                    strSql.Append("select id,title,pic,islink,linkurl,'' as video,1 as type from wechat_news");
                    strSql.Append(" where newstype=0 and title like @title  order by id desc  limit @pageindex, @pagesize");
                    MySqlParameter[] parameters = {				
                  new MySqlParameter("@pageindex", MySqlDbType.Int32),
                  new MySqlParameter("@pagesize", MySqlDbType.Int32),
                  new MySqlParameter("@title", MySqlDbType.VarChar,200)
                };
                    parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
                    parameters[1].Value = pagesize;
                    parameters[2].Value = "%" + searchtxt + "%";

                    strSqlcount.Append("select count(id) from wechat_news  ");
                    strSqlcount.Append(" where  title  like @title  and  newstype=0 ");
                    MySqlParameter[] parameterscount = {
				  new MySqlParameter("@title", MySqlDbType.VarChar,2000),
                };
                    parameterscount[0].Value = "%" + searchtxt + "%";

                    count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
                    int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
                    strHtml = "{";
                    strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
                    strHtml += "[";
                    DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strHtml += "{\"type\":1,\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\",\"video\":\"" + dt.Rows[i]["video"].ToString() + "\"},";
                    }
                    strHtml = strHtml.TrimEnd(',');
                    strHtml += "]}";
                }
                if (type == 2)
                {
                    int count = 0;
                    StringBuilder strSqlcount = new StringBuilder();
                    strSql.Append("select id,title,pic,islink,linkurl,'' as video,2 as type from wechat_pic ");
                    strSql.Append(" where pictype=0 and title like @title  order by id desc  limit @pageindex, @pagesize");
                    MySqlParameter[] parameters = {				
                  new MySqlParameter("@pageindex", MySqlDbType.Int32),
                  new MySqlParameter("@pagesize", MySqlDbType.Int32),
                  new MySqlParameter("@title", MySqlDbType.VarChar,200)
                };
                    parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
                    parameters[1].Value = pagesize;
                    parameters[2].Value = "%" + searchtxt + "%";

                    strSqlcount.Append("select count(id) from wechat_pic  ");
                    strSqlcount.Append(" where  title  like @title  and  pictype=0 ");
                    MySqlParameter[] parameterscount = {
				  new MySqlParameter("@title", MySqlDbType.VarChar,2000),
                };
                    parameterscount[0].Value = "%" + searchtxt + "%";

                    count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
                    int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
                    strHtml = "{";
                    strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
                    strHtml += "[";
                    DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strHtml += "{\"type\":2,\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\",\"video\":\"" + dt.Rows[i]["video"].ToString() + "\"},";
                    }
                    strHtml = strHtml.TrimEnd(',');
                    strHtml += "]}";
                }
                if (type == 3)
                {
                    int count = 0;
                    StringBuilder strSqlcount = new StringBuilder();
                    strSql.Append("select id,title,pic,islink,linkurl,video,3 as type from wechat_video ");
                    strSql.Append(" where videotype=0 and title like @title  order by id desc  limit @pageindex, @pagesize");
                    MySqlParameter[] parameters = {				
                  new MySqlParameter("@pageindex", MySqlDbType.Int32),
                  new MySqlParameter("@pagesize", MySqlDbType.Int32),
                  new MySqlParameter("@title", MySqlDbType.VarChar,200)
                };
                    parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
                    parameters[1].Value = pagesize;
                    parameters[2].Value = "%" + searchtxt + "%";

                    strSqlcount.Append("select count(id) from wechat_video  ");
                    strSqlcount.Append(" where  title  like @title  and  videotype=0 ");
                    MySqlParameter[] parameterscount = {
				  new MySqlParameter("@title", MySqlDbType.VarChar,2000),
                };
                    parameterscount[0].Value = "%" + searchtxt + "%";

                    count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
                    int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
                    strHtml = "{";
                    strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
                    strHtml += "[";
                    DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strHtml += "{\"type\":3,\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\",\"video\":\"" + dt.Rows[i]["video"].ToString() + "\"},";
                    }
                    strHtml = strHtml.TrimEnd(',');
                    strHtml += "]}";
                }
            }
            Response.Write(strHtml);
            Response.End();
        }
        private void Review()
        {
            int reviewtype = Tool.Tools.CheckVar(Request["reviewtype"], -1);
            string nickname =Tools.SafeSql(Tool.Tools.CheckVar(Request["nickname"], ""),0);
            string mobile = Tools.SafeSql(Tool.Tools.CheckVar(Request["mobile"], ""),0);
            string email = Tools.SafeSql(Tool.Tools.CheckVar(Request["email"], ""),0);
            string content = Tools.SafeSql(Tool.Tools.CheckVar(Request["content"], ""),0);
            int booktype = Tool.Tools.CheckVar(Request["booktype"], 0);
            string code = Tools.SafeSql(Tool.Tools.CheckVar(Request["code"], ""), 0);
            if (reviewtype==-1)
            {
                Response.Write("请选择留言类型");
                Response.End();
            }
            if (string.IsNullOrEmpty(nickname))
            {
                Response.Write("请输入昵称");
                Response.End();
            }
            if (booktype == 0)
            {
                if (string.IsNullOrEmpty(email))
                {
                    Response.Write("请输入邮箱");
                    Response.End();
                }
            }
            if (string.IsNullOrEmpty(content))
            {
                Response.Write("请输入留言内容");
                Response.End();
            }
            if (content.Length > 2000)
            {
                Response.Write("请输入2000字以内的留言内容");
                Response.End();
            }
            if (string.IsNullOrEmpty(code))
            {
                Response.Write("请输入验证码");
                Response.End();
            }
            if (MySession.ValidateCode == null || code != MySession.ValidateCode as string)
            {
                Response.Write("验证码错误或已过期");
                Response.End();
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO wechat_review (");
            sql.Append("reviewtype,nickname,mobile,email,content,booktype");
            sql.Append(") VALUES(");
            sql.Append("@reviewtype,@nickname,@mobile,@email,@content,@booktype)");
            MySqlParameter[] parameters = {
                                            new MySqlParameter("@reviewtype", MySqlDbType.Int32){Value=Tools.SafeSql(reviewtype.ToString())},
                                            new MySqlParameter("@nickname", MySqlDbType.VarChar,50){Value=Tools.SafeSql(nickname)},
                                            new MySqlParameter("@mobile", MySqlDbType.VarChar){Value=Tools.SafeSql(mobile)},
                                            new MySqlParameter("@email", MySqlDbType.VarChar,50){Value=Tools.SafeSql(email)},
                                            new MySqlParameter("@content", MySqlDbType.VarChar,2000){Value=Tools.SafeSql(content)},
                                            new MySqlParameter("@booktype", MySqlDbType.Int32){Value=booktype}						                       
                                          };
            if (Dal.SQLTool.ExecuteNonQuery(sql.ToString(), parameters)>0)
            {
                Response.Write("ok");
                Response.End();
            }
            else
            {
                Response.Write("no");
                Response.End();
            }
        }
        private void JobJoin()
        {
            string username = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["username"], ""),0);
            string mobile = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["mobile"], ""), 0);
            string email = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["email"], ""), 0);
            string filepath = UpFile();
            string code = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["code"], ""), 0);
            if (string.IsNullOrEmpty(username))
            {
                Response.Write("{\"msg\":\"请输入姓名\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(email))
            {
                Response.Write("{\"msg\":\"请输入邮箱\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(filepath))
            {
                Response.Write("{\"msg\":\"请上传简历\"}");
                Response.End();
            }
            if (filepath == "0")
            {
                Response.Write("{\"msg\":\"请上传不超过2M,word类型的文件\"}");
                Response.End();
            }
            if (filepath == "1")
            {
                Response.Write("{\"msg\":\"请上传不超过2M,word类型的文件\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(code))
            {
                Response.Write("{\"msg\":\"请输入验证码\"}");
                Response.End();
            }
            if (Session["validateRndCode"] == null || code != Session["validateRndCode"] as string)
            {
                Response.Write("{\"msg\":\"验证码错误或已过期\"}");
                Response.End();
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO wechat_jobjoin (");
            sql.Append("username,mobile,email,filepath");
            sql.Append(") VALUES(");
            sql.Append("@username,@mobile,@email,@filepath)");
            MySqlParameter[] parameters = {
                                            new MySqlParameter("@username", MySqlDbType.VarChar,50){Value=Tools.SafeSql(username)},
                                            new MySqlParameter("@mobile", MySqlDbType.VarChar){Value=Tools.SafeSql(mobile)},
                                            new MySqlParameter("@email", MySqlDbType.VarChar,50){Value=Tools.SafeSql(email)},
                                            new MySqlParameter("@filepath", MySqlDbType.VarChar,50){Value=Tools.SafeSql(filepath)}                  
                                          };
            if (Dal.SQLTool.ExecuteNonQuery(sql.ToString(), parameters) > 0)
            {
                Response.Write("{\"msg\":\"ok\"}");
                Response.End();
            }
            else
            {
                Response.Write("{\"msg\":\"no\"}");
                Response.End();
            }
        }
        private void MJobJoin()
        {
            string username = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["username"], ""), 0);
            string mobile = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["mobile"], ""), 0);
            string email = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["email"], ""), 0);
            string code = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["code"], ""), 0);
            if (string.IsNullOrEmpty(username))
            {
                Response.Write("{\"msg\":\"请输入姓名\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(email))
            {
                Response.Write("{\"msg\":\"请输入邮箱\"}");
                Response.End();
            }          
            if (string.IsNullOrEmpty(code))
            {
                Response.Write("{\"msg\":\"请输入验证码\"}");
                Response.End();
            }
            if (Session["validateRndCode"] == null || code != Session["validateRndCode"] as string)
            {
                Response.Write("{\"msg\":\"验证码错误或已过期\"}");
                Response.End();
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO wechat_jobjoin (");
            sql.Append("username,mobile,email");
            sql.Append(") VALUES(");
            sql.Append("@username,@mobile,@email)");
            MySqlParameter[] parameters = {
                                            new MySqlParameter("@username", MySqlDbType.VarChar,50){Value=Tools.SafeSql(username)},
                                            new MySqlParameter("@mobile", MySqlDbType.VarChar){Value=Tools.SafeSql(mobile)},
                                            new MySqlParameter("@email", MySqlDbType.VarChar,50){Value=Tools.SafeSql(email)}    
                                          };
            if (Dal.SQLTool.ExecuteNonQuery(sql.ToString(), parameters) > 0)
            {
                Response.Write("{\"msg\":\"ok\"}");
                Response.End();
            }
            else
            {
                Response.Write("{\"msg\":\"no\"}");
                Response.End();
            }
        }
        private void JoinFrom()
        {
            string username = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["username"], ""), 0);
            string mobile = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["mobile"], ""), 0);
            string email = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["email"], ""), 0);
            string filepath = UpFile();
            string code = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["code"], ""), 0);
            if (string.IsNullOrEmpty(username))
            {
                Response.Write("{\"msg\":\"请输入姓名\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(email))
            {
                Response.Write("{\"msg\":\"请输入邮箱\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(filepath))
            {
                Response.Write("{\"msg\":\"请上传不超过2M,word类型的文件\"}");
                Response.End();
            }
            if (filepath == "0")
            {
                Response.Write("{\"msg\":\"请上传不超过2M,word类型的文件\"}");
                Response.End();
            }
            if (filepath == "1")
            {
                Response.Write("{\"msg\":\"请上传不超过2M,word类型的文件\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(code))
            {
                Response.Write("{\"msg\":\"请输入验证码\"}");
                Response.End();
            }
            if (Session["validateRndCode"] == null || code != Session["validateRndCode"] as string)
            {
                Response.Write("{\"msg\":\"验证码错误或已过期\"}");
                Response.End();
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO wechat_joinfrom (");
            sql.Append("username,mobile,email,filepath");
            sql.Append(") VALUES(");
            sql.Append("@username,@mobile,@email,@filepath)");
            MySqlParameter[] parameters = {
                                            new MySqlParameter("@username", MySqlDbType.VarChar,50){Value=Tools.SafeSql(username)},
                                            new MySqlParameter("@mobile", MySqlDbType.VarChar){Value=Tools.SafeSql(mobile)},
                                            new MySqlParameter("@email", MySqlDbType.VarChar,50){Value=Tools.SafeSql(email)},
                                            new MySqlParameter("@filepath", MySqlDbType.VarChar,50){Value=Tools.SafeSql(filepath)}                  
                                          };
            if (Dal.SQLTool.ExecuteNonQuery(sql.ToString(), parameters) > 0)
            {
                Response.Write("{\"msg\":\"ok\"}");
                Response.End();
            }
            else
            {
                Response.Write("{\"msg\":\"no\"}");
                Response.End();
            }
        }
        public void MJoinFrom()
        {
            string username = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["username"], ""), 0);
            string mobile = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["mobile"], ""), 0);
            string email = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["email"], ""), 0);
            string code = Tools.SafeSql(Tool.Tools.CheckVar(Request.Form["code"], ""), 0);
            if (string.IsNullOrEmpty(username))
            {
                Response.Write("{\"msg\":\"请输入姓名\"}");
                Response.End();
            }
            if (string.IsNullOrEmpty(email))
            {
                Response.Write("{\"msg\":\"请输入邮箱\"}");
                Response.End();
            }           
            if (string.IsNullOrEmpty(code))
            {
                Response.Write("{\"msg\":\"请输入验证码\"}");
                Response.End();
            }
            if (Session["validateRndCode"] == null || code != Session["validateRndCode"] as string)
            {
                Response.Write("{\"msg\":\"验证码错误或已过期\"}");
                Response.End();
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO wechat_joinfrom (");
            sql.Append("username,mobile,email");
            sql.Append(") VALUES(");
            sql.Append("@username,@mobile,@email)");
            MySqlParameter[] parameters = {
                                            new MySqlParameter("@username", MySqlDbType.VarChar,50){Value=Tools.SafeSql(username)},
                                            new MySqlParameter("@mobile", MySqlDbType.VarChar){Value=Tools.SafeSql(mobile)},
                                            new MySqlParameter("@email", MySqlDbType.VarChar,50){Value=Tools.SafeSql(email)}                 
                                          };
            if (Dal.SQLTool.ExecuteNonQuery(sql.ToString(), parameters) > 0)
            {
                Response.Write("{\"msg\":\"ok\"}");
                Response.End();
            }
            else
            {
                Response.Write("{\"msg\":\"no\"}");
                Response.End();
            }
        }
        public string UpFile()
        {
            HttpFileCollection files = Request.Files;
            try
            {
                string fileName = files[0].FileName;
                if (string.IsNullOrEmpty(fileName))
                {
                    return "";
                }

                //获得绝对路径
                string applicationPath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath) + "/UpFiles/source/";
                if (!Directory.Exists(applicationPath))
                {
                    Directory.CreateDirectory(applicationPath);
                }
                string upfileName = files[0].FileName;
                string ext = fileName.Substring(upfileName.LastIndexOf("."), upfileName.Length - upfileName.LastIndexOf(".")).ToLower();
                if (ext == ".doc" || ext == ".docx")
                {
                    //设置每次上传的图片文件名的相对路径
                    string TimeStr = System.DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    string AllFileName = TimeStr + files[0].ContentLength.ToString() + upfileName.Substring(upfileName.LastIndexOf("."), upfileName.Length - upfileName.LastIndexOf("."));

                    AllFileName = AllFileName.ToLower();
                    string fileTempPath = applicationPath + AllFileName;
                    string laststr = upfileName.Substring(upfileName.LastIndexOf("."), upfileName.Length - upfileName.LastIndexOf(".")).ToLower();
                    int fileLength = files[0].ContentLength / 1024 / 1024;//获取上传文件大小，以字节为单位
                    if (fileLength > 2)
                    {
                        return "0";
                    }
                    else
                    {
                      
                        files[0].SaveAs(fileTempPath);
                        return AllFileName;
                    }
                }
                else
                {
                    return "1";
                }
            }
            catch
            {
                return "";
            }
        }
    }
}