﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Feedbackmanage.aspx.cs" Inherits="FractalistWX.Manage.Feedback" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">客户留言列表</span></div>
        <div style="height:10px;border-bottom: 2px solid #196fc0;"></div>   
         <div style="width:810px; margin-top:15px; text-align:right;">
            <span>留言来源：</span>
            <span>
                <select id="tb_type" name="tb_type " style="width:120px; font-size:12px; height:44px;">
                    <option value="" >全部</option>
                    <option value="0">pc平台</option>
                    <option value="1">手机平台</option>
                </select>
            </span>  
             <span>留言类型：</span>
            <span>
                <select id="tb_wtype" name="tb_wtype " style="width:120px; font-size:12px; height:44px;">
                ````<option value="" >全部</option>
                    <option value="0" >咨询</option>
                    <option value="1">建议</option>
                     <option value="2">投诉</option>
                </select>
            </span>         
         </div>          
        <div class="clear imgTextListBoxAddpic" style="padding-top:0px;">
            <ul class="bookTextList fl clear">                 
            </ul>           
        </div>   
        <div style="clear:both;"></div>
        <div class="clear imgPageBox" style="width:600px; margin-top:30px;">
		    <a class="c222  imgPageR fr" href="javascript:;" style="margin-top:3px;"><img src="images/selbtn.png" width="100%" /></a>
		    <div class="boe7e7eb  fr"><input class="db inpNoBg imgPageMinp" type="number" style="width:60px;" /></div>
		    <div class="imgPageL fr">
			    <a href="javascript:;" class="c222 fl  iPagePrev"><img src="images/left.png" style="margin-top:10px;" /></a>
			    <p class="imgPageInfo fl">1 / 1</p>
			    <a href="javascript:;" class="c222 fl  iPageNext"><img src="images/right.png" style="margin-top:10px;" /></a>
		    </div>
	    </div>
    </div>
    <div id="divDisable" style="display: none; opacity:0.5; -moz-opacity: 0.5; z-index: 1099;position: absolute; left: 0px; top: 0px; filter: alpha(opacity=60); background-color: #000; width: 100%; height:100%;">
     </div>
     <div id="addImgTextTk" class="a imgTk addImgTextTk bgfbfbfb imgTkex dn" style="z-index: 1200;width:584px;">
	    <h2 class="bg0b82c7 imgTkH2" style="color:#fff;width:538px;">留言查看</h2>           
	    <div class="clear imgTextListBox" style="width:584px; height:285px;">
		       <table width="90%" border="0" cellspacing="5" cellpadding="5" style="font-size:12px;line-height:25px; margin:0 auto; font-size:14px;">
                    <tr style="height:65px;">
                        <td>留言来源：</td>
                        <td>                        
                        <asp:DropDownList ID="booktypeTB"  runat="server" Width="120px" Font-Size="12px" Enabled="false"  Height="35px"><asp:ListItem Text="请选择" Value=""></asp:ListItem><asp:ListItem Text="PC" Value="0"></asp:ListItem><asp:ListItem Text="MO" Value="1"></asp:ListItem></asp:DropDownList></td>
                        <td>留言类型：</td>
                        <td>
                        <asp:DropDownList ID="reviewtypeTB"  runat="server" Width="120px" Enabled="false"  Font-Size="12px" Height="35px"><asp:ListItem Text="请选择" Value=""></asp:ListItem><asp:ListItem Text="咨询" Value="0"></asp:ListItem><asp:ListItem Text="建议" Value="1"></asp:ListItem><asp:ListItem Text="投诉" Value="2"></asp:ListItem></asp:DropDownList>
                        </td>
                    </tr>                   
                    <tr  style="height:65px;">
                        <td>昵称：</td>
                        <td><asp:TextBox ID="nicknameTB" TextMode="SingleLine" runat="server" Width="120px" Enabled="false"  Font-Size="12px" Height="35px"></asp:TextBox></td>
                        <td>手机号码：</td>
                        <td><asp:TextBox ID="mobileTB" TextMode="SingleLine" runat="server" Width="120px" Enabled="false"  Font-Size="12px" Height="35px"></asp:TextBox></td>
                    </tr>
                    <tr  style="height:65px;">
                        <td>邮箱地址</td>
                        <td><asp:TextBox ID="emailTB" TextMode="SingleLine" runat="server" Width="120px" Enabled="false"  Font-Size="12px" Height="35px"></asp:TextBox></td>
                    </tr>
                    <tr  style="height:65px;">
                        <td>留言内容：</td>
                        <td colspan="3"><asp:TextBox ID="contentTB" TextMode="MultiLine" runat="server" Enabled="false" Width="396px" Font-Size="12px" Height="65px"></asp:TextBox></td>
                    </tr>                   
                </table>
	    </div>      
        <div class="clear" style="width:98%; margin:0 auto; text-align:center;">
            <hr style="height:1px;border:none;border-top:1px solid #f2f2f2;" />    
        </div>   
	    <div class="tac tkBtn">
		    <a href="javascript:;" class="cfff tkBtnL dib"><img src="images/okbtn.png" width="184" height="47" id="closebtn" /></a>		         
	    </div>
    </div>
    </form>
</body>
</html>
<script>
    var ispost = false;
    var pictype = $("#tb_type").val();
    var tb_wtype = $("#tb_wtype").val();
    var pageindex = 1, totalpage = 0;
    PageData();
    $("#closebtn").bind("click",function(){
        $("#addImgTextTk").hide();
        $("#divDisable").hide();
    })
    $(".showbtn").live("click", function () {
        var state = 0;
        var pid = $(this).parent().parent().attr("value");       
        if ($(this).find("img").attr("src") == "images/unshowhui.png") {          
            $(this).find("img").attr("src", "images/showhui.png");
            state = 1;
        }
        else {
            $(this).find("img").attr("src", "images/unshowhui.png");
            state = 0;
        }
        if (!ispost) {
            ispost = true;
            var Param = $.param({
                pid: pid,
                state:state
            });
            $.ajax({ url: "Ajax.aspx?action=EditstateBook&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;                    
                }
            });
        }
    })
    $("span[name='pic_del']").live("click", function () {
        var pid = $(this).parent().parent().attr("value");
        if (confirm('确定要删除吗？')) {           
            var Param = $.param({
                pid: pid
            });
            if (!ispost) {
                ispost = true;
                $.ajax({ url: "Ajax.aspx?action=DelBook&t=" + Math.random(),
                    type: "POST",
                    data: Param,
                    dataType: "json",
                    processData: false,
                    error: function (datas) {
                        ispost = false;
                    },
                    success: function (datas) {
                        ispost = false;
                        PageData();
                    }
                });
            }
        }
    })
    //$(document).on("click", "span[name='pic_edit']", function () { alert("Goodbye!"); });  
    $("span[name='pic_edit']").live("click", function () {
        if ($(".recontentDiv").is(':hidden')) {
            $(this).parent().parent().find(".bookleft");
            $(".recontentDiv").slideUp();
            $(this).parent().parent().find(".recontentDiv").slideDown();
        }
        //        var pid = $(this).parent().parent().attr("value");
        //        var Param = $.param({
        //            pid: pid
        //        });
        //        if (!ispost) {
        //            ispost = true;
        //            $.ajax({ url: "Ajax.aspx?action=ShowBook&t=" + Math.random(),
        //                type: "POST",
        //                data: Param,
        //                dataType: "json",
        //                processData: false,
        //                error: function (datas) {
        //                    ispost = false;
        //                },
        //                success: function (datas) {
        //                    ispost = false;
        //                    $("#<%=booktypeTB.ClientID%>").val(datas.booktype);
        //                    $("#<%=reviewtypeTB.ClientID%>").val(datas.reviewtype);
        //                    $("#<%=nicknameTB.ClientID%>").val(datas.nickname);
        //                    $("#<%=mobileTB.ClientID%>").val(datas.mobile);
        //                    $("#<%=emailTB.ClientID%>").val(datas.email);                    
        //                    $("#<%=contentTB.ClientID%>").val(datas.content);
        //                    $("#addImgTextTk").show();
        //                    $("#divDisable").show();
        //                }
        //            });
        //        }
    })
    $(".cannelbtn").live("click", function () {
        $(".bookleft1").removeClass().addClass("bookleft");
        $(this).parent().parent().parent().slideUp();
        $(this).parent().parent().parent().parent().find(".showbtn").find("img").attr("src", "images/unshowhui.png");
        $("#form1")[0].reset();
    })

    $(".okbtn").live("click", function () {
        if (!ispost) {
            ispost = true;
            var msgtxt = $(this).parent().parent().find(".msgtxt");
            var pid = $(this).parent().parent().parent().parent().attr("value");
            var recontent = $(this).parent().parent().parent().find("textarea").val();
            if (recontent == "") {
                msgtxt.text("请回复");
                ispost = false;
                return;
            }
            var Param = $.param({
                recontent: recontent,
                pid: pid
            });
            $.ajax({ url: "Ajax.aspx?action=BookRecontent&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    msgtxt.text("提交成功");
                    setTimeout(function () {
                        msgtxt.text("");                        
                    }, 1000);
                }
            });
        }
    })
    $("#tb_type").bind("change", function () {
        pictype = $("#tb_type").val();
        tb_wtype = $("#tb_wtype").val();
        pageindex = 1, totalpage = 0;
        PageData();
    })
    $("#tb_wtype").bind("change", function () {
        pictype = $("#tb_type").val();
        tb_wtype = $("#tb_wtype").val();
        pageindex = 1, totalpage = 0;
        PageData();
    })
    function PageData() {
        var Param = $.param({
            wtype: pictype,
            reviewtype:tb_wtype,
            pageindex: pageindex
        });
        if (!ispost) {           
            ispost = true;
            $.ajax({ url: "Ajax.aspx?action=BookList&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;                   
                },
                success: function (datas) {                   
                    ispost = false;
                    var list = datas.list;
                    totalpage = datas.totalpage;
                    $('.bookTextList').html("");
                    if (list != null && list.length > 0) {
                        var htmldesc = "";
                        for (var j = 0; j < list.length; j++) {
                            var hui = "";
                            console.log(list[j].state);
                            if (list[j].state == 0) {
                                hui = '<img src="images/unshowhui.png" style="vertical-align: middle;" /></span>';
                            } else {
                                hui = '<img src="images/showhui.png" style="vertical-align: middle;" /></span>';
                            }
                            htmldesc += '<li style="clear: both;" value="' + list[j].pid + '">' +
                                        '<div style=" text-align:left;"><table width="60%"><tr><td>姓名：' + list[j].nickname + '</td><td>手机号:' + list[j].mobile + '</td><td>邮箱：' + list[j].email + '</td><td>发布时间：' + list[j].ntime + '</td></tr></table></div>' +
                                        '<div class="bookleft">' +
                                        '<table cellpadding="0" cellspacing="0">' +
                                        '<tr>' +
                                        '<td width="40px;" style="text-align: center;"><div style="background:#1bb8fa; height:40px;line-height:40px;color:#fff; ">' + (j + 1) + '</div></td>' +
                                        '<td width="340px;" style="padding-left:20px;text-align: left; display: block; color:#888;vertical-align:middle;">' + list[j].content + '</td>' +
                                        '</tr></table></div>' +
                                        '<div class="bookright">' +
                                            '<span style="cursor:pointer; " name="pic_edit"><img src="images/newsedit.png" style="vertical-align:middle;" /></span>' +
                                            '<span style="cursor: pointer; margin-left:10px;" name="pic_del"><img src="images/newsdel.png" style="vertical-align: middle;"/></span>' +
                                            '<span style="margin-left:10px;" class="showbtn">' + hui + '</span>' +
                                        '</div>' +
                                        '<div class="recontentDiv  dn" style="clear:both;">' +
                                            '<div class="bookrecontent">' +
                                                '<textarea name="recontent" style=" width:786px; height:59px; margin:5px; border:0px;">' + list[j].recontent + '</textarea>' +
                                            '</div>' +
                                            '<div class="tac tkBtn">' +
                                                    '<span class="msgtxt" style="margin-right:10px;color:red;"></span>' +
		                                            '<a href="javascript:;" class="cfff dib"  ><img src="images/booksend.png" width="162" height="41" class="okbtn" /></a>' +
		                                            '<a href="javascript:;" class="c000 dib" style="margin-left:10px;" ><img src="images/bookcannel.png" width="162" height="41" class="cannelbtn" /></a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</li>';

                        }
                        $('.bookTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                    else {
                        var htmldesc = "";
                        $('.bookTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                }
            });
        }
    }
    $(".addImgTextTk .iPagePrev").bind("click", function () {
        if (pageindex != 1) {
            pageindex--;
            PageData();
        }
    });
    $(".addImgTextTk .iPageNext").bind("click", function () {
        if (pageindex != totalpage) {
            pageindex++;
            PageData();
        }
    });
    $(".addImgTextTk .imgPageR").bind("click", function () {
        var selPage = $(".addImgTextTk .imgPageMinp").val();
        if (selPage == "" || selPage == undefined) {
            alert("请输入跳转页数");
        }
        else {
            pageindex = selPage;
            if (pageindex <= totalpage) {
                PageData();
            }
            else {
                alert("索引页码不存在");
            }
        }
    });
    $("img[name='pic_del']").live("click", function () {
        if (confirm('确定要删除吗？')) {
            var pid = $(this).attr("value");
            var Param = $.param({
                pid: pid
            });
            if (!ispost) {
                ispost = true;
                $.ajax({ url: "Ajax.aspx?action=DelBook&t=" + Math.random(),
                    type: "POST",
                    data: Param,
                    dataType: "json",
                    processData: false,
                    error: function (datas) {
                        ispost = false;
                    },
                    success: function (datas) {
                        ispost = false;
                        PageData();
                    }
                });
            }
        }
    })   
</script>