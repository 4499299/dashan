﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="newsadd.aspx.cs" Inherits="FractalistWX.Manage.newsadd"   validateRequest="false"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="js/lang/zh-cn/zh-cn.js"></script>
    <style type="text/css">
        div{
            width:100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">新闻中心</span>－新闻管理</div>
        <div class="sel">
            <ul>
                <li class="selli" style="cursor:pointer;">新闻发布管理</li>
                <li class="selact">新闻编辑管理</li>
            </ul>
        </div>         
        <div class="clear">
            <table width="770px" border="0" cellspacing="5" cellpadding="5" style="font-size: 12px;
                line-height: 25px; font-size: 14px;">
                <tr style="height: 65px;">
                    <td>
                        素材平台：
                    </td>
                    <td width="150px;">                       
                        <asp:DropDownList ID="typeTB" runat="server" Width="120px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="请选择" Value=""></asp:ListItem>
                            <asp:ListItem Text="PC" Selected Value="0"></asp:ListItem>
                            <asp:ListItem Text="MO" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="150px;" style="text-align: right;">
                        新闻类型：
                    </td>
                    <td>
                        <asp:DropDownList ID="classtypeTB" runat="server" Width="150px" Font-Size="12px"
                            Height="35px">
                            <asp:ListItem Text="公司动态" Value="0"></asp:ListItem>
                            <asp:ListItem Text="产品动态" Value="1"></asp:ListItem>
                            <asp:ListItem Text="行业动态" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        跳转页面：
                    </td>
                    <td>
                        <asp:DropDownList ID="islinkTB" runat="server" Width="120px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="外部链接" Value="0"></asp:ListItem>
                            <asp:ListItem Text="内部链接" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="150px;" style="text-align: right;">
                        输入URL地址：
                    </td>
                    <td>
                        <asp:TextBox ID="linkurlTB" TextMode="SingleLine" runat="server" Width="145px" Font-Size="12px"
                            Text="" Height="35px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        名称备注：
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="445px" Font-Size="12px"
                            Height="35px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        上传banner2:
                    </td>
                    <td colspan="3">
                        <input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35"
                                src="UploadFile1.aspx?cid=selPic&type=1" scrolling="no" frameborder="0"></iframe>
                                <p id="picsize" style="font-size:11px; color:Red;">尺寸(宽X高)：263X228(手机)，173X148(PC)</p>
                    </td>
                </tr>
                <tr id="editdiv" <%if(islink==0){ %>style="display:none;"<%} %>>
                    <td height="295" style="vertical-align: top;">
                        内 容：
                    </td>
                    <td colspan="3" style="text-align: left; vertical-align: bottom;">
                        <script id="editor" type="text/plain" style="width:610px;height:500px;"><%=Information %></script>
                        <textarea name="InformationTB" id="InformationTB" style="display:none;"></textarea>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        显示到首页：
                    </td>
                    <td>
                        <input type="checkbox" id="isindex" name="isindex" value="1" <%if(isindex==1){ %> checked="checked"<%} %> style="width: 30px; height: 30px;" />
                    </td>
                </tr>                
            </table>
        </div>   
        <div style="clear:both;"></div>        
         <div style="height:25px;"><asp:Label ID="errorLB" runat="server" Text="" ForeColor="#CC0000"></asp:Label></div>
        <div class="tac" style="height:40px;line-height:40px; margin-bottom:40px;">
            <div style="float:left; width:184px; height:47px; margin-top:6px; cursor:pointer;" id="subbtn">
                <asp:ImageButton ImageUrl="images/okbtn.png" width="184" height="47" runat="server" OnClick="comfirm_BT_Click" OnClientClick="GetCKContent();"  />
               </div>
            <div style="float:left; width:184px; height:47px; line-height:25px;margin-top:6px; margin-left:20px;cursor:pointer;" id="canbtn"><img src="images/cannelbtn.png" width="184" height="47" /></div>
        </div> 
    </div>
    <asp:HiddenField ID="mId" Value="0" runat="server" />
    </form>
</body>
</html>
<script>
    $("#canbtn").bind("click", function () {
        $("#form1")[0].reset();
    })
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "newsmanage.aspx?pictype=" + pictype;
    })
    $("#<%=typeTB.ClientID %>").bind("change", function () {
        if ($(this).val() == "0") {
            $("#updiv").attr("src", "UploadFile1.aspx?cid=selPic&type=1");
        }
        if ($(this).val() == "1") {
            $("#updiv").attr("src", "UploadFile1.aspx?cid=selPic&type=2");
        }
    }); 
    function GetCKContent() {
        $("#InformationTB").val(UE.getEditor('editor').getContent());
    }
    $("#<%=islinkTB.ClientID %>").bind("click",function(){
       if($("#<%=islinkTB.ClientID %>").val()=="0"){
           $("#editdiv").hide();
           $("#<%=linkurlTB.ClientID %>").attr("disabled", false);         
       }
       else{
           $("#editdiv").show();
           $("#<%=linkurlTB.ClientID %>").attr("disabled", true);
       }
    })
    function HTMLDecode2(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#39;/g, "\'");
        s = s.replace(/&quot;/g, "\"");
        return s;
    }
</script>
<script type="text/javascript">
    var ue = UE.getEditor('editor');

</script>
