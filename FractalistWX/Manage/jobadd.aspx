﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jobadd.aspx.cs" Inherits="FractalistWX.Manage.jobadd" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">招聘管理</span>－招聘信息</div>
        <div class="sel">
            <ul>
                <li class="selli" style="cursor:pointer;">招聘发布管理</li>
                <li class="selact">招聘编辑管理</li>
            </ul>
        </div>         
        <div class="clear">
            <table width="670px" border="0" cellspacing="5" cellpadding="5" style="font-size: 12px;
                line-height: 25px; font-size: 14px;">                
                <tr style="height: 65px;">
                    <td>
                        招聘信息：
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="445px" Font-Size="12px"
                            Height="35px" placeholder="输入招聘信息"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        上传职位图片:
                    </td>
                    <td colspan="3">
                        <input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35"
                                src="UploadFile1.aspx?cid=selPic" scrolling="no" frameborder="0"></iframe>
                                <p id="picsize" style="font-size:11px; color:Red;">尺寸(宽X高)：444X170</p>
                    </td>
                </tr>                             
            </table>
        </div>   
        <div style="clear:both;"></div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
         <div style="height:25px;"><asp:Label ID="errorLB" runat="server" Text="" ForeColor="#CC0000"></asp:Label></div>
        <div class="tac" style="height:40px;line-height:40px; margin-bottom:40px;">
            <div style="float:left; width:184px; height:47px; margin-top:6px; cursor:pointer;" id="subbtn">
                <asp:ImageButton ID="ImageButton1" ImageUrl="images/okbtn.png" width="184" height="47" runat="server" OnClick="comfirm_BT_Click" OnClientClick="GetCKContent();"  />
               </div>
            <div style="float:left; width:184px; height:47px; line-height:25px;margin-top:6px; margin-left:20px;cursor:pointer;" id="canbtn"><img src="images/cannelbtn.png" width="184" height="47" /></div>
        </div>    
         </ContentTemplate>
       </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="mId" Value="0" runat="server" />
    </form>
</body>
</html>
<script>
    $("#canbtn").bind("click", function () {
        $("#form1")[0].reset();
    })
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "jobmanage.aspx?pictype=" + pictype;
    }) 
</script>
