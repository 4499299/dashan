﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="picresouceManage.aspx.cs" Inherits="FractalistWX.Manage.picresouceManage" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div id="addImgTextTklist" class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">首页管理</span>－轮播图管理</div>
        <div class="sel">
            <ul>
                <li class="selli"><a href="javascript:;">轮播图发布管理</a></li>
                <li class="selact">轮播图素材管理</li>
            </ul>
        </div>  
        <div style="width:860px; margin-top:15px; text-align:right;">
            <span>素材平台：</span>
            <span>
                <select id="tb_type" name="tb_type " style="width:120px; font-size:12px; height:44px;">
                    <option value="0" <%if(pictype==0){ %>selected="selected"<%} %>>pc平台</option>
                    <option value="1" <%if(pictype==1){ %>selected="selected"<%} %>>手机平台</option>
                </select>
            </span>           
         </div>  
         <div class="clear imgTextListBoxAddpic">
            <ul class="imgTextList fl clear"> 
                <li id="addpic">
                    <div class="pic_txt"></div>
                    <img src="images/addpic1.jpg" />
                </li>        
            </ul>           
        </div>   
        <div class="clear imgPageBox" style="width:600px;">
		    <a class="c222  imgPageR fr" href="javascript:;" style="margin-top:3px;"><img src="images/selbtn.png" width="100%" /></a>
		    <div class="boe7e7eb  fr"><input class="db inpNoBg imgPageMinp" type="number" style="width:60px;" /></div>
		    <div class="imgPageL fr">
			    <a href="javascript:;" class="c222 fl  iPagePrev"><img src="images/left.png" style="margin-top:10px;" /></a>
			    <p class="imgPageInfo fl">1 / 1</p>
			    <a href="javascript:;" class="c222 fl  iPageNext"><img src="images/right.png" style="margin-top:10px;" /></a>
		    </div>
	    </div>
    </div>
     <div id="divDisable" style="display: none; opacity:0.5; -moz-opacity: 0.5; z-index: 1099;position: absolute; left: 0px; top: 0px; filter: alpha(opacity=60); background-color: #000; width: 100%; height:100%;">
     </div>
     <div id="addImgTextTk" class="a imgTk addImgTextTk bgfbfbfb imgTkex dn" style="z-index: 1200;width:584px;">
	    <h2 class="bg0b82c7 imgTkH2" style="color:#fff;width:538px;">轮播图素材编辑</h2>           
	    <div class="clear imgTextListBox" style="width:584px; height:285px;">
		       <table width="90%" border="0" cellspacing="5" cellpadding="5" style="font-size:12px;line-height:25px; margin:0 auto; font-size:14px;">
                    <tr style="height:65px;">
                        <td>素材平台：</td>
                        <td>
                        <input type="hidden" id="pid" value="0" />
                        <asp:DropDownList ID="typeTB"  runat="server" Width="120px" Font-Size="12px" Height="35px"><asp:ListItem Text="请选择" Value=""></asp:ListItem><asp:ListItem Text="PC" Value="0"></asp:ListItem><asp:ListItem Text="MO" Value="1"></asp:ListItem></asp:DropDownList></td>
                        <td>是否可点击：</td>
                        <td><asp:DropDownList ID="isclickTB"  runat="server" Width="130px" Font-Size="12px" Height="35px"><asp:ListItem Text="可点击" Value="0"></asp:ListItem><asp:ListItem Text="不可点击" Value="1"></asp:ListItem></asp:DropDownList></td>
                    </tr>
                    <tr  style="height:65px;">
                        <td>跳转页面：</td>
                        <td><asp:DropDownList ID="islinkTB"  runat="server" Width="120px" Font-Size="12px" Height="35px"><asp:ListItem Text="外部链接" Value="0"></asp:ListItem><asp:ListItem Text="内部链接" Value="1"></asp:ListItem></asp:DropDownList></td>
                        <td>输入URL地址：</td>
                        <td><asp:TextBox ID="linkurlTB" TextMode="SingleLine" runat="server" Width="125px" Font-Size="12px" Height="35px" placeholder="http://"></asp:TextBox></td>
                    </tr>
                    <tr  style="height:65px;">
                        <td>名称备注：</td>
                        <td colspan="3"><asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="396px" Font-Size="12px" Height="35px"></asp:TextBox></td>
                    </tr>
                    <tr  style="height:65px;">
                        <td valign="top">
                        上传banner：                       
                        </td>
                        <td  colspan="3">
                            <input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px; line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" id="updiv" width="225" height="35" src="UploadFile.aspx?cid=selPic&type=1" scrolling="no" frameborder="0"></iframe>
                             <p id="picsize" style="font-size:11px; color:Red;">尺寸(宽X高)：640X685(手机)，1920X683(PC)</p>
                        </td>
                    </tr>
                    <tr  style="height:15px;">
                        <td></td>
                        <td style=" color:Red; font-size:12px;" id="msgtxt"></td>
                    </tr>
                </table>
	    </div>      
        <div class="clear" style="width:98%; margin:0 auto; text-align:center;">
            <hr style="height:1px;border:none;border-top:1px solid #f2f2f2;" />    
        </div>   
	    <div class="tac tkBtn">
		    <a href="javascript:;" class="cfff tkBtnL dib"><img src="images/okbtn.png" width="184" height="47" /></a>
		    <a href="javascript:;" class="c000 tkBtnR dib"><img src="images/cannelbtn.png" width="184" height="47" /></a>        
	    </div>
    </div>
    </form>
</body>
</html>
<script>
    var ispost = false;
    $("#addpic").live("click", function () {
        $("input").val("");
        $("#pid").val("0");
        $("#addImgTextTk").show();
        $("#divDisable").show();
    })
    $(".tkBtnR").bind("click", function () {        
        $("#addImgTextTk").hide();
        $("#divDisable").hide();
    })
    $("#<%=typeTB.ClientID %>").bind("change", function () {
        if ($(this).val() == "0") {
            $("#updiv").attr("src", "UploadFile.aspx?cid=selPic&type=1");
        }
        if ($(this).val() == "1") {
            $("#updiv").attr("src", "UploadFile.aspx?cid=selPic&type=2");
        }
    }); 
    $("#<%=isclickTB.ClientID %>").bind("change", function () {
        if ($(this).val() == "1") {
            $("#<%=islinkTB.ClientID %>").attr("disabled", true);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", true);            
        }
        else {
            $("#<%=islinkTB.ClientID %>").attr("disabled", false);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", false);   
        }
    })
    $(".tkBtnL").bind("click", function () {
        if (!ispost) {
            ispost = true;
            var pid = $("#pid").val();
            var typeTB = $("#<%=typeTB.ClientID %>").val();
            var isclickTB = $("#<%=isclickTB.ClientID %>").val();
            var islinkTB = $("#<%=islinkTB.ClientID %>").val();
            var linkurlTB = $("#<%=linkurlTB.ClientID %>").val();
            var titleTB = $("#<%=titleTB.ClientID %>").val();
            var selPic = $("#<%=selPic.ClientID %>").val();
            if (typeTB == "") {
                $("#msgtxt").text("请选择素材平台");
                ispost = false;
                return
            }
            if (isclickTB == "0") {
                if (linkurlTB == "") {
                    $("#msgtxt").text("请选择链接地址");
                    ispost = false;
                    return
                }
            }
            if (titleTB == "") {
                $("#msgtxt").text("请输入名称备注");
                ispost = false;
                return
            }
            if (selPic == "") {
                $("#msgtxt").text("请上传banner");
                ispost = false;
                return
            }
            var Param = $.param({
                typeTB: typeTB,
                isclickTB: isclickTB,
                islinkTB: islinkTB,
                linkurlTB: linkurlTB,
                titleTB: titleTB,
                selPic: selPic,
                pid: pid
            });
            $.ajax({ url: "Ajax.aspx?action=AddPic&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    $("#msgtxt").text("素材添加成功");
                    setTimeout(function () {
                        $("#addImgTextTk").hide();
                        $("#divDisable").hide();
                        $("#msgtxt").text("");
                        var pictype = $("#tb_type").val();                      
                        PageData();
                    }, 1000);
                }
            });
        }
    })
    var pictype = $("#tb_type").val();
    var pageindex = 1, totalpage = 0;
    PageData();

    $("#tb_type").bind("change", function () {        
        pictype = $("#tb_type").val();
        pageindex = 1, totalpage = 0;
        PageData();
    })
    function PageData() {
        var Param = $.param({
            pictype: pictype,
            pageindex: pageindex
        });
        if (!ispost) {
            ispost = true;
            $.ajax({ url: "Ajax.aspx?action=picpgsource&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    var list = datas.list;
                    totalpage = datas.totalpage;
                    $('.imgTextList').html("");
                    if (list != null && list.length > 0) {
                        var htmldesc = "";
                        for (var j = 0; j < list.length; j++) {
                            htmldesc += '<li>';
                            htmldesc += '<div class="pic_txt">' + list[j].title + '</div>';
                            htmldesc += '<div class="pic_pic"><img src="../UpFiles/' + list[j].pic + '" width="236" height="132" /></div>';
                            htmldesc += '<div style="margin-top:2px;"><span style="cursor: pointer;"><img src="images/edit.png" name="pic_edit" value="' + list[j].pid + '" isshow="' + list[j].isshow + '" isclick="' + list[j].isclick + '" islink="' + list[j].islink + '" linkurl="' + list[j].linkurl + '" /></span><span style="margin-left:3px;cursor: pointer;"><img src="images/del.png" name="pic_del" value="' + list[j].pid + '" /></span> </div>';
                            var pictype = list[j].pictype == "0" ? "PC" : "MO";
                            htmldesc += '<div class="pic_tishi">' + pictype + '</div>'
                            htmldesc += '</li> ';
                        }
                        htmldesc += '<li id="addpic" style="cursor: pointer;">';
                        htmldesc += '<div class="pic_txt"></div>';
                        htmldesc += '<img src="images/addpic1.jpg" />';
                        htmldesc += '</li>';
                        $('.imgTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                    else {
                        var htmldesc = "";
                        htmldesc += '<li id="addpic">';
                        htmldesc += '<div class="pic_txt"></div>';
                        htmldesc += '<img src="images/addpic1.jpg" />';
                        htmldesc += '</li>';
                        $('.imgTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                }
            });
        }
    }
    $(".addImgTextTk .iPagePrev").bind("click", function () {
        if (pageindex != 1) {
            pageindex--;
            PageData();
        }
    });
    $(".addImgTextTk .iPageNext").bind("click", function () {
        if (pageindex != totalpage) {
            pageindex++;
            PageData();
        }
    });
    $(".addImgTextTk .imgPageR").bind("click", function () {
        var selPage = $(".addImgTextTk .imgPageMinp").val();
        if (selPage == "" || selPage == undefined) {
            alert("请输入跳转页数");
        }
        else {
            pageindex = selPage;           
            if (pageindex <= totalpage) {
                PageData();
            }
            else {
                alert("索引页码不存在");
            }
        }
    });
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "picshow.aspx?type=" + pictype;
    })
    $("img[name='pic_del']").live("click", function () {
        if (confirm('确定要删除吗？')) {
            var pid = $(this).attr("value");
            var Param = $.param({
                pid: pid
            });
            if (!ispost) {
                ispost = true;
                $.ajax({ url: "Ajax.aspx?action=DelPic&t=" + Math.random(),
                    type: "POST",
                    data: Param,
                    dataType: "json",
                    processData: false,
                    error: function (datas) {
                        ispost = false;
                    },
                    success: function (datas) {
                        ispost = false;
                        PageData();
                    }
                });
            }
        }
    })
    $("img[name='pic_edit']").live("click", function () {
        var pid = $(this).attr("value");
        var title = $(this).parent().parent().parent("li").find(".pic_txt").text();
        var pic = $(this).parent().parent().parent("li").find(".pic_pic img").attr("src");
        pic = pic.substring(11).replace("%2f", "/");
        var linkurlTB = $(this).attr("linkurl");
        var islinkTB = $(this).attr("islink");
        var isclickTB = $(this).attr("isclick");
        var typeTB = $("#tb_type").val();
        $("#pid").val(pid);
        $("#<%=titleTB.ClientID %>").val(title);
        $("#<%=selPic.ClientID %>").val(pic);
        $("#<%=linkurlTB.ClientID %>").val(linkurlTB);
        $("#<%=islinkTB.ClientID %>").val(islinkTB);
        $("#<%=isclickTB.ClientID %>").val(isclickTB);
        $("#<%=typeTB.ClientID %>").val(typeTB);
        $("#addImgTextTk").show();
        $("#divDisable").show();
    })
</script>
