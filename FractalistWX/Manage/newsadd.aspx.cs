﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Tool;
using FractalistWX.Dal;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;

namespace FractalistWX.Manage
{
    public partial class newsadd : System.Web.UI.Page
    {
        public int pictype = 0, isindex=0,islink=0;
        public string Information = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pictype = Tools.CheckVar(Request["type"], 0);
                int pid = Tools.CheckVar(Request["pid"], 0);
                mId.Value = pid.ToString();                
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select * from wechat_news");
                strSql.Append(" where id=@pid");
                MySqlParameter[] parameters = {
				    new MySqlParameter("@pid", MySqlDbType.Int32)               
                };
                parameters[0].Value = pid;
                DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                if (dt != null&&dt.Rows.Count>0)
                {
                    typeTB.SelectedValue = dt.Rows[0]["newstype"].ToString();
                    classtypeTB.SelectedValue = dt.Rows[0]["classtype"].ToString();
                    islinkTB.SelectedValue = dt.Rows[0]["islink"].ToString();
                    linkurlTB.Text = dt.Rows[0]["linkurl"].ToString();
                    islink = Convert.ToInt32(dt.Rows[0]["islink"].ToString());
                    titleTB.Text = dt.Rows[0]["title"].ToString();
                    selPic.Value = dt.Rows[0]["pic"].ToString();
                    Information = dt.Rows[0]["content"].ToString();
                    isindex = dt.Rows[0]["isindex"].ToString()=="1"?1:0;
                }
            }
            
        }
        protected void comfirm_BT_Click(object sender, EventArgs e)
        {
            errorLB.Text = "";
            int isindex=Tools.CheckVar(Request["isindex"],0);
            if (titleTB.Text == "")
            {
                errorLB.Text = "标题不能为空";
                return;
            }
            if (islinkTB.SelectedValue == "0"&&linkurlTB.Text=="")
            {
                errorLB.Text = "链接地址不能为空";
                return;
            }
            if (islinkTB.SelectedValue == "1")
            {
                if (Request["InformationTB"] == "")
                {
                    errorLB.Text = "链接地址不能为空";
                    return;
                }
            }           
            int m_Id = int.Parse(mId.Value);
            if (m_Id > 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_news set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic,");
                strSql.Append("newstype=@newstype,");
                strSql.Append("classtype=@classtype,");
                strSql.Append("isindex=@isindex,");
                strSql.Append("islink=@islink,");
                strSql.Append("linkurl=@linkurl,");
                strSql.Append("content=@content");
                strSql.Append(" where id=@id");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),
					new MySqlParameter("@newstype", MySqlDbType.Int32,4),
					new MySqlParameter("@classtype", MySqlDbType.Int32,4),
					new MySqlParameter("@isindex", MySqlDbType.Int32,4),
					new MySqlParameter("@islink", MySqlDbType.Int32,4),
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@content", MySqlDbType.Text),
					new MySqlParameter("@id", MySqlDbType.Int32,11)};
                parameters[0].Value =Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                parameters[2].Value = this.typeTB.SelectedValue;
                parameters[3].Value = this.classtypeTB.SelectedValue;
                parameters[4].Value = isindex;
                parameters[5].Value = islinkTB.SelectedValue;
                parameters[6].Value = Tools.SafeSql(this.linkurlTB.Text);
                parameters[7].Value = Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], ""));
                parameters[8].Value = m_Id;

                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
               
                if (this.typeTB.SelectedValue == "0")
                {
                    if (isindex == 1)
                    {
                        ch.WriteP_indexFile();
                    }
                    if (this.classtypeTB.SelectedValue == "0")
                    {
                       
                        ch.WriteP_newsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "1")
                    {
                        
                        ch.WriteP_pronewsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "2")
                    {
                       
                        ch.WriteP_economyFile();
                    }
                }
                else
                {
                    if (isindex == 1)
                    {
                        ch.WriteM_indexFile();
                    }

                    if (this.classtypeTB.SelectedValue == "0")
                    {
                        
                        ch.WriteM_newsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "1")
                    {
                       
                        ch.WriteM_pronewsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "2")
                    {
                        
                        ch.WriteM_economyFile();
                    }

                }
                if (islinkTB.SelectedValue == "1")
                {
                    if (this.typeTB.SelectedValue == "0")
                    {
                        string path = HttpContext.Current.Server.MapPath("../");
                        Encoding code = Encoding.GetEncoding("UTF-8");
                        // 读取模板文件
                        string temp = HttpContext.Current.Server.MapPath("../template/p_shownews.html");
                        StreamReader sr = null;
                        StreamWriter sw = null;
                        string str = "";
                        try
                        {
                            sr = new StreamReader(temp, code);
                            str = sr.ReadToEnd(); // 读取文件
                        }
                        catch (Exception exp)
                        {
                            HttpContext.Current.Response.Write(exp.Message);
                            HttpContext.Current.Response.End();
                            sr.Close();
                        }
                        string htmlfilename = "shownews_" + m_Id + ".html";


                        string strnav = "";
                        if (this.classtypeTB.SelectedValue == "0")
                        {
                            strnav = "<li class=\"selact\"><a href=\"news.aspx\">公司动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "1")
                        {
                            strnav = "<li class=\"selli\"><a href=\"news.aspx\">公司动态</a></li>" +
                                     "<li class=\"selact\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "2")
                        {
                            strnav = "<li class=\"selli\"><a href=\"news.aspx\">公司动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                     "<li class=\"selact\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        // 替换内容      
                        str = str.Replace("$strnav$", strnav);
                        str = str.Replace("$title$", Tools.SafeSql(titleTB.Text));
                        str = str.Replace("$content$", Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], "")));
                        // 写文件
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                    }
                    else
                    {
                        string path = HttpContext.Current.Server.MapPath("../");
                        Encoding code = Encoding.GetEncoding("UTF-8");
                        // 读取模板文件
                        string temp = HttpContext.Current.Server.MapPath("../template/M_shownews.html");
                        StreamReader sr = null;
                        StreamWriter sw = null;
                        string str = "";
                        try
                        {
                            sr = new StreamReader(temp, code);
                            str = sr.ReadToEnd(); // 读取文件
                        }
                        catch (Exception exp)
                        {
                            HttpContext.Current.Response.Write(exp.Message);
                            HttpContext.Current.Response.End();
                            sr.Close();
                        }
                        string htmlfilename = "M_shownews_" + m_Id + ".html";

                        string strnav = "";
                        if (this.classtypeTB.SelectedValue == "0")
                        {
                            strnav = "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#fff;\">公司动态</a></div></li>" +
                                     "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#888;\">产品动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#888;\">行业动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "1")
                        {
                            strnav = "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#888;\">公司动态</a></div></li>" +
                                     "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#fff;\">产品动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#888;\">行业动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "2")
                        {
                            strnav = "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#888;\">公司动态</a></div></li>" +
                                      "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#888;\">产品动态</a></div></li>" +
                                    "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#fff;\">行业动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        // 替换内容      
                        str = str.Replace("$strnav$", strnav);
                        str = str.Replace("$title$", Tools.SafeSql(titleTB.Text));
                        str = str.Replace("$content$", Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], "")));
                        // 写文件
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                    }
                }
                errorLB.Text = "更新成功";
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_news(");
                strSql.Append("title,pic,newstype,classtype,isindex,islink,linkurl,content)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic,@newstype,@classtype,@isindex,@islink,@linkurl,@content);SELECT LAST_INSERT_ID()");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),
					new MySqlParameter("@newstype", MySqlDbType.Int32,4),
					new MySqlParameter("@classtype", MySqlDbType.Int32,4),
					new MySqlParameter("@isindex", MySqlDbType.Int32,4),
					new MySqlParameter("@islink", MySqlDbType.Int32,4),
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@content", MySqlDbType.Text)};

                parameters[0].Value = Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                parameters[2].Value = this.typeTB.SelectedValue;
                parameters[3].Value = this.classtypeTB.SelectedValue;
                parameters[4].Value = isindex;
                parameters[5].Value = islinkTB.SelectedValue;
                parameters[6].Value = Tools.SafeSql(this.linkurlTB.Text);
                parameters[7].Value = Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], ""));

                int id = 0;
                id= SQLTool.GetFirstIntField(strSql.ToString(), parameters);
                if (this.typeTB.SelectedValue == "0")
                {
                    if (this.classtypeTB.SelectedValue == "0")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteP_newsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "1")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteP_pronewsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "2")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteP_economyFile();
                    }
                }
                else
                {

                    if (this.classtypeTB.SelectedValue == "0")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteM_newsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "1")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteM_pronewsFile();
                    }
                    else if (this.classtypeTB.SelectedValue == "2")
                    {
                        CreatHtml ch = new CreatHtml();
                        ch.WriteM_economyFile();
                    }

                }
                if (islinkTB.SelectedValue == "1")
                {
                    if (this.typeTB.SelectedValue == "0")
                    {
                        string path = HttpContext.Current.Server.MapPath("../");
                        Encoding code = Encoding.GetEncoding("UTF-8");
                        // 读取模板文件
                        string temp = HttpContext.Current.Server.MapPath("../template/p_shownews.html");
                        StreamReader sr = null;
                        StreamWriter sw = null;
                        string str = "";
                        try
                        {
                            sr = new StreamReader(temp, code);
                            str = sr.ReadToEnd(); // 读取文件
                        }
                        catch (Exception exp)
                        {
                            HttpContext.Current.Response.Write(exp.Message);
                            HttpContext.Current.Response.End();
                            sr.Close();
                        }
                        string htmlfilename = "shownews_" + id + ".html";

                        string strnav = "";
                        if (this.classtypeTB.SelectedValue == "0")
                        {
                            strnav = "<li class=\"selact\"><a href=\"news.aspx\">公司动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                      "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "1")
                        {
                            strnav = "<li class=\"selli\"><a href=\"news.aspx\">公司动态</a></li>" +
                                     "<li class=\"selact\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "2")
                        {
                            strnav = "<li class=\"selli\"><a href=\"news.aspx\">公司动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"pronews.aspx\">产品动态</a></li>" +
                                     "<li class=\"selact\"><a href=\"economy.aspx\">行业动态</a></li>" +
                                     "<li class=\"selli\"><a href=\"videolist.aspx\">最新视频</a></li>";
                        }
                        // 替换内容      
                        str = str.Replace("$strnav$", strnav);
                        str = str.Replace("$title$", Tools.SafeSql(titleTB.Text));
                        str = str.Replace("$content$", Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], "")));
                        // 写文件
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                    }
                    else
                    {
                        string path = HttpContext.Current.Server.MapPath("../");
                        Encoding code = Encoding.GetEncoding("UTF-8");
                        // 读取模板文件
                        string temp = HttpContext.Current.Server.MapPath("../template/M_shownews.html");
                        StreamReader sr = null;
                        StreamWriter sw = null;
                        string str = "";
                        try
                        {
                            sr = new StreamReader(temp, code);
                            str = sr.ReadToEnd(); // 读取文件
                        }
                        catch (Exception exp)
                        {
                            HttpContext.Current.Response.Write(exp.Message);
                            HttpContext.Current.Response.End();
                            sr.Close();
                        }
                        string htmlfilename = "M_shownews_" + id + ".html";

                        string strnav = "";
                        if (this.classtypeTB.SelectedValue == "0")
                        {
                            strnav = "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#fff;\">公司动态</a></div></li>" +
                                     "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#888;\">产品动态</a></div></li>"+
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#888;\">行业动态</a></div></li>"+
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "1")
                        {
                            strnav = "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#888;\">公司动态</a></div></li>" +
                                     "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#fff;\">产品动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#888;\">行业动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        else if (this.classtypeTB.SelectedValue == "2")
                        {
                            strnav = "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div style=\"position:relative;\"><a href=\"news.aspx\" style=\"color:#888;\">公司动态</a></div></li>" +
                                      "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"pronews.aspx\" style=\"color:#888;\">产品动态</a></div></li>" +
                                    "<li  class=\"newsact\"><img src=\"/img/news_bjact.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"economy.aspx\" style=\"color:#fff;\">行业动态</a></div></li>" +
                                    "<li><img src=\"/img/news_bj.png\" width=\"100%\" alt=\"\" /><div  style=\"position:relative;\"><a href=\"videolist.aspx\" style=\"color:#888;\">最新视频</a></div></li>";
                        }
                        // 替换内容      
                        str = str.Replace("$strnav$", strnav);
                        str = str.Replace("$title$", Tools.SafeSql(titleTB.Text));
                        str = str.Replace("$content$", Tools.SafeSql(Tools.CheckVar(Request["InformationTB"], "")));
                        // 写文件
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                            
                    }
                }

                errorLB.Text = "提交成功";

            }           
          
        }
    }
}