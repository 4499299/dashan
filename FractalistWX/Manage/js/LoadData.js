﻿LoadMenu();

var isposts = false;
function LoadMenu() {
    var Param = $.param({
        action: "getmenu"
    });
    if (!isposts) {
        isposts = true;
        $('#sArea').html("");
        $.ajax({ url: "Ajax.aspx?t=" + Math.random(),
            type: "POST",
            data: Param,
            dataType: "json",
            processData: false,
            error: function (datas) {
                isposts = false;
            },
            success: function (datas) {
                isposts = false;
                var list = datas;
                for (var i = 0; i < list.length; i++) {
                    LoadMenuChild(list[i], null, true);
                    //console.log(list[i]);
                    if (list[i].list.length > 0) {
                        for (var j = 0; j < list[i].list.length; j++) {
                            LoadMenuChild(list[i].list[j], i, false);
                        }
                    }
                }
            }
        });
    }
}
/*
* 添加初始数据
* stkInpTit string 菜单名字
* INow 数字 第几个有二级
* isOne 布尔 是否是一级菜单， true→一级，false→二级
*/
function LoadMenuChild(stkInpTit, iNow, isOne) {
    var sName = stkInpTit.Name,
		$sArea = $('#sArea'),
		$tk = $('.addTk');
    $('.sDl dt,.sDl dd').removeAttr('bOn');

    if (isOne) {//一级菜单添加
        var sMenuBigHtml =
			    '<strong>' + sName + '</strong>' +
			    '<span class="a menu_opr dn">' +
				    '<a href="javascript:void(0);" class="cw icon14_common add_gray">添加</a>' +
				    '<a href="javascript:void(0);" class="cw icon14_common  edit_gray">编辑</a>' +
				    '<a href="javascript:void(0);" class="cw icon14_common up_gray">↑</a>' +
				    '<a href="javascript:void(0);" class="cw icon14_common down_gray">↓</a>' +
				    '<a href="javascript:void(0);" class="cw icon14_common del_gray">删除</a>' +
			    '</span>';
        var $addDl = $('<dl>'),
			$addDt = $('<dt>').attr({ id: stkInpTit.ID, sort: stkInpTit.Sort, level: 1 });
        $sArea.append($addDl);
        $addDl.addClass('bobe7e7eb sDl');
        $addDt.addClass('r fs14 tpbg').attr('bOn', true);
        $addDl.append($addDt);
        $addDt.append(sMenuBigHtml);
    } else {
        var sMenuSmallHtml =
			'<i class="a cd5d5d5 icon_dot">●</i>' +
			'<a href="javascript:void(0);" class="inner_menu_link"><strong>' + sName + '</strong></a>' +
			'<span class="a menu_opr dn">' +
				'<a href="javascript:void(0);" class="icon14_common edit_gray">编辑</a>' +
				'<a href="javascript:void(0);" class="icon14_common del_gray">删除</a>' +
				'<a href="javascript:void(0);" class="cw icon14_common up_gray">↑</a>' +
				'<a href="javascript:void(0);" class="cw icon14_common down_gray">↓</a>' +
			'</span>';
        var $addDD = $('<dd>').attr({ id: stkInpTit.ID, sort: stkInpTit.Sort, pid: stkInpTit.Pid, level: 2 }),
			$addDl = $('.sDl').eq($tk.data('iNow'));
        $addDD.addClass('r lh32').attr('bOn', true);
        $addDD.append(sMenuSmallHtml);
        $('.sDl').eq(iNow).append($addDD);
    }
};
function LoadMenuDesc() {
    var Param = $.param({
        action: "getmenu"
    });
    if (!isposts) {
        isposts = true;
        $.ajax({ url: "Ajax.aspx?t=" + Math.random(),
            type: "POST",
            data: Param,
            dataType: "json",
            processData: false,
            error: function (datas) {
                isposts = false;
            },
            success: function (datas) {
                isposts = false;
                var list = datas;
                for (var i = 0; i < list.length; i++) {
                    LoadMenuChild(list[i], null, true);
                    if (list[i].list.length > 0) {
                        for (var j = 0; j < list[i].list.length; j++) {
                            LoadMenuChild(list[i].list[j], i, false);
                        }
                    }
                }
            }
        });
    }
}