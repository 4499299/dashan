﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Dal;

namespace FractalistWX.Manage
{
    public partial class joblist : System.Web.UI.Page
    {
        public int Count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GridViewDataBind();
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridViewDataBind();

            TextBox tb = (TextBox)GridView1.BottomPagerRow.FindControl("inPageNum");
            tb.Text = (GridView1.PageIndex + 1).ToString();

        }

        protected void GridViewDataBind()
        {
            // GridView1.DataSource = SqlHelper.GetDataSet("select Id,ShopName,Province,City,Stamp from TryGetInfo " + getsql() + " order by Id desc", out Count);
            GridView1.DataSource = MySQLHelper.GetDataSet("select id,username,mobile,email,filepath,stamp from wechat_jobjoin  order by Id desc", out Count);
            GridView1.DataBind();

            if (GridView1.Rows.Count != 0)
            {
                Control table = GridView1.Controls[0];
                int count = table.Controls.Count;
                table.Controls[count - 1].Visible = true;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "go")
            {
                try
                {
                    TextBox tb = (TextBox)GridView1.BottomPagerRow.FindControl("inPageNum");
                    int num = Int32.Parse(tb.Text);
                    GridViewPageEventArgs ea = new GridViewPageEventArgs(num - 1);
                    GridView1_PageIndexChanging(null, ea);
                }
                catch { }
            }
            else if (e.CommandName == "batch")
            {
                try
                {
                    string selck = Request.Form["selck"];
                    //SQLTool.ExeSql("update wechat_jobjoin set State='0' where Id in (" + selck + ")");
                    GridViewDataBind();
                }
                catch { }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GridView1.PageIndex = 0;
            GridViewDataBind();
        }

        protected void Button_Hide_Click(object sender, EventArgs e)
        {
            GridViewDataBind();
        }

        
       
    }
}