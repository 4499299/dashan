﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="actionmoshow.aspx.cs" Inherits="FractalistWX.Manage.actionmoshow" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">首页管理</span>－最新活动管理</div>
        <div class="sel">
            <ul>
                <li class="selli"><a href="activitypcshow.aspx">最新活动发布-PC</a></li>
                <li class="selact">最新活动发布-手机</li>
            </ul>
        </div>  
        <div class="clear">
            <table width="530px" border="0" cellspacing="5" cellpadding="5" style="font-size: 12px;
                line-height: 25px; font-size: 14px;">
                <tr style="height: 65px;">
                    <td>
                        素材平台：
                    </td>
                    <td>
                        <input type="hidden" id="pid" value="<%=pid %>" />
                        <asp:DropDownList ID="typeTB" Enabled="false" runat="server" Width="120px" Font-Size="12px"
                            Height="35px">
                            <asp:ListItem Text="请选择" Value=""></asp:ListItem>
                            <asp:ListItem Text="PC"  Value="0"></asp:ListItem>
                            <asp:ListItem Text="MO" Selected Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        是否可点击：
                    </td>
                    <td>
                        <asp:DropDownList ID="isclickTB" runat="server" Width="130px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="可点击跳转"  Value="0"></asp:ListItem>
                            <asp:ListItem Text="不可点击跳转"  Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td>
                        跳转页面：
                    </td>
                    <td>
                        <asp:DropDownList ID="islinkTB" runat="server" Width="120px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="外部链接"  Value="0"></asp:ListItem>
                            <asp:ListItem Text="内部链接"  Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        输入URL地址：
                    </td>
                    <td>
                        <asp:TextBox ID="linkurlTB" TextMode="SingleLine" runat="server" Width="125px" Font-Size="12px" Text="" Height="35px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px; display:none;">
                    <td>
                        名称备注：
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="396px" Font-Size="12px"
                            Height="35px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px; display:none;">
                    <td>
                        上传banner：
                    </td>
                    <td colspan="3">
                        <input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35"
                                src="UploadFile1.aspx?cid=selPic" scrolling="no" frameborder="0"></iframe>
                    </td>
                </tr>
                <tr style="height: 25px;">
                    <td>
                    </td>
                    <td style="color: Red; font-size: 12px;" id="msgtxt">
                    </td>
                </tr>
            </table>
        </div>   
        <div class="tac" style="height:40px;line-height:40px; margin-bottom:40px;">
            <div style="float:left; width:184px; height:47px; margin-top:6px; cursor:pointer;" id="subbtn"><img src="images/okbtn.png" width="184" height="47" /></div>
            <div style="float:left; width:184px; height:47px; line-height:25px;margin-top:6px; margin-left:20px;cursor:pointer;" id="canbtn"><img src="images/cannelbtn.png" width="184" height="47" /></div>
        </div>
    </div>
    </form>
</body>
</html>
<script>
    var ispost = false;
    $("#canbtn").bind("click", function () {
        $("#form1")[0].reset();
    })
    $("#<%=isclickTB.ClientID %>").bind("change", function () {
        if ($(this).val() == "1") {
            $("#<%=islinkTB.ClientID %>").attr("disabled", true);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", true);
        }
        else {
            $("#<%=islinkTB.ClientID %>").attr("disabled", false);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", false);
        }
    })
    $("#subbtn").bind("click", function () {
        if (!ispost) {
            ispost = true;
            var pid = $("#pid").val();
            var typeTB = $("#<%=typeTB.ClientID %>").val();
            var isclickTB = $("#<%=isclickTB.ClientID %>").val();
            var islinkTB = $("#<%=islinkTB.ClientID %>").val();
            var linkurlTB = $("#<%=linkurlTB.ClientID %>").val();
            var titleTB = $("#<%=titleTB.ClientID %>").val();
            var selPic = $("#<%=selPic.ClientID %>").val();
            if (typeTB == "") {
                $("#msgtxt").text("请选择素材平台");
                ispost = false;
                return
            }
            if (isclickTB == "0") {
                if (linkurlTB == "") {
                    $("#msgtxt").text("请选择链接地址");
                    ispost = false;
                    return
                }
            }
            
            var Param = $.param({
                typeTB: typeTB,
                isclickTB: isclickTB,
                islinkTB: islinkTB,
                linkurlTB: linkurlTB,
                titleTB: titleTB,
                selPic: selPic,
                pid: pid
            });
            $.ajax({ url: "Ajax.aspx?action=AddActiviy&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    $("#msgtxt").text("操作成功");
                    setTimeout(function () {
                        $("#msgtxt").text("");
                    }, 1000);
                }
            });
        }
    })
</script>