﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="newsmanage.aspx.cs" Inherits="FractalistWX.Manage.newsmanage" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">新闻中心</span>－新闻管理</div>
        <div class="sel">
            <ul>
                <li class="selact">新闻发布管理</li>
                <li class="selli" style="cursor:pointer;">新闻编辑管理</li>
            </ul>
        </div>  
         <div style="width:833px; margin-top:15px; text-align:right;">
             <span>
                <select id="classTB_type" name="classTB_type " style="width:120px; font-size:12px; height:44px;">
                    <option value="">全部</option>
                    <option value="0">公司动态</option>
                    <option value="1">产品动态</option>
                    <option value="2">行业动态</option>
                </select>
            </span>  
            <span>选择平台：</span>
            <span>
                <select id="tb_type" name="tb_type " style="width:120px; font-size:12px; height:44px;">
                    <option value="0" <%if(pictype==0){ %>selected="selected"<%} %>>pc平台</option>
                    <option value="1" <%if(pictype==1){ %>selected="selected"<%} %>>手机平台</option>
                </select>
            </span>  
            <span id="addnewsbtn" style="cursor:pointer;"><img src="images/newsadd.png" style="vertical-align:middle; margin-top:-3px;" /></span>         
         </div>  
         <div class="clear"></div>
        <div class="clear imgTextListBoxAddpic">
            <ul class="newsTextList fl clear">                       
            </ul>           
        </div>   
        <div style="clear:both;"></div>
        <div class="clear imgPageBox" style="width:600px; margin-top:30px;">
		    <a class="c222  imgPageR fr" href="javascript:;" style="margin-top:3px;"><img src="images/selbtn.png" width="100%" /></a>
		    <div class="boe7e7eb  fr"><input class="db inpNoBg imgPageMinp" type="number" style="width:60px;" /></div>
		    <div class="imgPageL fr">
			    <a href="javascript:;" class="c222 fl  iPagePrev"><img src="images/left.png" style="margin-top:10px;" /></a>
			    <p class="imgPageInfo fl">1 / 1</p>
			    <a href="javascript:;" class="c222 fl  iPageNext"><img src="images/right.png" style="margin-top:10px;" /></a>
		    </div>
	    </div>
    </div>
    </form>
</body>
</html>
<script>
    var ispost = false;
    var pictype = $("#tb_type").val();
    var classtype = $("#classTB_type").val();
    var pageindex = 1, totalpage = 0;
    PageData();

    $("#tb_type").bind("change", function () {
        pictype = $("#tb_type").val();
        classtype = $("#classTB_type").val();     
        pageindex = 1, totalpage = 0;
        PageData();
    })
    $("#classTB_type").bind("change", function () {
        pictype = $("#tb_type").val();
        classtype = $("#classTB_type").val();
        pageindex = 1, totalpage = 0;
        PageData();
    })
    function PageData() {
        var Param = $.param({
            newstype: pictype,
            pageindex: pageindex,
            classtype: classtype
        });
        if (!ispost) {
            ispost = true;
            $.ajax({ url: "Ajax.aspx?action=NewsList&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    var list = datas.list;
                    totalpage = datas.totalpage;
                    $('.newsTextList').html("");
                    if (list != null && list.length > 0) {
                        var htmldesc = "";
                        for (var j = 0; j < list.length; j++) {
                            htmldesc += '<li>' +
                                    '<div class="newsleft">' +
                                    '<table cellpadding="0" cellspacing="0">' +
                                        ' <tr>' +
                                        '<td width="60px;" style="text-align:center;">'+(j+1)+'</td>' +
                                        '<td  width="470px;" style="text-align:center;display:block;white-space:nowrap; overflow:hidden; text-overflow:ellipsis; color:#888;">'+list[j].title+'</td>' +
                                        '</tr>' +
                                    '</table>' +
                                    '</div>' +
                                    '<div class="newsright">' +
                                        '<span value="' + list[j].pid + '" style="cursor:pointer; "><img src="images/newsedit.png" style="vertical-align:middle;" name="pic_edit"  value="' + list[j].pid + '" /></span>' +
                                        '<span value="' + list[j].pid + '" style="cursor:pointer;margin-left:10px;"><img src="images/newsdel.png" style="vertical-align:middle;" name="pic_del"  value="' + list[j].pid + '"  /></span>' +
                                        '</div>' +
                                       ' </li>';

                        }
                        $('.newsTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                    else {
                        var htmldesc = "";                      
                        $('.imgTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                }
            });
        }
    }
    $(".addImgTextTk .iPagePrev").bind("click", function () {
        if (pageindex != 1) {
            pageindex--;
            PageData();
        }
    });
    $(".addImgTextTk .iPageNext").bind("click", function () {
        if (pageindex != totalpage) {
            pageindex++;
            PageData();
        }
    });
    $(".addImgTextTk .imgPageR").bind("click", function () {
        var selPage = $(".addImgTextTk .imgPageMinp").val();
        if (selPage == "" || selPage == undefined) {
            alert("请输入跳转页数");
        }
        else {
            pageindex = selPage;           
            if (pageindex <= totalpage) {
                PageData();
            }
            else {
                alert("索引页码不存在");
            }
        }
    });
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "newsadd.aspx?type=" + pictype;
    })
    $("img[name='pic_del']").live("click", function () {
        if (confirm('确定要删除吗？')) {
            var pid = $(this).attr("value");
            var Param = $.param({
                pid: pid
            });
            if (!ispost) {
                ispost = true;
                $.ajax({ url: "Ajax.aspx?action=DelNews&t=" + Math.random(),
                    type: "POST",
                    data: Param,
                    dataType: "json",
                    processData: false,
                    error: function (datas) {
                        ispost = false;
                    },
                    success: function (datas) {
                        ispost = false;
                        PageData();
                    }
                });
            }
        }
    })
    $("img[name='pic_edit']").live("click", function () {
        var pid = $(this).attr("value");        
        window.location.href = "newsadd.aspx?pid=" + pid;
    })
    $("#addnewsbtn").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "newsadd.aspx?pictype=" + pictype;
    })
</script>