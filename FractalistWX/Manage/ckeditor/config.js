﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

//CKEDITOR.editorConfig = function( config )
//{
//	// Define changes to default configuration here. For example:
//	// config.language = 'fr';
//	// config.uiColor = '#AADC6E';
//};
CKEDITOR.editorConfig = function (config) {  
    config.language = 'zh-cn';
    config.toolbar = 'Basic';
    config.toolbar = 'Full';
    config.toolbar_Full = [
    //['Source', '-', 'NewPage', 'Preview', '-', 'Templates'],  //,'Save'去掉保存按钮，这个已经被我注释掉

    //['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker','Scayt'],

  ['Source', '-', 'Undo', 'Redo', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', 'TextColor', 'BGColor'],

  //['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],

  //['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],

    //['Link','Unlink','Anchor'],

['Image', 'Table', 'SpecialChar'],

['Font', 'FontSize'],

//['Maximize', 'ShowBlocks', '-', 'About']
    ];

    config.toolbarCanCollapse = true;
    //工具栏默认是否展开
    config.toolbarStartupExpanded = true;
    // 当提交包含有此编辑器的表单时，是否自动更新元素内的数据
    config.autoUpdateElement = true;

};
//CKEDITOR.on('instanceReady', function (ev) {
//    // Ends self closing tags the HTML4 way, like <br>.
//    ev.editor.dataProcessor.htmlFilter.addRules(
//    {
//        elements:
//        {
//            $: function (element) {
//                // Output dimensions of images as width and height
//                if (element.name == 'img') {
//                    var style = element.attributes.style;

//                    if (style) {
//                        // Get the width from the style.
//                        var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
//                            width = match && match[1];

//                        // Get the height from the style.
//                        match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
//                        var height = match && match[1];

//                        if (width) {
//                            element.attributes.style = element.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, 'width: 100%');
//                        }

//                        if (height) {
//                            element.attributes.style = element.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, '');
//                        }
//                    }
//                }



//                if (!element.attributes.style)
//                    delete element.attributes.style;

//                return element;
//            }
//        }
//    });
//});

