﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Tool;
using FractalistWX.Dal;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace FractalistWX.Manage
{
    public partial class videoadd : System.Web.UI.Page
    {
        public int pictype = 0, isindex = 0, islink = 0, istuijian=0,picindex1=0;
        public string typetb1="";
        public string Information = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pictype = Tools.CheckVar(Request["type"], 0);
                int pid = Tools.CheckVar(Request["pid"], 0);
                mId.Value = pid.ToString();
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select * from wechat_video");
                strSql.Append(" where id=@pid");
                MySqlParameter[] parameters = {
				    new MySqlParameter("@pid", MySqlDbType.Int32)               
                };
                parameters[0].Value = pid;
                DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    typeTB.SelectedValue = dt.Rows[0]["videotype"].ToString();                   
                    islinkTB.SelectedValue = dt.Rows[0]["islink"].ToString();
                    linkurlTB.Text = dt.Rows[0]["linkurl"].ToString();
                    islink = Convert.ToInt32(dt.Rows[0]["islink"].ToString());
                    istuijian = Convert.ToInt32(dt.Rows[0]["istuijian"].ToString());
                    titleTB.Text = dt.Rows[0]["title"].ToString();
                    selPic.Value = dt.Rows[0]["pic"].ToString();
                    selPic1.Value = dt.Rows[0]["indexpic"].ToString();
                    selvideo.Value = dt.Rows[0]["video"].ToString();
                    picindex1 = string.IsNullOrEmpty(dt.Rows[0]["picindex"].ToString())?0:Convert.ToInt32(dt.Rows[0]["picindex"].ToString());
                    typetb1 = dt.Rows[0]["videotype"].ToString();
                    isindex = dt.Rows[0]["isindex"].ToString() == "1" ? 1 : 0;
                }
            }

        }
        protected void comfirm_BT_Click(object sender, EventArgs e)
        {
            errorLB.Text = "";
            int isindex1 = Tools.CheckVar(Request["isindex"], 0);
            int istuijian1 = Tools.CheckVar(Request["istuijian"], 0);
            int showindex = Tools.CheckVar(Request["showindex"], 0);
            if (titleTB.Text == "")
            {
                errorLB.Text = "标题不能为空";
                return;
            }
            if (islinkTB.SelectedValue == "0" && linkurlTB.Text == "")
            {
                errorLB.Text = "链接地址不能为空";
                return;
            }
            if (islinkTB.SelectedValue == "1")
            {
                if (Request["selvideo"] == "")
                {
                    errorLB.Text = "视频地址不能为空";
                    return;
                }
            }
            int m_Id = int.Parse(mId.Value);
            if (m_Id > 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_video set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic,");
                strSql.Append("videotype=@videotype,");
                strSql.Append("isindex=@isindex,");
                strSql.Append("islink=@islink,");
                strSql.Append("linkurl=@linkurl,");
                strSql.Append("video=@video,");
                strSql.Append("istuijian=@istuijian,");            
                strSql.Append("indexpic=@indexpic,");
                strSql.Append("picindex=@picindex");
                strSql.Append(" where id=@id ");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),
					new MySqlParameter("@videotype", MySqlDbType.Int32,4),
					new MySqlParameter("@isindex", MySqlDbType.Int32,4),
					new MySqlParameter("@islink", MySqlDbType.Int32,4),
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@video", MySqlDbType.VarChar,500),
                    new MySqlParameter("@istuijian", MySqlDbType.Int32,4),
                    new MySqlParameter("@indexpic", MySqlDbType.VarChar,200),
					new MySqlParameter("@picindex", MySqlDbType.Int32,4),
					new MySqlParameter("@id", MySqlDbType.Int32,11)};
                parameters[0].Value = Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                parameters[2].Value = this.typeTB.SelectedValue;
                parameters[3].Value = isindex1;                
                parameters[4].Value = islinkTB.SelectedValue;
                parameters[5].Value = Tools.SafeSql(this.linkurlTB.Text);
                parameters[6].Value = Tools.SafeSql(selvideo.Value);
                parameters[7].Value = istuijian1;
                parameters[8].Value = Tools.SafeSql(this.selPic1.Value);
                parameters[9].Value = showindex;
                parameters[10].Value = m_Id;

                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                errorLB.Text = "更新成功";
                CreatHtml CH = new CreatHtml();
                CH.WriteP_indexFile();
                CH.WriteP_videoTJFile();
                CH.WriteM_videoTJFile();
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_video(");
                strSql.Append("title,pic,videotype,isindex,islink,linkurl,video,istuijian,indexpic,picindex)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic,@videotype,@isindex,@islink,@linkurl,@video,@istuijian,@indexpic,@picindex)");
                MySqlParameter[] parameters = {					
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),
					new MySqlParameter("@videotype", MySqlDbType.Int32,4),
					new MySqlParameter("@isindex", MySqlDbType.Int32,4),
					new MySqlParameter("@islink", MySqlDbType.Int32,4),
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@video", MySqlDbType.VarChar,500),
                    new MySqlParameter("@istuijian", MySqlDbType.Int32,4),
                    new MySqlParameter("@indexpic", MySqlDbType.VarChar,200),
					new MySqlParameter("@picindex", MySqlDbType.Int32,4)};

                parameters[0].Value = Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                parameters[2].Value = this.typeTB.SelectedValue;
                parameters[3].Value = isindex1;
                parameters[4].Value = islinkTB.SelectedValue;
                parameters[5].Value = Tools.SafeSql(this.linkurlTB.Text);
                parameters[6].Value = Tools.SafeSql(this.selvideo.Value);
                parameters[7].Value = istuijian1;
                parameters[8].Value = Tools.SafeSql(this.selPic1.Value);
                parameters[9].Value = showindex;             


                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                errorLB.Text = "提交成功";
                CreatHtml CH = new CreatHtml();
                CH.WriteP_indexFile();
                CH.WriteP_videoTJFile();
                CH.WriteM_videoTJFile();

            }

        }
    }
}