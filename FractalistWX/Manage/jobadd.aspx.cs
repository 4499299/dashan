﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Tool;
using FractalistWX.Dal;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;


namespace FractalistWX.Manage
{
    public partial class jobadd : System.Web.UI.Page
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                int pid = Tools.CheckVar(Request["pid"], 0);
                mId.Value = pid.ToString();
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select * from wechat_job");
                strSql.Append(" where id=@pid");
                MySqlParameter[] parameters = {
				    new MySqlParameter("@pid", MySqlDbType.Int32)               
                };
                parameters[0].Value = pid;
                DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
                if (dt != null && dt.Rows.Count > 0)
                {                    
                    titleTB.Text = dt.Rows[0]["title"].ToString();
                    selPic.Value = dt.Rows[0]["pic"].ToString();                   
                }
            }

        }
        protected void comfirm_BT_Click(object sender, EventArgs e)
        {
            errorLB.Text = "";          
            if (titleTB.Text == "")
            {
                errorLB.Text = "标题不能为空";
                return;
            }
            if (selPic.Value == "")
            {
                errorLB.Text = "图片不能为空";
                return;
            }
           
            int m_Id = int.Parse(mId.Value);
            if (m_Id > 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_job set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic");              
                strSql.Append(" where id=@id");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),					
					new MySqlParameter("@id", MySqlDbType.Int32,11)};
                parameters[0].Value = Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                parameters[2].Value = m_Id;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
                ch.WriteP_jobshowFiles();
                ch.WriteM_jobshowFiles();               
                errorLB.Text = "更新成功";

            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_job(");
                strSql.Append("title,pic)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic)");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200)};

                parameters[0].Value = Tools.SafeSql(titleTB.Text);
                parameters[1].Value = Tools.SafeSql(this.selPic.Value);
                CreatHtml ch = new CreatHtml();
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                ch.WriteP_jobshowFiles();
                ch.WriteM_jobshowFiles();                
                errorLB.Text = "提交成功";

            }

        }
    }
}