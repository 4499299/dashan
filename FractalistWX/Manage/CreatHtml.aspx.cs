﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using FractalistWX.Dal;

namespace FractalistWX.Manage
{
    public partial class CreatHtml : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (WriteP_indexFile())
                {
                    showtxt.Text += "PC首页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "PC首页生成失败!<br>";
                }

                if (WriteP_newsFile())
                {
                    showtxt.Text += "公司动态页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "公司动态页生成失败!<br>";
                }

                if (WriteP_pronewsFile())
                {
                    showtxt.Text += "产品动态页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "产品动态页生成失败!<br>";
                }

                if (WriteP_economyFile())
                {
                    showtxt.Text += "行业动态页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "行业动态页生成失败!<br>";
                }

                if (WriteP_videoTJFile())
                {
                    showtxt.Text += "视频推荐页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "视频推荐页生成失败!<br>";
                }

                if (WriteP_videolistFile())
                {
                    showtxt.Text += "视频列表页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "视频列表页生成失败!<br>";
                }

                if (WriteP_prolistFiles())
                {
                    showtxt.Text += "产品页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "产品页生成失败!<br>";
                }

                if (WriteP_prolist1Files())
                {
                    showtxt.Text += "瓶装水页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "瓶装水页生成失败!<br>";
                }

                if (WriteP_prolist2Files())
                {
                    showtxt.Text += "桶装水页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "桶装水页生成失败!<br>";
                }

                if (WriteP_showwriteFiles())
                {
                    showtxt.Text += "留言页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "留言页生成失败!<br>";
                }

                if (WriteP_jobshowFiles())
                {
                    showtxt.Text += "招聘页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "招聘页生成失败!<br>";
                }

                if (WriteM_indexFile())
                {
                    showtxt.Text += "MO首页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO首页生成失败!<br>";
                }

                if (WriteM_newsFile())
                {
                    showtxt.Text += "MO新闻动态生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO新闻动态生成失败!<br>";
                }

                if (WriteM_pronewsFile())
                {
                    showtxt.Text += "MO产品动态生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO产品动态生成失败!<br>";
                }

                if (WriteM_economyFile())
                {
                    showtxt.Text += "MO行业动态生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO行业动态生成失败!<br>";
                }

                if (WriteM_videoTJFile())
                {
                    showtxt.Text += "MO视频推荐页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO视频推荐页生成失败!<br>";
                }

                if (WriteM_videolistFile())
                {
                    showtxt.Text += "MO视频页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO视频页生成失败!<br>";
                }

                if (WriteM_prolistFiles())
                {
                    showtxt.Text += "MO产品页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO产品页生成失败!<br>";
                }

                if (WriteM_prolist1Files())
                {
                    showtxt.Text += "MO瓶装水页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO瓶装水页生成失败!<br>";
                }

                if (WriteM_prolist2Files())
                {
                    showtxt.Text += "MO桶装水页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO桶装水页生成失败!<br>";
                }

                if (WriteM_showwriteFiles())
                {
                    showtxt.Text += "MO留言页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO留言页生成失败!<br>";
                }

                if (WriteM_jobshowFiles())
                {
                    showtxt.Text += "MO招聘页生成成功!<br>";
                }
                else
                {
                    showtxt.Text += "MO招聘页生成失败!<br>";
                }
            }
        }
        public bool WriteP_indexFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_index.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "P_index.html";
            //banner
            string bannertxt = "", bannerpic = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,linkurl from wechat_pic where pictype=0 and isshow=1 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                bannertxt += "<li>" + i + 1 + "</li>";
                if (!string.IsNullOrEmpty(dt.Rows[i]["linkurl"].ToString()))
                {
                    bannerpic += "<li><a href=\"" + dt.Rows[i]["linkurl"] + "\"><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\"  alt=\"" + dt.Rows[i]["title"] + "\" /></a></li>";
                }
                else
                {
                    bannerpic += "<li><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\"  alt=\"" + dt.Rows[i]["title"] + "\" /></a></li>";
                }
            }

            //最新活动
            string stract = "";
            List<string> actlist = SQLTool.GetListField("select title,pic,isclick,linkurl from wechat_activity where pictype=0 order by id desc limit 1", 4);
            if (actlist.Count > 0)
            {
                if (string.IsNullOrEmpty(actlist[3]))
                {
                    stract = "<img src=\"/upfiles/" + actlist[1] + "\"  width=\"264\" height=\"133\" alt=\"" + actlist[0] + "\" />";
                }
                else
                {
                    stract = "<a href=\"" + actlist[3] + "\"><img src=\"/upfiles/" + actlist[1] + "\"  width=\"264\" height=\"133\" alt=\"" + actlist[0] + "\" /></a>";
                }
            }

            //最新视频
            string strvideo = "";
            List<string> videolist = SQLTool.GetListField("select title,indexpic,islink,linkurl,video from wechat_video where videotype=0 and isindex=1 order by id desc limit 1", 5);
            if (videolist.Count > 0)
            {
                if (videolist[2] == "0")
                {
                    strvideo = "<a href=\"" + videolist[3] + "\" target=_blank><img src=\"/upfiles/" + videolist[1] + "\" width=\"330\" height=\"114\" alt=\"" + videolist[0] + "\" /></a>";
                }
                else
                {
                    strvideo = "<img src=\"/upfiles/" + videolist[1] + "\" width=\"330\" height=\"114\" alt=\"" + videolist[0] + "\" onclick=\"showvideo('" + videolist[4] + "')\" />";
                }
            }

            //新闻
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,title,islink,linkurl,stamp from wechat_news where isindex=1 and newstype=0 order by id desc limit 0,4");
            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    strnews += "<li><div class=\"newsimg\"><img src=\"/images/dian.png\" alt=\"\" /></div><div class=\"newstxt\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></div><div class=\"newsdate\">" + Convert.ToDateTime(dtnews.Rows[i]["stamp"].ToString()).ToString("yyyy-MM-dd") + "</div></li>";
                }
                else
                {
                    strnews += "<li><div class=\"newsimg\"><img src=\"/images/dian.png\" alt=\"\" /></div><div class=\"newstxt\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></div><div class=\"newsdate\">" + Convert.ToDateTime(dtnews.Rows[i]["stamp"].ToString()).ToString("yyyy-MM-dd") + "</div></li>";
                }
            }

            // 替换内容        
            str = str.Replace("$bannertxt$", bannertxt);
            str = str.Replace("$bannerpic$", bannerpic);
            str = str.Replace("$activitytxt$", stract);
            str = str.Replace("$videotxt$", strvideo);
            str = str.Replace("$strnews$", strnews);
            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteP_newsFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_news.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 10;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=0 and newstype=0");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=0 and newstype=0 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/images/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"173\" height=\"148\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank><img src=\"/images/nopic.jpg\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "P_news" + page + ".html";
                    if (page == 1)
                    {
                       // string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        string strpage = "";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 10 == 9 && i > 0)
                {
                    htmlfilename = "P_news" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "P_news1.html";               
                string strpage = "";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteP_pronewsFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_pronews.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 10;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=1 and newstype=0");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=1 and newstype=0 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/images/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"173\" height=\"148\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank><img src=\"/images/nopic.jpg\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "P_pronews" + page + ".html";
                    if (page == 1)
                    {
                       // string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        string strpage = "";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 10 == 9 && i > 0)
                {
                    htmlfilename = "P_pronews" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "P_pronews1.html";
                // string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                string strpage = "";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteP_economyFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/P_economy.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 10;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=2 and newstype=0");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=2 and newstype=0 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/images/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"173\" height=\"148\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank><img src=\"/images/nopic.jpg\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"173\" height=\"148\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"overflow: hidden;height: 40px;\"><a href=\"shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "P_economy" + page + ".html";
                    if (page == 1)
                    {
                        //string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        string strpage = "";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 10 == 9 && i > 0)
                {
                    htmlfilename = "P_economy" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "P_economy1.html";
                // string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                string strpage = "";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteP_videoTJFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_videolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "p_videolist0.html";

            //最新视频
            string strvideo = "", strpage = "";
            DataTable videodt = MySQLHelper.GetDataTable(@"(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=0 and istuijian=1 and picindex>0 and picindex=1  order by id desc limit 1)
UNION 
(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=0 and istuijian=1 and picindex>0 and picindex=2  order by id desc limit 1)
UNION 
(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=0 and istuijian=1 and picindex>0 and picindex=3  order by id desc limit 1)
UNION 
(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=0 and istuijian=1 and picindex>0 and picindex=4  order by id desc limit 1)
UNION 
(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=0 and istuijian=1 and picindex>0 and picindex=5  order by id desc limit 1)");
            if (videodt != null)
            {
                for (int i = 0; i < videodt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li  style=\"width:630px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"630\" height=\"317\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                        else
                        {
                            strvideo += "<li  style=\"width:630px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"630\" height=\"317\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                    }
                    else if (i == 1)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li  style=\"width:376px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\"  target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"376\" height=\"317\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                        else
                        {
                            strvideo += "<li  style=\"width:376px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"376\" height=\"317\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                    }
                    else if (i == 2)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li  style=\"width:365px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"365\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                        else
                        {
                            strvideo += "<li  style=\"width:365px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"365\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                    }
                    else if (i == 3)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li  style=\"width:365px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"365\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                        else
                        {
                            strvideo += "<li  style=\"width:365px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"365\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                    }
                    else if (i == 4)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li  style=\"width:258px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"258\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                        else
                        {
                            strvideo += "<li  style=\"width:258px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"258\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                        }
                    }
                }
            }
            if (SQLTool.GetFirstIntField(" select count(id) from  wechat_video  where id not in (select id from wechat_video where videotype=0 and istuijian=1) and videotype=0") > 0)
            {
                strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=1\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";
            }
            else
            {
                strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
            }


            // 替换内容        
            str = str.Replace("$strvideo$", strvideo);
            str = str.Replace("$newspage$", strpage);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        private bool WriteP_videolistFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_video.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 8;
            int count = SQLTool.GetFirstIntField("select count(id) from  wechat_video  where id not in (select id from wechat_video where videotype=0 and istuijian=1) and videotype=0");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable videodt = MySQLHelper.GetDataTable("select title,pic,islink,linkurl,video from wechat_video where videotype=0  and id not in (select id from wechat_video where videotype=0 and istuijian=1) order by id desc");

            for (int i = 0; i < videodt.Rows.Count; i++)
            {
                if (videodt.Rows[i]["islink"].ToString() == "0")
                {
                    strnews += "<li  style=\"width:258px;\" onclick=\"javascript:window.window.open('" + videodt.Rows[i]["linkurl"] + "')\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"258\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                }
                else
                {
                    strnews += "<li  style=\"width:258px;\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\" target=\"_blank\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\"  width=\"258\" height=\"191\" /><div class=\"pictxt\"> </div><div class=\"pictxt2\">" + videodt.Rows[i]["title"] + "</div></li>";
                }

                if (i == (videodt.Rows.Count - 1))
                {
                    htmlfilename = "P_videolist" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><a href=\"videolist.aspx\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"videolist.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 8 == 7 && i > 0)
                {
                    htmlfilename = "P_videolist" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><a href=\"videolist.aspx\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"videolist.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            return true;
        }

        public bool WriteP_prolistFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "P_prolist.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=0 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                protxt += "<li>" + i + 1 + "</li>";
                proimg += "<li><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"1920\" height=\"628\" alt=\"" + dt.Rows[i]["prodesc"] + "\" /></a></li>";

            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);
            str = str.Replace("$protxt$", protxt);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteP_prolist1Files()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "P_prolist1.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=0 and classname=1 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                protxt += "<li>" + i + 1 + "</li>";
                proimg += "<li><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"1920\" height=\"628\" alt=\"" + dt.Rows[i]["prodesc"] + "\" /></a></li>";

            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);
            str = str.Replace("$protxt$", protxt);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteP_prolist2Files()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "P_prolist2.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=0 and classname=2 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                protxt += "<li>" + i + 1 + "</li>";
                proimg += "<li><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"1920\" height=\"628\" alt=\"" + dt.Rows[i]["prodesc"] + "\" /></a></li>";

            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);
            str = str.Replace("$protxt$", protxt);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteP_showwriteFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_showwrite.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 3;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_review where state=1 and booktype=0");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //留言列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select reviewtype,nickname,mobile,email,content,ntime,recontent  from wechat_review where state=1 and booktype=0 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                var hui = "";
                if (string.IsNullOrEmpty(dtnews.Rows[i]["recontent"].ToString()))
                {
                    hui = "";
                }
                else
                {
                    hui = "<tr>" +
                            "<td  style=\"height:55px;  line-height:55px;  width:70px;\">回复内容：</td>" +
                            "<td>" +
                                "<div  class=\"showrad\">" + dtnews.Rows[i]["recontent"] + "<br /></div>" +
                            "</td>" +
                        "</tr>";
                }

                strnews += " <li>" +
                           "<table cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"margin:0 auto;\">" +
                                "<tr>" +
                                  "<td style=\"height:55px; line-height:55px; width:70px;color:#196fc0;\">留言内容：</td><td><div  class=\"showrad\" style=\"color:#196fc0;\">" + dtnews.Rows[i]["content"] + "</div></td><td style=\"height:55px; line-height:55px; width:120px; padding-left:30px;\"><p>留言人：" + dtnews.Rows[i]["nickname"] + "</p><p>" + Convert.ToDateTime(dtnews.Rows[i]["ntime"]).ToString("yyyy/MM/dd") + "</p></td>" +
                                 "</tr>" +hui+                                 
                           "</table>" +
                           "<br />" +
                           "<div style=\"border-bottom:1px dotted #202020;\"></div>" +
                       "</li>";

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "P_showwrite" + page + ".html";
                    if (page == 1)
                    {                      
                        string strpage="<table  style=\"margin: 0 auto;\">"+
                                        "<tr>"+
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                            "<td style=\"padding-right:20px;\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></td>"+                               
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">首页</div></td>"+
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</div></td>"+
                                            "<td style=\"margin-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></td>"+
                                        "</tr>"+
                                    "</table>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                       "<tr>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                           "<td style=\"padding-right:20px;\"><a href=\"showwrite.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></a></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\"><a href=\"showwrite.aspx\">首页</a></div></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</div></td>" +
                                           "<td style=\"margin-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></td>" +
                                       "</tr>" +
                                   "</table>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "P_showwrite" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                       "<tr>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">"+count+"条留言</div></td>" +
                                           "<td style=\"padding-right:20px;\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">首页</div></td>" +
                                           "<td><a href=\"showwrite.aspx?page=" + totalpage + "\"><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</a></div></td>" +
                                           "<td style=\"margin-left:20px;\"><a href=\"showwrite.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></a></td>" +
                                       "</tr>" +
                                   "</table>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {                    

                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                            "<tr>" +
                                          "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                          "<td style=\"padding-right:20px;\"><a href=\"showwrite.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></a></td>" +
                                          "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\"><a href=\"showwrite.aspx\">首页</a></div></td>" +
                                          "<td><a href=\"showwrite.aspx?page=" + totalpage + "\"><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</a></div></td>" +
                                          "<td style=\"margin-left:20px;\"><a href=\"showwrite.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></a></td>" +
                                      "</tr>" +
                                  "</table>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "P_showwrite1.html";
                string strpage = "";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteP_jobshowFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/p_jobshow.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "p_jobshow.html";

            string strjob = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic from wechat_job  order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strjob += "<li><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"444\" height=\"170\" alt=\"" + dt.Rows[i]["title"] + "\" /></li>";
            }

            // 替换内容        
            str = str.Replace("$joblist$", strjob);
            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteM_indexFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_index.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "m_index.html";
            //banner
            string bannertxt = "", bannerpic = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,linkurl from wechat_pic where pictype=1 and isshow=1 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (!string.IsNullOrEmpty(dt.Rows[i]["linkurl"].ToString()))
                {
                    bannerpic += "<div class=\"swiper-slide\">" +
                                "<a href=\"" + dt.Rows[i]["linkurl"] + "\"><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></a>" +
                                "</div>";
                }
                else
                {
                    bannerpic += "<div class=\"swiper-slide\">" +
                                "<img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" />" +
                                "</div>";
                }
            }

            //最新活动
            string stract = "";
            List<string> actlist = SQLTool.GetListField("select title,pic,isclick,linkurl from wechat_activity where pictype=1 order by id desc limit 1", 4);
            if (actlist.Count > 0)
            {
                if (string.IsNullOrEmpty(actlist[3]))
                {
                    stract = "<div class=\"actividiv\"></div>";
                }
                else
                {
                    stract = "<a href=\"" + actlist[3] + "\"><div class=\"actividiv\"></div></a>";
                }
            }

            //最新视频
            string strvideo = "";
            List<string> videolist = SQLTool.GetListField("select title,pic,islink,linkurl,video from wechat_video where videotype=1 and isindex=1 order by id desc limit 1", 5);
            if (videolist.Count > 0)
            {
                if (videolist[2] == "0")
                {
                    strvideo = "<a href=\"" + videolist[3] + "\" target=_blank><div class=\"videodiv\"></div></a>";
                }
                else
                {
                    strvideo = "<div class=\"videodiv\" alt=\"" + videolist[0] + "\" onclick=\"showvideo('" + videolist[4] + "')\" ></div>";
                }
            }

            // 替换内容        
            str = str.Replace("$bannertxt$", bannerpic);
            str = str.Replace("$activitytxt$", stract);
            str = str.Replace("$videotxt$", strvideo);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteM_newsFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/M_news.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 4;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=0 and newstype=1");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=0 and newstype=1 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\"><img src=\"/img/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"100%\"  /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" ><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/img/nopic.jpg\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "M_news" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "M_news" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"news.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"news.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "M_news1.html";
                string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteM_pronewsFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_pronews.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 4;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=1 and newstype=1");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=1 and newstype=1 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\"><img src=\"/img/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"100%\"  /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" ><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/img/nopic.jpg\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "M_pronews" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"pronews.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "M_pronews" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"pronews.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"pronews.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"pronews.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "M_pronews1.html";
                string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteM_economyFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_economy.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 4;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_news where classtype=2 and newstype=1");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select id,pic,title,islink,linkurl,stamp from wechat_news where classtype=2 and newstype=1 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                if (dtnews.Rows[i]["islink"].ToString() == "0")
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\"><img src=\"/img/nopic.jpg\" alt=\"" + dtnews.Rows[i]["title"] + "\"  width=\"100%\"  /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" ><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\" alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"" + dtnews.Rows[i]["linkurl"] + "\" target=_blank>" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dtnews.Rows[i]["pic"].ToString()))
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/img/nopic.jpg\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                    else
                    {
                        strnews += "<li><p class=\"newspic\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\"><img src=\"/upfiles/" + dtnews.Rows[i]["pic"] + "\"  width=\"100%\"  alt=\"" + dtnews.Rows[i]["title"] + "\" /></a></p><p style=\"margin-bottom:.5rem;height:2rem;overflow: hidden;\"><a href=\"M_shownews_" + dtnews.Rows[i]["id"] + ".html\">" + dtnews.Rows[i]["title"] + "</a></p></li>";
                    }
                }

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "M_economy" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"economy.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "M_economy" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"economy.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"economy.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"economy.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            if (dtnews == null || dtnews.Rows.Count == 0)
            {
                htmlfilename = "M_economy1.html";
                string strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteM_videoTJFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/M_videolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "M_videolist0.html";

            //最新视频
            string strvideo = "", strpage = "";
            DataTable videodt = MySQLHelper.GetDataTable(@"(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=1 and istuijian=1 and picindex>0 and picindex=1  order by id desc limit 1)
UNION ,
(select id, title,pic,islink,linkurl,video,picindex from wechat_video where videotype=1 and istuijian=1 and picindex>0 and picindex=2  order by id desc limit 1)
UNION 
(select id ,title,pic,islink,linkurl,video,picindex from wechat_video where videotype=1 and istuijian=1 and picindex>0 and picindex=3  order by id desc limit 1)");
            if (videodt != null)
            {
                for (int i = 0; i < videodt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li class=\"videoli\" onclick=\"javascript:window.location.href='" + videodt.Rows[i]["linkurl"] + "'\">" +
                                        "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                        "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                        "</li>";
                        }
                        else
                        {
                            strvideo += "<li class=\"videoli\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\">" +
                                      "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                      "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                      "</li>";
                        }
                    }
                    else if (i == 1)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li class=\"videoli2\" onclick=\"javascript:window.location.href='" + videodt.Rows[i]["linkurl"] + "'\">" +
                                        "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                        "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                        "</li>";
                        }
                        else
                        {
                            strvideo += "<li class=\"videoli2\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\">" +
                                      "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                      "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                      "</li>";
                        }
                    }
                    else if (i == 2)
                    {
                        if (videodt.Rows[i]["islink"].ToString() == "0")
                        {
                            strvideo += "<li class=\"videoli3\" onclick=\"javascript:window.location.href='" + videodt.Rows[i]["linkurl"] + "'\">" +
                                        "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                        "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                        "</li>";
                        }
                        else
                        {
                            strvideo += "<li class=\"videoli3\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\">" +
                                      "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                      "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                      "</li>";
                        }
                    }
                }
            }
            if (SQLTool.GetFirstIntField(" select count(id) from  wechat_video  where id not in (select id from wechat_video where videotype=1 and istuijian=1) and videotype=1") > 0)
            {
                strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=1\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";
            }
            else
            {
                strpage = "<span><img src=\"/images/pagepre.jpg\" alt=\"\" /></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
            }


            // 替换内容        
            str = str.Replace("$strvideo$", strvideo);
            str = str.Replace("$newspage$", strpage);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        private bool WriteM_videolistFile()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_video.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 4;
            int count = SQLTool.GetFirstIntField("select count(id) from  wechat_video  where id not in (select id from wechat_video where videotype=1 and istuijian=1) and videotype=1");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //新闻列表
            string strnews = "";
            DataTable videodt = MySQLHelper.GetDataTable("select title,pic,islink,linkurl,video from wechat_video where videotype=1  and id not in (select id from wechat_video where videotype=1 and istuijian=1) order by id desc");

            for (int i = 0; i < videodt.Rows.Count; i++)
            {
                if (videodt.Rows[i]["islink"].ToString() == "0")
                {
                    strnews += "<li class=\"videoli2\" onclick=\"javascript:window.location.href='" + videodt.Rows[i]["linkurl"] + "'\">" +
                                "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                                "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                                "</li>";
                }
                else
                {
                    strnews += "<li class=\"videoli2\" onclick=\"showvideo('" + videodt.Rows[i]["video"] + "');\">" +
                              "<div class=\"newspic\"><img src=\"/upfiles/" + videodt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>" +
                              "<div class=\"videotxt\">" + videodt.Rows[i]["title"] + "</div>" +
                              "</li>";
                }
                if (i == (videodt.Rows.Count - 1))
                {
                    htmlfilename = "M_videolist" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><a href=\"videolist.aspx\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";
                        str1 = str.Replace("$strvideo$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"videolist.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></span>";

                        str1 = str.Replace("$strvideo$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "M_videolist" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<span><a href=\"videolist.aspx\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$strvideo$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<span><a href=\"videolist.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre_1.jpg\" alt=\"\" /></a></span><span style=\"padding-left:20px;\"><a href=\"videolist.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext_1.jpg\" alt=\"\" /></a></span>";

                        str1 = str.Replace("$strvideo$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }
            return true;
        }

        public bool WriteM_prolistFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "M_prolist.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=1 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {                 
                proimg += "<div class=\"swiper-slide\"><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>";
            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteM_prolist1Files()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "M_prolist1.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=1 and classname=1 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                proimg += "<div class=\"swiper-slide\"><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>";
            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteM_prolist2Files()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_prolist.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "M_prolist2.html";
            //banner
            string proimg = "", protxt = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic,prodesc from wechat_pro where protype=1 and classname=2 order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                proimg += "<div class=\"swiper-slide\"><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></div>";
            }

            // 替换内容        
            str = str.Replace("$propic$", proimg);

            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }

        public bool WriteM_showwriteFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_showwrite.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "", str1 = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "";
            int pagesize = 4;
            int count = SQLTool.GetFirstIntField("select count(id) from wechat_review where state=1 and booktype=1");
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            int page = 1;
            //留言列表
            string strnews = "";
            DataTable dtnews = MySQLHelper.GetDataTable("select reviewtype,nickname,mobile,email,content,ntime,recontent  from wechat_review where state=1 and booktype=1 order by id desc");

            for (int i = 0; i < dtnews.Rows.Count; i++)
            {
                var hui = "";
                if (string.IsNullOrEmpty(dtnews.Rows[i]["recontent"].ToString()))
                {
                    hui = "";
                }
                else
                {
                    hui = "<div class=\"wtitle\">回复内容</div> " +
                             "<div  class=\"showrad2\">" + dtnews.Rows[i]["recontent"] + "</div>  ";
                }
                strnews += "<ul>" +
                        "<li>" +
                             "<div class=\"wtitle\" style=\"color:#196fc0;\">留言内容</div>" +
                             "<div  class=\"showrad\">" + dtnews.Rows[i]["content"] + "</div>" +
                             "<div class=\"showname\">" +
                                "<p>留言人： " + dtnews.Rows[i]["nickname"] + "</p>" +
                                "<p>" + Convert.ToDateTime(dtnews.Rows[i]["ntime"]).ToString("yyyy/MM/dd") + "</p>" +
                             "</div>" +
                             "<div class=\"clear\"></div>" +hui+                            
                             "<br><div style=\"border-bottom:1px dotted #202020;\"></div>" +
                        "</li>" +
                    "</ul>";

                if (i == (dtnews.Rows.Count - 1))
                {
                    htmlfilename = "M_showwrite" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                        "<tr>" +
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                            "<td style=\"padding-right:20px;\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></td>" +
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">首页</div></td>" +
                                            "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</div></td>" +
                                            "<td style=\"margin-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></td>" +
                                        "</tr>" +
                                    "</table>";
                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                       "<tr>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                           "<td style=\"padding-right:20px;\"><a href=\"showwrite.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></a></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\"><a href=\"showwrite.aspx\">首页</a></div></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</div></td>" +
                                           "<td style=\"margin-left:20px;\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></td>" +
                                       "</tr>" +
                                   "</table>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                }
                else if (i % 4 == 3 && i > 0)
                {
                    htmlfilename = "M_showwrite" + page + ".html";
                    if (page == 1)
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                       "<tr>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                           "<td style=\"padding-right:20px;\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></td>" +
                                           "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">首页</div></td>" +
                                           "<td><a href=\"showwrite.aspx?page=" + totalpage + "\"><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</a></div></td>" +
                                           "<td style=\"margin-left:20px;\"><a href=\"showwrite.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></a></td>" +
                                       "</tr>" +
                                   "</table>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    else
                    {
                        string strpage = "<table  style=\"margin: 0 auto;\">" +
                                            "<tr>" +
                                          "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">" + count + "条留言</div></td>" +
                                          "<td style=\"padding-right:20px;\"><a href=\"showwrite.aspx?page=" + (page - 1) + "\"><img src=\"/images/pagepre.jpg\" alt=\"\" /></a></td>" +
                                          "<td><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\"><a href=\"showwrite.aspx\">首页</a></div></td>" +
                                          "<td><a href=\"showwrite.aspx?page=" + totalpage + "\"><div class=\"pagecont\" style=\"margin-right:20px; border:1px solid #c3cacf;\">尾页</a></div></td>" +
                                          "<td style=\"margin-left:20px;\"><a href=\"showwrite.aspx?page=" + (page + 1) + "\"><img src=\"/images/pagenext.jpg\" alt=\"\" /></a></td>" +
                                      "</tr>" +
                                  "</table>";

                        str1 = str.Replace("$newslist$", strnews);
                        str1 = str1.Replace("$newspage$", strpage);
                        try
                        {
                            sw = new StreamWriter(path + htmlfilename, false, code);
                            sw.Write(str1);
                            sw.Flush();
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write(ex.Message);
                            HttpContext.Current.Response.End();
                        }
                        finally
                        {
                            sw.Close();
                        }
                        strnews = "";
                        strpage = "";
                    }
                    page++;
                }
            }            
            if (dtnews == null|| dtnews.Rows.Count==0)
            {
                htmlfilename = "M_showwrite1.html";
                string strpage = "";
                str1 = str.Replace("$newslist$", strnews);
                str1 = str1.Replace("$newspage$", strpage);
                try
                {
                    sw = new StreamWriter(path + htmlfilename, false, code);
                    sw.Write(str1);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write(ex.Message);
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    sw.Close();
                }
                strnews = "";
                strpage = "";
            }
            return true;
        }

        public bool WriteM_jobshowFiles()
        {
            string path = HttpContext.Current.Server.MapPath("../");
            Encoding code = Encoding.GetEncoding("UTF-8");
            // 读取模板文件
            string temp = HttpContext.Current.Server.MapPath("../template/m_jobshow.html");
            StreamReader sr = null;
            StreamWriter sw = null;
            string str = "";
            try
            {
                sr = new StreamReader(temp, code);
                str = sr.ReadToEnd(); // 读取文件
            }
            catch (Exception exp)
            {
                HttpContext.Current.Response.Write(exp.Message);
                HttpContext.Current.Response.End();
                sr.Close();
            }
            string htmlfilename = "M_jobshow.html";

            string strjob = "";
            DataTable dt = MySQLHelper.GetDataTable("select title,pic from wechat_job  order by id desc");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strjob += " <p><img src=\"/upfiles/" + dt.Rows[i]["pic"] + "\" width=\"100%\" alt=\"\" /></p>  ";
            }

            // 替换内容        
            str = str.Replace("$joblist$", strjob);
            // 写文件
            try
            {
                sw = new StreamWriter(path + htmlfilename, false, code);
                sw.Write(str);
                sw.Flush();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                HttpContext.Current.Response.End();
            }
            finally
            {
                sw.Close();
            }
            return true;
        }
    }
}