﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="joblist2.aspx.cs" Inherits="FractalistWX.Manage.joblist2" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../css/skin.css" rel="stylesheet" type="text/css" /> 
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
     <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
      <div style="float: left; display: none;">
                        <asp:UpdatePanel ID="UpdatePanelHide" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="Button_Hide" runat="server"
                                    Style="background-color: #cfd7e1; border: solid 1px #666666; padding-top: 0px;
                                    font-size: 12px; width: 65px; height: 22px; padding-left: 1px;" Text="查询" 
                                    onclick="Button_Hide_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
        <div class="mtitle"><span class="font30">招聘管理</span>－加盟信息</div>      
        <div class="imgTextListBoxAddpic">         
                <asp:UpdatePanel ID="UpdatePanelGridView" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            OnPageIndexChanging="GridView1_PageIndexChanging" 
                            OnRowCommand="GridView1_RowCommand" DataKeyNames="Id" PageSize="15"
                            EmptyDataText="暂无相关内容" EmptyDataRowStyle-Height="40px" 
                            EmptyDataRowStyle-HorizontalAlign="Center" 
                            EmptyDataRowStyle-VerticalAlign="Middle" Width="100%" Font-Size="12px" 
                            HeaderStyle-BackColor="#e1e5ee" HeaderStyle-Height="25px" BorderWidth="0" 
                            GridLines="None" AlternatingRowStyle-BackColor="#f2f2f2" RowStyle-Height="28px">
                            <Columns>
                              
                    <asp:TemplateField HeaderText="选择" HeaderStyle-CssClass="tdHeadNoneCss"  Visible=false>
                        <ItemTemplate>
                            <input type="checkbox" name="selck" id="selck_<%# Eval("Id").ToString()%>" value="<%# Eval("Id").ToString()%>" />
                        </ItemTemplate>
                        <ItemStyle Width="6%" CssClass="tdNoneCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="编号">
                        <ItemTemplate>
                            <%# Eval("id").ToString()%>
                        </ItemTemplate>
                        <ItemStyle Width="10%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="姓名">
                        <ItemTemplate>
                            <%# Eval("username").ToString()%>
                        </ItemTemplate>
                        <ItemStyle Width="10%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="电话">
                        <ItemTemplate>
                            <%# Eval("mobile").ToString()%>
                        </ItemTemplate>
                        <ItemStyle Width="10%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="email">
                        <ItemTemplate>
                            <%# Eval("email").ToString()%>
                        </ItemTemplate>
                        <ItemStyle Width="20%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="简历查看">
                        <ItemTemplate>
                            <a href="../UpFiles/source/<%# Eval("filepath").ToString()%>" target="_blank"><%# Eval("filepath").ToString()%></a>
                        </ItemTemplate>
                        <ItemStyle Width="20%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="日期">
                        <ItemTemplate>
                            <%# Eval("stamp").ToString()%>
                        </ItemTemplate>
                        <ItemStyle Width="20%" CssClass="tdCss" HorizontalAlign="Center" />
                    </asp:TemplateField>
                                <asp:TemplateField HeaderText="操作" >
                                    <ItemTemplate>
                                        <input type="button" name="mName" value="修改" style ="BACKGROUND-COLOR: #cfd7e1;border:solid 1px #666666;padding-top:0px; font-size:12px; width: 40px; height: 20px; padding-left:1px; cursor: pointer; display: none" />
                                        <input type="button" name="dName" value="删除" style ="BACKGROUND-COLOR: #cfd7e1;border:solid 1px #666666;padding-top:0px; font-size:12px; width: 40px; height: 20px; padding-left:1px; cursor: pointer;" />
                                    </ItemTemplate>
                                    <ItemStyle Width="14%" HorizontalAlign="Center" CssClass="tdCss" />
                                </asp:TemplateField>
                            </Columns>

                            <PagerTemplate>
                                <div style="width: 100%; margin-left: -1px; display: none">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center" width="6%" class="tdCss"><input type="checkbox" name="selck_all" id="selck_all" /></td>
                                            <td class="tdCss"><asp:Button ID="Button_Batch" CommandName="batch" Text="批量删除" style ="BACKGROUND-COLOR: #cfd7e1;border:solid 1px #666666;padding-top:0px; font-size:12px; width: 90px; height: 21px; padding-left:1px; cursor: pointer;" runat="server" /></td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 100%; text-align: center; padding-top: 8px;">
                                <asp:Label ID="lblPage" runat="server" Text='<%# "当前页" + (((GridView)Container.NamingContainer).PageIndex + 1)  + "/" + (((GridView)Container.NamingContainer).PageCount) + " 共有" + Count + "条" %> '></asp:Label>
                                <asp:LinkButton ID="lbnFirst" runat="Server" Text="首页" Enabled='<%# ((GridView)Container.NamingContainer).PageIndex != 0 %>'
                                    CommandName="Page" CommandArgument="First"></asp:LinkButton>
                                <asp:LinkButton ID="lbnPrev" runat="server" Text="上页" Enabled='<%# ((GridView)Container.NamingContainer).PageIndex != 0 %>'
                                    CommandName="Page" CommandArgument="Prev"></asp:LinkButton>
                                <asp:LinkButton ID="lbnNext" runat="Server" Text="下页" Enabled='<%# ((GridView)Container.NamingContainer).PageIndex != (((GridView)Container.NamingContainer).PageCount - 1) %>'
                                    CommandName="Page" CommandArgument="Next"></asp:LinkButton>
                                <asp:LinkButton ID="lbnLast" runat="Server" Text="尾页" Enabled='<%# ((GridView)Container.NamingContainer).PageIndex != (((GridView)Container.NamingContainer).PageCount - 1) %>'
                                    CommandName="Page" CommandArgument="Last"></asp:LinkButton>
                                到第 <asp:TextBox runat="server" ID="inPageNum" Width="20" Text="1"></asp:TextBox> 页
                                <asp:Button ID="Button_GO" CommandName="go" Text="go" style ="BACKGROUND-COLOR: #cfd7e1;border:solid 1px #666666;padding-top:0px; font-size:12px; width: 30px; height: 21px; padding-left:1px; cursor: pointer;" runat="server" />
                                </div>
                            </PagerTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>     
    </div>
    <asp:HiddenField ID="mId" Value="0" runat="server" />
    </form>
</body>
</html>
<script type="text/javascript">
    function ShowdivDisable() {
        document.getElementById('divDisable').style.height = document.body.scrollHeight + 'px';
        document.getElementById('divDisable').style.display = 'block';
    }

    function CloseOpenedDiv() {
        document.getElementById('divDisable').style.display = 'none';
        document.getElementById('modifyDiv').style.display = 'none';
    }

    $("input[name='dName']").live("click", function () {
        var obj = $(this).parent().parent().children("td");
        if (confirm('确定要删除吗？')) {
            $.post('Ajax.aspx?action=setStatejoin&Id=' + $.trim(obj.eq(1).text()) + '&t=' + Math.random(), {
        }, function (data) {
            if (data == "ok") {
                $("#<%=Button_Hide.ClientID %>").trigger("click");
            }
        })
    }
})