﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFile1.aspx.cs" Inherits="FractalistWX.Manage.UploadFile1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>文件上传</title>
    <script type="text/javascript">
        function SubmitValidator() {
            var filename = document.getElementById("FileUpload1").value;
            if (filename.lastIndexOf(".") < 1) {
                alert("请选择你要上传的文件！", null, null, "系统提示");
                return false;
            }
            var fileExtension = filename.substr(filename.lastIndexOf("."), filename.length - filename.lastIndexOf("."));
            fileExtension = fileExtension.toLowerCase();
            if (fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".bmp" || fileExtension == ".jpeg") {
                return true;
            }
            alert("对不起，请您上传的图片文件！", null, null, "系统提示");
            return false;
        }
    </script>
</head>
<body style="margin:0px; background-color:#fff;" >
    <form id="form1" runat="server">
    <div style="text-align:left">
        <asp:FileUpload ID="FileUpload1" ToolTip="文件须为图片格式" Width="180px" Height="35px" runat="server" />
        <asp:Button ID="btnUpload" runat="server" Text="上传"  style ="BACKGROUND-COLOR: #cfd7e1;border:solid 1px #666666;padding-top:0px; font-size:12px; width: 34px; height: 35px; padding-left:1px;" OnClick="btnUpload_Click" OnClientClick="return SubmitValidator()" />
    </div>
    </form>
</body>
</html>


