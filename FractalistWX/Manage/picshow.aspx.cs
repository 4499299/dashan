﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FractalistWX.Dal;
using FractalistWX.Tool;
using System.Text;
using MySql.Data.MySqlClient;

namespace FractalistWX.Manage
{
    public partial class picshow : System.Web.UI.Page
    {
        public DataTable dt;
        public int pictype;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                pictype = Tools.CheckVar(Request["type"], 0);
                if (!IsPostBack)
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("select * from wechat_pic");
                    strSql.Append(" where isshow=1 and pictype=@type");
                    MySqlParameter[] parameters = {
					new MySqlParameter("@type", MySqlDbType.Int32)
};
                    parameters[0].Value = pictype;

                    dt = MySQLHelper.GetDataTable(MySQLHelper.connectionString, CommandType.Text, strSql.ToString(), parameters);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}