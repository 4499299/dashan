﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FractalistWX.Tool;
using MySql.Data.MySqlClient;
using FractalistWX.Dal;
using System.Text;
using System.Data;
using System.IO;

namespace FractalistWX.Manage
{
    public partial class Ajax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string action = Tool.Tools.SafeSql(Tools.CheckVar(Request["action"], ""));
            if (action.Length == 0)
            {
                Response.Redirect("/index.aspx");
            }
            switch (Request["action"].ToString().Trim())
            {
                case "picpgsource":
                    GetPicPageSource();
                    break;
                case "ShowPicList":
                    ShowPicList();
                    break;
                case "AddPic":
                    AddPic();
                    break;
                case "DelPic":
                    DelPic();
                    break;
                case "ShowPic":
                    ShowPic();
                    break;
                case "ShowDelPic":
                    ShowDelPic();
                    break;
                case"EditPic":
                    EditPic();
                    break;
                case "AddActiviy":
                    AddActiviy();
                    break;
                case "NewsList":
                    NewsList();
                    break;
                case"DelNews":
                    DelNews();
                    break;
                case "VideoList":
                    VideoList();
                    break;
                case"DelVideo":
                    DelVideo();
                    break;
                case"ProList":
                    ProList();
                    break;
                case "DelPro":
                    DelPro();
                    break;
                case"AddPro":
                    AddPro();
                    break;
                case"JobList":
                    JobList();
                    break;
                case"DelJob":
                    DelJob();
                    break;
                case"BookList":
                    BookList();
                    break;
                case"BookRecontent":
                    BookRecontent();
                    break;
                case "DelBook":
                    DelBook();
                    break;
                case"ShowBook":
                    ShowBook();
                    break;
                case "setState":
                    setState();
                    break;
                case"setStatejoin":
                    setStatejoin();
                    break;
                case"EditstateBook":
                    EditstateBook();
                    break;
            }
        }
        private void EditstateBook()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            int state = Tools.CheckVar(Request["state"], 0);
            string strSql = "update wechat_review set state=@state where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),
				    new MySqlParameter("@state", MySqlDbType.Int16),	
                    };
            parameters[0].Value = pid;
            parameters[1].Value = state;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml CH = new CreatHtml();
            CH.WriteP_showwriteFiles();
            CH.WriteM_showwriteFiles();
        }
        private void ShowBook()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from wechat_review");
            strSql.Append(" where id=@pid");
            MySqlParameter[] parameters = {
				new MySqlParameter("@pid", MySqlDbType.Int32)               
            };
            parameters[0].Value = pid;
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            string strHtml = "{";
            strHtml += "\"pid\":\"" + dt.Rows[0]["id"] + "\",\"booktype\":\"" + dt.Rows[0]["booktype"] + "\",\"reviewtype\":\"" + dt.Rows[0]["reviewtype"].ToString() + "\",\"nickname\":\"" + dt.Rows[0]["nickname"] + "\",\"mobile\":\"" + dt.Rows[0]["mobile"] + "\",\"email\":\"" + dt.Rows[0]["email"] + "\",\"content\":\"" + dt.Rows[0]["content"] + "\",\"ntime\":\"" + dt.Rows[0]["ntime"] + "\",\"state\":\"" + dt.Rows[0]["state"] + "\"";
            strHtml += "}";
            Response.Write(strHtml);
            Response.End();
        }
        private void DelBook()
        {            
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "delete from  wechat_review where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml CH = new CreatHtml();
            CH.WriteP_showwriteFiles();
            CH.WriteM_showwriteFiles();
        }
        private void BookRecontent()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string recontent = Tools.SafeSql(Tools.CheckVar(Request["recontent"], ""));
            string strSql = "update wechat_review set state=1, recontent=@recontent where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),	
			        new MySqlParameter("@recontent", MySqlDbType.VarChar,800),			
                    };
            parameters[0].Value = pid;
            parameters[1].Value = recontent;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml CH = new CreatHtml();
            CH.WriteP_showwriteFiles();
            CH.WriteM_showwriteFiles();
        }
        private void BookList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int newstype = Tools.CheckVar(Request["wtype"], 6);
            int reviewtype = Tools.CheckVar(Request["reviewtype"], 6);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,nickname,mobile,email,ntime,content,recontent,state from wechat_review ");
            if (reviewtype==6)
            {
                if (newstype == 6)
                {
                    strSql.Append(" order by id desc  limit @pageindex, @pagesize");
                }
                else
                {
                    strSql.Append(" where booktype=@booktype   order by id desc  limit @pageindex, @pagesize");
                }
                
            }
            else
            {
                if (newstype == 6)
                {
                    strSql.Append(" where reviewtype=@reviewtype  order by id desc  limit @pageindex, @pagesize");
                }
                else
                {
                    strSql.Append(" where booktype=@booktype  and reviewtype=@reviewtype order by id desc  limit @pageindex, @pagesize");
                }
                
            }
            MySqlParameter[] parameters = {				
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32),
                 new MySqlParameter("@booktype", MySqlDbType.Int32),
                new MySqlParameter("@reviewtype", MySqlDbType.Int32)
            };
            parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[1].Value = pagesize;
            parameters[2].Value = newstype;
            parameters[3].Value = reviewtype;
            int count = 0;
           
            StringBuilder strSqlcount = new StringBuilder();
            strSqlcount.Append("select count(id) from wechat_review ");
            if (reviewtype == 6)
            {
                if (newstype == 6)
                {
                    strSqlcount.Append(" order by id desc ");
                }
                else
                {
                    strSqlcount.Append(" where booktype=@booktype   order by id desc ");
                }

            }
            else
            {
                if (newstype == 6)
                {
                    strSqlcount.Append(" where reviewtype=@reviewtype  order by id desc ");
                }
                else
                {
                    strSqlcount.Append(" where booktype=@booktype  and reviewtype=@reviewtype   order by id desc ");
                }

            }
            MySqlParameter[] parameterscount = {
                new MySqlParameter("@booktype", MySqlDbType.Int32),
                new MySqlParameter("@reviewtype", MySqlDbType.Int32)
            };
            parameterscount[0].Value = newstype;
            parameterscount[1].Value = reviewtype;

            count = SQLTool.GetFirstIntField(strSqlcount.ToString(),parameterscount);

            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"nickname\":\"" + dt.Rows[i]["nickname"].ToString() + "\",\"mobile\":\"" + dt.Rows[i]["mobile"].ToString() + "\",\"email\":\"" + dt.Rows[i]["email"].ToString() + "\",\"ntime\":\"" + dt.Rows[i]["ntime"].ToString() + "\",\"content\":\"" + dt.Rows[i]["content"].ToString().Replace("\\", "\\\\").Replace("\'", "\\\'").Replace("'", "\\\'").Replace("\t", " ").Replace("\r", " ").Replace("\n", "<br/>").Replace("\"", "'") + "\",\"state\":\"" + dt.Rows[i]["state"].ToString() + "\",\"recontent\":\"" + dt.Rows[i]["recontent"].ToString() + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void JobList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int newstype = Tools.CheckVar(Request["newstype"], 0);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,title,pic from wechat_job");
            strSql.Append(" order by id desc  limit @pageindex, @pagesize");
            MySqlParameter[] parameters = {				
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32)
            };           
            parameters[0].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[1].Value = pagesize;
            int count = 0;

            string strSqlcount = "select count(id) from wechat_job";

            count = SQLTool.GetFirstIntField(strSqlcount);
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void DelJob()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "delete from  wechat_job where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml ch = new CreatHtml();
            ch.WriteP_jobshowFiles();
            ch.WriteM_jobshowFiles();           
        }
        private void AddPro()
        {
            int pid = Tools.CheckVar(Request["pid"], 0);
            int typeTB = Tools.CheckVar(Request["typeTB"], 1);
            int classTB = Tools.CheckVar(Request["classTB"], 0); 
            string titleTB = Tools.SafeSql(Tools.CheckVar(Request["titleTB"], ""));
            string selPic = Tools.SafeSql(Tools.CheckVar(Request["selPic"], ""));
            string prodescTB = Tools.SafeSql(Tools.CheckVar(Request["prodescTB"], ""));

            if (pid == 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_pro(");
                strSql.Append("title,pic,protype,prodesc,classname)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic,@protype,@prodesc,@classname)");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),	
					new MySqlParameter("@protype", MySqlDbType.Int16,4),								
					new MySqlParameter("@prodesc", MySqlDbType.VarChar,500),
                    new MySqlParameter("@classname", MySqlDbType.Int16,4),	
                    };
                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = prodescTB;
                parameters[4].Value = classTB;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
                ch.WriteP_prolistFiles();
                ch.WriteP_prolist1Files();
                ch.WriteP_prolist2Files();
                ch.WriteM_prolistFiles();
                ch.WriteM_prolist1Files();
                ch.WriteM_prolist2Files();
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_pro set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic,");
                strSql.Append("protype=@protype,");
                strSql.Append("classname=@classname,");
                strSql.Append("prodesc=@prodesc");             
                strSql.Append(" where id=@id");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),					
					new MySqlParameter("@protype", MySqlDbType.Int16,4),
					new MySqlParameter("@prodesc", MySqlDbType.VarChar,500),
                    new MySqlParameter("@classname", MySqlDbType.Int16,4),	
					new MySqlParameter("@id", MySqlDbType.Int32,11)};

                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = prodescTB;          
                parameters[4].Value = classTB;
                parameters[5].Value = pid;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
                ch.WriteP_prolistFiles();
                ch.WriteP_prolist1Files();
                ch.WriteP_prolist2Files();
                ch.WriteM_prolistFiles();
                ch.WriteM_prolist1Files();
                ch.WriteM_prolist2Files();
            }          
        }
        private void DelPro()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "delete from  wechat_pro where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml ch = new CreatHtml();
            ch.WriteP_prolistFiles();
            ch.WriteP_prolist1Files();
            ch.WriteP_prolist2Files();
            ch.WriteM_prolistFiles();
            ch.WriteM_prolist1Files();
            ch.WriteM_prolist2Files();


        }
        private void ProList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int protype = Tools.CheckVar(Request["protype"], 0);
            int class_type = Tools.CheckVar(Request["class_type"], 0);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from wechat_pro");
            if (class_type == 0)
            {
                strSql.Append(" where protype=@type order by id desc  limit @pageindex, @pagesize");
            }
            else
            {
                strSql.Append(" where protype=@type  and classname=@class_type order by id desc  limit @pageindex, @pagesize");
            }
            MySqlParameter[] parameters = {
				new MySqlParameter("@type", MySqlDbType.Int32),
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32),
                 new MySqlParameter("@class_type", MySqlDbType.Int32)
            };
            parameters[0].Value = protype;
            parameters[1].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[2].Value = pagesize;
            parameters[3].Value = class_type;
            int count = 0;

            StringBuilder strSqlcount = new StringBuilder();
            strSqlcount.Append("select count(id) from wechat_pro");
            strSqlcount.Append(" where protype=@type");
            MySqlParameter[] parameterscount = {
				new MySqlParameter("@type", MySqlDbType.Int32)              
            };
            parameterscount[0].Value = protype;

            count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"protype\":\"" + dt.Rows[i]["protype"].ToString() + "\",\"classname\":\"" + dt.Rows[i]["classname"].ToString() + "\",\"prodesc\":\"" + dt.Rows[i]["prodesc"].ToString() + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();   
        }
        private void DelVideo()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "delete from  wechat_video where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            CreatHtml CH = new CreatHtml();
            CH.WriteP_videoTJFile();
            CH.WriteM_videoTJFile();
        }
        private void VideoList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int newstype = Tools.CheckVar(Request["videotype"], 0);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,title,pic from wechat_video");
            strSql.Append(" where videotype=@type order by id desc  limit @pageindex, @pagesize");
            MySqlParameter[] parameters = {
				new MySqlParameter("@type", MySqlDbType.Int32),
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32)
            };
            parameters[0].Value = newstype;
            parameters[1].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[2].Value = pagesize;
            int count = 0;

            StringBuilder strSqlcount = new StringBuilder();
            strSqlcount.Append("select count(id) from wechat_video");
            strSqlcount.Append(" where videotype=@type");
            MySqlParameter[] parameterscount = {
				new MySqlParameter("@type", MySqlDbType.Int32)              
            };
            parameterscount[0].Value = newstype;

            count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void DelNews()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "delete from  wechat_news where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);

            CreatHtml ch = new CreatHtml();            
            ch.WriteP_indexFile();
            ch.WriteP_newsFile();
            ch.WriteP_pronewsFile();
            ch.WriteP_economyFile();
            ch.WriteM_newsFile();
            ch.WriteM_pronewsFile();
            ch.WriteM_economyFile();
        }
        private void NewsList()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int newstype = Tools.CheckVar(Request["newstype"], 0);
            int classtype = Tools.CheckVar(Request["classtype"], 6);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,title,pic from wechat_news");
            if (classtype == 6)
            {
                strSql.Append(" where newstype=@type order by id desc  limit @pageindex, @pagesize");
            }
            else
            {
                strSql.Append(" where newstype=@type and classtype=@classtype order by id desc  limit @pageindex, @pagesize");
            }
            MySqlParameter[] parameters = {
				new MySqlParameter("@type", MySqlDbType.Int32),
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32),
                new MySqlParameter("@classtype", MySqlDbType.Int32),
            };
            parameters[0].Value = newstype;
            parameters[1].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[2].Value = pagesize;
            parameters[3].Value = classtype;
            int count = 0;

            StringBuilder strSqlcount = new StringBuilder();
            strSqlcount.Append("select count(id) from wechat_news");
            strSqlcount.Append(" where newstype=@type");
            MySqlParameter[] parameterscount = {
				new MySqlParameter("@type", MySqlDbType.Int32)              
            };
            parameterscount[0].Value = newstype;

            count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void AddActiviy()
        {
            int pid = Tools.CheckVar(Request["pid"], 0);
            int typeTB = Tools.CheckVar(Request["typeTB"], 1);
            int isclickTB = Tools.CheckVar(Request["isclickTB"], 1);
            int islinkTB = Tools.CheckVar(Request["islinkTB"], 1);
            string linkurlTB = Tools.SafeSql(Tools.CheckVar(Request["linkurlTB"], ""));
            string titleTB = Tools.SafeSql(Tools.CheckVar(Request["titleTB"], ""));
            string selPic = Tools.SafeSql(Tools.CheckVar(Request["selPic"], ""));

            if (pid == 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_activity(");
                strSql.Append("title,pic,pictype,isclick,islink,linkurl)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic,@pictype,@isclick,@islink,@linkurl)");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),
					new MySqlParameter("@pictype", MySqlDbType.Int32,4),
					new MySqlParameter("@isclick", MySqlDbType.Int32,4),
					new MySqlParameter("@islink", MySqlDbType.Int32,4),
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500)};
                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = isclickTB;
                parameters[4].Value = islinkTB;
                parameters[5].Value = linkurlTB;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
                ch.WriteP_indexFile();
                ch.WriteM_indexFile();
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_activity set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic,");
                strSql.Append("pictype=@pictype,");
                strSql.Append("isclick=@isclick,");
                strSql.Append("islink=@islink,");
                strSql.Append("linkurl=@linkurl");
                strSql.Append(" where id=@id");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),					
					new MySqlParameter("@pictype", MySqlDbType.Int16,4),
					new MySqlParameter("@isclick", MySqlDbType.Int16,4),
					new MySqlParameter("@islink", MySqlDbType.Int16,4),				
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@id", MySqlDbType.Int32,11)};

                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = isclickTB;
                parameters[4].Value = islinkTB;
                parameters[5].Value = linkurlTB;
                parameters[6].Value = pid;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
                CreatHtml ch = new CreatHtml();
                ch.WriteP_indexFile();
                ch.WriteM_indexFile();
            }
        }
        private void ShowPicList()
        {
            string strHtml = "";            
            int pictype = Tools.CheckVar(Request["pictype"], 0);
            List<string> list = new List<string>();

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from wechat_pic");
            strSql.Append(" where isshow=1 and pictype=@type order by id desc");
            MySqlParameter[] parameters = {
				new MySqlParameter("@type", MySqlDbType.Int32)              
            };
            parameters[0].Value = pictype;    
 
            strHtml = "{";
            strHtml += "\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"isshow\":\"" + dt.Rows[i]["isshow"].ToString() + "\",\"pictype\":\"" + dt.Rows[i]["pictype"].ToString() + "\",\"isclick\":\"" + dt.Rows[i]["isclick"].ToString() + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\"},";
            }
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void ShowPic(){
            int pid = Tools.CheckVar(Request["pid"], 1);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from wechat_pic");
            strSql.Append(" where id=@pid");
            MySqlParameter[] parameters = {
				new MySqlParameter("@pid", MySqlDbType.Int32)               
            };
            parameters[0].Value = pid;
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            string strHtml = "{";
            strHtml += "\"pid\":\"" + dt.Rows[0]["id"] + "\",\"title\":\"" + dt.Rows[0]["title"] + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[0]["pic"].ToString()).Replace("+", "%20") + "\",\"isshow\":\"" + dt.Rows[0]["isshow"] + "\",\"pictype\":\"" + dt.Rows[0]["pictype"] + "\",\"isclick\":\"" + dt.Rows[0]["isclick"] + "\",\"islink\":\"" + dt.Rows[0]["islink"] + "\",\"linkurl\":\"" + dt.Rows[0]["linkurl"] + "\"";
            strHtml += "}";
            Response.Write(strHtml);
            Response.End();
        }
        private void ShowDelPic()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);
            string strSql = "update wechat_pic set isshow=0  where id=@pid";           
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
           
        }
        private void EditPic()
        {
            string  pid =Tools.SafeSql(Tools.CheckVar(Request["pid"], ""));
            if (pid.Length >= 2)
            {
                pid = pid.TrimEnd(',');
            }
            for (int i = 0; i < pid.Split(',').Count(); i++)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_pic set ");
                strSql.Append("isshow=1");
                strSql.Append(" where id in (@pid)");
                MySqlParameter[] parameters = {					
					new MySqlParameter("@pid", MySqlDbType.VarChar,200)};
                parameters[0].Value = pid.Split(',')[i];
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
            }
            CreatHtml CH = new CreatHtml();
            CH.WriteP_indexFile();
            CH.WriteM_indexFile();
        }
        private void DelPic()
        {
            int pid = Tools.CheckVar(Request["pid"], 1);         
            string strSql="delete from wechat_pic where id=@pid";           
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
        }
        private void AddPic()
        {
            int pid = Tools.CheckVar(Request["pid"], 0);
            int typeTB = Tools.CheckVar(Request["typeTB"], 1);
            int isclickTB = Tools.CheckVar(Request["isclickTB"], 1);
            int islinkTB = Tools.CheckVar(Request["islinkTB"], 1);
            string linkurlTB = Tools.SafeSql(Tools.CheckVar(Request["linkurlTB"], ""));
            string titleTB = Tools.SafeSql(Tools.CheckVar(Request["titleTB"], ""));
            string selPic = Tools.SafeSql(Tools.CheckVar(Request["selPic"], ""));
            if (selPic == "" || titleTB == "" || titleTB == "")
            {
                Response.Write("访问限制");
                Response.End();
                return;
            }
            string applicationPath = System.Web.HttpContext.Current.Server.MapPath("~/") + "UpFiles/res/" + selPic;            
            if (!File.Exists(applicationPath))
             {
                 Response.Write("访问限制");
                 Response.End();
                 return; 
             }
            if (pid == 0)
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wechat_pic(");
                strSql.Append("title,pic,isshow,pictype,isclick,islink,linkurl)");
                strSql.Append(" values (");
                strSql.Append("@title,@pic,'0',@pictype,@isclick,@islink,@linkurl)");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),	
					new MySqlParameter("@pictype", MySqlDbType.Int16,4),
					new MySqlParameter("@isclick", MySqlDbType.Int16,4),
					new MySqlParameter("@islink", MySqlDbType.Int16,4),					
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500)
                    };
                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = isclickTB;
                parameters[4].Value = islinkTB;
                parameters[5].Value = linkurlTB;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
            }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("update wechat_pic set ");
                strSql.Append("title=@title,");
                strSql.Append("pic=@pic,");
                strSql.Append("pictype=@pictype,");
                strSql.Append("isclick=@isclick,");
                strSql.Append("islink=@islink,");               
                strSql.Append("linkurl=@linkurl");
                strSql.Append(" where id=@id");
                MySqlParameter[] parameters = {
					new MySqlParameter("@title", MySqlDbType.VarChar,200),
					new MySqlParameter("@pic", MySqlDbType.VarChar,200),					
					new MySqlParameter("@pictype", MySqlDbType.Int16,4),
					new MySqlParameter("@isclick", MySqlDbType.Int16,4),
					new MySqlParameter("@islink", MySqlDbType.Int16,4),				
					new MySqlParameter("@linkurl", MySqlDbType.VarChar,500),
					new MySqlParameter("@id", MySqlDbType.Int32,11)};                

                parameters[0].Value = titleTB;
                parameters[1].Value = selPic;
                parameters[2].Value = typeTB;
                parameters[3].Value = isclickTB;
                parameters[4].Value = islinkTB;
                parameters[5].Value = linkurlTB;
                parameters[6].Value = pid;
                SQLTool.ExecuteNonQuery(strSql.ToString(), parameters);
            }            

        }
        private void GetPicPageSource()
        {
            string strHtml = "";
            int pageindex = Tools.CheckVar(Request["pageindex"], 1);
            int pagesize = Tools.CheckVar(Request["pagesize"], 5);
            int pictype = Tools.CheckVar(Request["pictype"], 0);
            List<string> list = new List<string>();          
            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from wechat_pic");
            strSql.Append(" where pictype=@type order by id desc  limit @pageindex, @pagesize");
            MySqlParameter[] parameters = {
				new MySqlParameter("@type", MySqlDbType.Int32),
                new MySqlParameter("@pageindex", MySqlDbType.Int32),
                new MySqlParameter("@pagesize", MySqlDbType.Int32)
            };
            parameters[0].Value = pictype;
            parameters[1].Value = (Convert.ToInt32(pageindex) - 1) * pagesize;
            parameters[2].Value = pagesize;
            int count = 0;

            StringBuilder strSqlcount = new StringBuilder();
            strSqlcount.Append("select count(id) from wechat_pic");
            strSqlcount.Append(" where  pictype=@type");
            MySqlParameter[] parameterscount = {
				new MySqlParameter("@type", MySqlDbType.Int32)              
            };
            parameterscount[0].Value = pictype;

            count = SQLTool.GetFirstIntField(strSqlcount.ToString(), parameterscount);
            int totalpage = (count % pagesize == 0 ? count / pagesize : count / pagesize + 1);
            strHtml = "{";
            strHtml += "\"total\":\"" + count + "\",\"totalpage\":\"" + totalpage + "\",\"list\":";
            strHtml += "[";
            DataTable dt = SQLTool.GetDataTable(strSql.ToString(), parameters);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strHtml += "{\"pid\":\"" + dt.Rows[i]["id"].ToString() + "\",\"title\":\"" + dt.Rows[i]["title"].ToString() + "\",\"pic\":\"" + HttpUtility.UrlEncode(dt.Rows[i]["pic"].ToString()).Replace("+", "%20") + "\",\"isshow\":\"" + dt.Rows[i]["isshow"].ToString() + "\",\"pictype\":\"" + dt.Rows[i]["pictype"].ToString() + "\",\"isclick\":\"" + dt.Rows[i]["isclick"].ToString() + "\",\"islink\":\"" + dt.Rows[i]["islink"].ToString() + "\",\"linkurl\":\"" + dt.Rows[i]["linkurl"].ToString() + "\"},";
            }       
            strHtml = strHtml.TrimEnd(',');
            strHtml += "]}";
            Response.Write(strHtml);
            Response.End();
        }
        private void setState()
        {
            int pid = Tools.CheckVar(Request["Id"], 1);
            string strSql = "delete from  wechat_jobjoin where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            Response.Clear();
            Response.Write("ok");
            Response.End();
        }
        private void setStatejoin()
        {
            int pid = Tools.CheckVar(Request["Id"], 1);
            string strSql = "delete from  wechat_joinfrom where id=@pid";
            MySqlParameter[] parameters = {
					new MySqlParameter("@pid", MySqlDbType.Int16),				
                    };
            parameters[0].Value = pid;
            SQLTool.ExecuteNonQuery(strSql, parameters);
            Response.Clear();
            Response.Write("ok");
            Response.End();
        }
    }
}