﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatHtml.aspx.cs" Inherits="FractalistWX.Manage.CreatHtml" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div class="imgTab addImgTextTk bgfff" style="padding-left: 47px;">
        <div class="mtitle">
            <span class="font30">网站生成</span></div>
        <div style="height: 10px; border-bottom: 2px solid #196fc0;">
        </div>
        <div style="width: 90%; margin: 20px;">
            <asp:Label runat="server" ID="showtxt"></asp:Label>
        </div>
    </div>
    </form>
</body>
</html>
