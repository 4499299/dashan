﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using MySql.Data.MySqlClient;
using FractalistWX.Dal;

namespace FractalistWX.Manage
{
    public partial class actionmoshow : System.Web.UI.Page
    {
        public int pid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            string strSql = "select id,title, pic, isclick, islink, linkurl from wechat_activity where pictype=1 order by id desc limit 1";
            List<string> strlist = SQLTool.GetListField(strSql, 6);
            if (strlist.Count > 0)
            {
                pid = Convert.ToInt32(strlist[0]);
                isclickTB.SelectedValue = strlist[3];
                islinkTB.SelectedValue = strlist[4];
                linkurlTB.Text = strlist[5];
                this.titleTB.Text = strlist[1];
                this.selPic.Value = strlist[2];
            }
        }
    }
}