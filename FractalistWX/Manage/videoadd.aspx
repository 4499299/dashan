﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="videoadd.aspx.cs" Inherits="FractalistWX.Manage.videoadd" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>  
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">新闻中心</span>－视频管理</div>
        <div class="sel">
            <ul>
               <li class="selli" style="cursor:pointer;">视频发布管理</li>
                <li  class="selact">视频编辑管理</li>
            </ul>
        </div>         
        <div class="clear">
            <table width="670px" border="0" cellspacing="5" cellpadding="5" style="font-size: 12px;
                line-height: 25px; font-size: 14px;">
                <tr style="height: 65px;">
                    <td>
                        素材平台：
                    </td>
                    <td width="150px;">                       
                        <asp:DropDownList ID="typeTB" runat="server" Width="120px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="请选择" Value=""></asp:ListItem>
                            <asp:ListItem Text="PC" Selected Value="0"></asp:ListItem>
                            <asp:ListItem Text="MO" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="150px;" style="text-align: right;">
                        跳转页面：
                    </td>
                    <td>
                         <asp:DropDownList ID="islinkTB" runat="server" Width="120px" Font-Size="12px" Height="35px">
                            <asp:ListItem Text="外部链接" Value="0"></asp:ListItem>
                          <%--  <asp:ListItem Text="内部链接" Value="1"></asp:ListItem>--%>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 65px;">                  
                    <td>
                        输入URL地址：
                    </td>
                    <td>
                        <asp:TextBox ID="linkurlTB" TextMode="SingleLine" runat="server" Width="145px" Font-Size="12px"
                            Text="" Height="35px"></asp:TextBox>
                    </td>
                    <td width="150px;" style="text-align: right;">
                        显示到首页：
                    </td>
                    <td>
                        <input type="checkbox" id="isindex" name="isindex" value="1" <%if(isindex==1){ %> checked="checked"<%} %> style="width: 30px; height: 30px;" />
                        
                    </td>
                </tr>
                <tr <%if(isindex==1&&typetb1=="0"){ %> style="height: 65px;"<%}else{ %> style="height: 65px;"<%} %> id="showyepic">
                    <td>
                        上传首页banner:
                    </td>
                    <td colspan="3">
                        <input class="inp2" type="text" id="selPic1" name="selPic1" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35"
                                src="UploadFile1.aspx?cid=selPic1" scrolling="no" frameborder="0"></iframe>
                            <br />
                            <p id="p1" style="font-size:11px; color:Red;">尺寸(宽X高)：330X114</p>
                    </td>
                </tr>     
                <tr style="height: 65px;">
                    <td>
                        名称备注：
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="445px" Font-Size="12px"
                            Height="35px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 65px;">
                    <td >
                        推荐到最新视频：
                    </td>
                    <td>
                        <input type="checkbox" id="istuijian" name="istuijian" value="1" <%if(istuijian==1){ %> checked="checked"<%} %> style="width: 30px; height: 30px;" />
                    </td>                    
                    <td colspan="2" id="indexdiv" <%if(istuijian!=1){ %>style="display:none;"<%} %>><span>请选择位置</span>
                        <select id="showindex" name="showindex">
                            <option value="0" <%if(picindex1==0){ %> selected<%} %>>请选择</option>
                            <option value="1" <%if(picindex1==1){ %> selected<%} %>>图一</option>
                            <option value="2" <%if(picindex1==2){ %> selected<%} %>>图二</option>
                            <option value="3" <%if(picindex1==3){ %> selected<%} %>>图三</option>
                            <option value="4" <%if(picindex1==4){ %> selected<%} %>>图四</option>
                            <option value="5" <%if(picindex1==5){ %> selected<%} %>>图五</option>
                        </select>
                    </td>
                </tr>  
                <tr style="height: 65px;">
                    <td>
                        上传banner:
                    </td>
                    <td colspan="3">
                        <input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35"
                                src="UploadFile1.aspx?cid=selPic" scrolling="no" frameborder="0"></iframe>
                            <br />
                            <p id="picsize" style="font-size:11px; color:Red;">
                            <%if (typetb1 == "1" && istuijian==1)
                              {
                               %>
                               <%if (picindex1 == 0)
                                 { %>
                                尺寸(宽X高)：258X191(PC) 289X240(MO)
                               <%}
                                 else if (picindex1 == 1)
                                 { %>
                                 尺寸(宽X高)：582X292
                              <%}
                                 else if (picindex1 == 2)
                                 { %>
                                 尺寸(宽X高)：289X240
                               <%}
                                 else if (picindex1 == 3)
                                 { %>
                                  尺寸(宽X高)：289X240
                               <%} %>
                            <%}
                              else if (typetb1 == "0" && istuijian == 1)
                              { %>
                                <%if (picindex1 == 0)
                                 { %>
                                尺寸(宽X高)：258X191(PC) 289X240(MO)
                               <%}
                                 else if (picindex1 == 1)
                                 { %>
                                  尺寸(宽X高)：630X317
                              <%}
                                 else if (picindex1 == 2)
                                 { %>
                                  尺寸(宽X高)：376X317
                               <%}
                                 else if (picindex1 == 3)
                                 { %>
                                  尺寸(宽X高)：365X191
                               <%}
                                 else if (picindex1 == 4)
                                 { %>
                                  尺寸(宽X高)：365X191
                               <%}
                                 else if (picindex1 == 5)
                                 { %>
                                  尺寸(宽X高)：258X191
                               <%} %>
                              <%}
                              else
                              { %>
                               尺寸(宽X高)：258X191(PC) 289X240(MO)
                              <%} %>
                            </p>
                    </td>
                </tr>                                
                <tr id="editdiv" <%if(islink==0){ %>style="display:none;"<%} %>>
                    <td>
                        上传视频:
                    </td>
                    <td colspan="3" style="text-align: left;">
                         <input class="inp2" type="text" id="selvideo" name="selvideo" runat="server" style="height: 35px;
                            line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="45"
                                src="UploadFile2.aspx?cid=selvideo" scrolling="no" frameborder="0"></iframe>
                    </td>
                </tr>                           
            </table>
        </div>   
        <div style="clear:both;"></div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
         <div style="height:25px;"><asp:Label ID="errorLB" runat="server" Text="" ForeColor="#CC0000"></asp:Label></div>
        <div class="tac" style="height:40px;line-height:40px; margin-bottom:40px;">
            <div style="float:left; width:184px; height:47px; margin-top:6px; cursor:pointer;" id="subbtn">
                <asp:ImageButton ID="ImageButton1" ImageUrl="images/okbtn.png" width="184" height="47" runat="server" OnClick="comfirm_BT_Click"   />
               </div>
            <div style="float:left; width:184px; height:47px; line-height:25px;margin-top:6px; margin-left:20px;cursor:pointer;" id="canbtn"><img src="images/cannelbtn.png" width="184" height="47" /></div>
        </div>    
         </ContentTemplate>
       </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="mId" Value="0" runat="server" />
    </form>
</body>
</html>
<script>
    $("#canbtn").bind("click", function () {
        $("#form1")[0].reset();
    })
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "videomanage.aspx?pictype=" + pictype;
    })   
    $("#<%=islinkTB.ClientID %>").bind("click", function () {
        if ($("#<%=islinkTB.ClientID %>").val() == "0") {
            $("#editdiv").hide();
            $("#<%=linkurlTB.ClientID %>").attr("disabled", false);  
        }
        else {
            $("#editdiv").show();
            $("#<%=linkurlTB.ClientID %>").attr("disabled", true);
        }
    })
    function HTMLDecode2(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#39;/g, "\'");
        s = s.replace(/&quot;/g, "\"");
        return s;
    }
    $("#typeTB").bind("change", function () {      
        if ($(this).val() == "1") {
            $("#showindex option").eq(3).hide();
            $("#showindex option").eq(4).hide();
        } else {
            $("#showindex option").eq(3).show();
            $("#showindex option").eq(4).show();
        }
    })
    $("#istuijian").change(function () {
        if($(this).is(':checked')){
            $("#indexdiv").show();
        }else{
            $("#indexdiv").hide();
        }
    })
    $("#showindex").change(function () {
        if ($("#typeTB").val() == "0") {
            if ($("#showindex").val() == "1") {
                $("#picsize").text("尺寸(宽X高)：630X317");
            }
            if ($("#showindex").val() == "2") {
                $("#picsize").text("尺寸(宽X高)：376X317");
            }
            if ($("#showindex").val() == "3") {
                $("#picsize").text("尺寸(宽X高)：365X191");
            }
            if ($("#showindex").val() == "4") {
                $("#picsize").text("尺寸(宽X高)：365X191");
            }
            if ($("#showindex").val() == "5") {
                $("#picsize").text("尺寸(宽X高)：258X191");
            }
            if ($("#showindex").val() == "0") {
                $("#picsize").text("尺寸(宽X高)：258X191(PC) 289X240(MO)");
            }
        }
        else {
            if ($("#showindex").val() == "1") {
                $("#picsize").text("尺寸(宽X高)：582X292");
            }
            if ($("#showindex").val() == "2") {
                $("#picsize").text("尺寸(宽X高)：289X240");
            }
            if ($("#showindex").val() == "3") {
                $("#picsize").text("尺寸(宽X高)：289X240");
            }           
            if ($("#showindex").val() == "0") {
                $("#picsize").text("尺寸(宽X高)：258X191(PC) 289X240(MO)");
            }
        }
    })
    $("#isindex").change(function () {
        if ($(this).is(':checked') && $("#typeTB").val()=="0") {
            $("#showyepic").show();
        } else {
            $("#showyepic").hide();
        }
    })
</script>