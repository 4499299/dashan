﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using FractalistWX.Tool;

namespace FractalistWX.Manage
{
    public partial class UploadFile2 : System.Web.UI.Page
    {
        string cId = "";
        string file = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            cId = Tools.CheckVar(Request["cid"], "PicTB");
            file = Tools.CheckVar(Request["file"], "res");
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = this.FileUpload1.PostedFile.FileName;
                if (string.IsNullOrEmpty(fileName))
                {
                    return;
                }

                //获得绝对路径
                string applicationPath = System.Web.HttpContext.Current.Server.MapPath("~/") + "UpFiles/" + file + "/";

                string imageName = FileUpload1.FileName;

                //设置每次上传的图片文件名的相对路径
                string TimeStr = System.DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string AllFileName = TimeStr + FileUpload1.PostedFile.ContentLength.ToString() + imageName.Substring(imageName.LastIndexOf("."), imageName.Length - imageName.LastIndexOf("."));
                AllFileName = AllFileName.ToLower();
                string imageTempPath = applicationPath + AllFileName;

                FileUpload1.PostedFile.SaveAs(imageTempPath);
                Response.Write("<script> window.parent.document.getElementById('" + cId + "').value = '" +file+"/"+ AllFileName + "';</script>");
            }
            catch
            {

            }
        }
    }
}