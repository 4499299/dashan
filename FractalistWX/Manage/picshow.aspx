﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="picshow.aspx.cs" Inherits="FractalistWX.Manage.picshow" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/menustyle.css" type="text/css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        runat="server">
    </asp:ScriptManager>
    <div  class="imgTab addImgTextTk bgfff" style="padding-left:47px;">
        <div class="mtitle"><span class="font30">首页管理</span>－轮播图管理</div>
        <div class="sel">
            <ul>
                <li class="selact">轮播图发布管理</li>
                <li class="selli"><a href="javascript:;">轮播图素材管理</a></li>
            </ul>
        </div>  
        <div style="width:860px; margin-top:15px; text-align:right;">
            <span>发布更新平台：</span>
            <span>
                <select id="tb_type" name="tb_type" id="tb_type" style="width:120px; font-size:12px; height:44px;">
                    <option value="0"  <%if(pictype==0){ %>selected="selected"<%} %>>pc平台</option>
                    <option value="1"  <%if(pictype==1){ %>selected="selected"<%} %>>手机平台</option>
                </select>
            </span>           
         </div>  
         <div class="clear imgTextListBoxAddpic">
            <ul class="imgTextList fl clear">  
                <% if (dt != null)
                   {
                       for (int i = 0; i < dt.Rows.Count; i++)
                       { %>
                <li>
                    <div class="pic_txt"><%=dt.Rows[i]["title"]%><input type="hidden" name="td_id" value="<%=dt.Rows[i]["id"] %>" /></div>
                    <div class="pic_pic"><img src="../UpFiles/<%=dt.Rows[i]["pic"] %>" width="236" height="132" /></div>
                    <div style="margin-top:2px;"><span style="cursor: pointer;"><img src="images/edit.png" name="pic_edit"/></span><span style="margin-left:3px;cursor: pointer;"><img src="images/del.png" name="pic_del" /></span> </div>
                    <div class="pic_tishi">pc</div>
                </li> 
                <%}
                  }%>               
                <li id="selectpic" style="cursor: pointer;">
                    <div class="pic_txt"></div>
                    <img src="images/addpic.jpg" />
                </li>        
            </ul>           
        </div>   
        <div class="tac" style="height:40px;line-height:40px; margin-bottom:40px;">
            <div style="float:left; width:184px; height:47px; margin-top:6px; cursor:pointer;" id="subbtn"><img src="images/okbtn.png" width="184" height="47" /></div>
            <div style="float:left; width:184px; height:47px; line-height:25px;margin-top:6px; margin-left:20px;cursor:pointer;" id="canbtn"><img src="images/cannelbtn.png" width="184" height="47" /></div>
        </div>
        <div id="divDisable" style="display: none; opacity:0.5; -moz-opacity: 0.5; z-index: 1099;position: absolute; left: 0px; top: 0px; filter: alpha(opacity=60); background-color: #000; width: 100%; height:130%;">
        </div>    
         <div id="editImgTextTk" class="a imgTk addImgTextTk bgfbfbfb imgTkex dn" style="z-index: 1200;width:584px;">
	        <h2 class="bg0b82c7 imgTkH2" style="color:#fff;width:538px;">轮播图素材编辑</h2>           
	        <div class="clear imgTextListBox" style="width:584px; height:285px;">
		           <table width="90%" border="0" cellspacing="5" cellpadding="5" style="font-size:12px;line-height:25px; margin:0 auto; font-size:14px;">
                        <tr style="height:65px;">
                            <td>素材平台：</td>
                            <td>
                            <input type="hidden" id="pid" value="0" />
                            <asp:DropDownList ID="typeTB"  runat="server" Width="120px" Font-Size="12px" Height="35px"><asp:ListItem Text="请选择" Value=""></asp:ListItem><asp:ListItem Text="PC" Value="0"></asp:ListItem><asp:ListItem Text="MO" Value="1"></asp:ListItem></asp:DropDownList></td>
                            <td>是否可点击：</td>
                            <td><asp:DropDownList ID="isclickTB"  runat="server" Width="130px" Font-Size="12px" Height="35px"><asp:ListItem Text="可点击" Value="0"></asp:ListItem><asp:ListItem Text="不可点击" Value="1"></asp:ListItem></asp:DropDownList></td>
                        </tr>
                        <tr  style="height:65px;">
                            <td>跳转页面：</td>
                            <td><asp:DropDownList ID="islinkTB"  runat="server" Width="120px" Font-Size="12px" Height="35px"><asp:ListItem Text="外部链接" Value="0"></asp:ListItem><asp:ListItem Text="内部链接" Value="1"></asp:ListItem></asp:DropDownList></td>
                            <td>输入URL地址：</td>
                            <td><asp:TextBox ID="linkurlTB" TextMode="SingleLine" runat="server" Width="125px" Font-Size="12px" Height="35px"></asp:TextBox></td>
                        </tr>
                        <tr  style="height:65px;">
                            <td>名称备注：</td>
                            <td colspan="3"><asp:TextBox ID="titleTB" TextMode="SingleLine" runat="server" Width="396px" Font-Size="12px" Height="35px"></asp:TextBox></td>
                        </tr>
                        <tr  style="height:65px;">
                            <td>上传banner：</td>
                            <td  colspan="3"><input class="inp2" type="text" id="selPic" name="selPic" runat="server" style="height: 35px; line-height: 35px;" />&nbsp;<iframe style="vertical-align: top;" width="225" height="35" src="UploadFile.aspx?cid=selPic" scrolling="no" frameborder="0"></iframe></td>
                        </tr>
                        <tr  style="height:15px;">
                            <td></td>
                            <td style=" color:Red; font-size:12px;" id="msgtxt"></td>
                        </tr>
                    </table>
	        </div>      
            <div class="clear" style="width:98%; margin:0 auto; text-align:center;">
                <hr style="height:1px;border:none;border-top:1px solid #f2f2f2;" />    
            </div>   
	        <div class="tac tkBtn">
		        <a href="javascript:;" class="cfff tkBtnL dib" id="editokbtn"><img src="images/okbtn.png" width="184" height="47" /></a>
		        <a href="javascript:;" class="c000 tkBtnR dib" id="editcanbtn"><img src="images/cannelbtn.png" width="184" height="47" /></a>        
	        </div>
        </div>
        <div id="addImgTextTk" class="a imgTk addImgTextTk bgfbfbfb imgTkex dn" style="z-index: 1200;">
	    <h2 class="bg0b82c7 imgTkH2" style="color:#fff;">选择轮播图素材</h2>           
	    <div class="clear imgTextListBox">
		    <ul class="timgTextList fl clear">
			    
		    </ul>		    
	    </div>
        <div class="clear" style="width:98%; margin:0 auto; text-align:center;">
            <hr style="height:1px;border:none;border-top:1px solid #f2f2f2;" />    
        </div> 
	    <div class="clear imgPageBox">
		    <a class="c222  imgPageR fr" href="javascript:;" style="margin-top:3px;"><img src="images/selbtn.png" width="100%" /></a>
		    <div class="boe7e7eb imgPageM fr"><input class="db w100 inpNoBg imgPageMinp" type="number" /></div>
		    <div class="imgPageL fr">
			    <a href="javascript:;" class="c222 fl  iPagePrev"><img src="images/left.png" style="margin-top:10px;" /></a>
			    <p class="imgPageInfo fl">1 / 1</p>
			    <a href="javascript:;" class="c222 fl  iPageNext"><img src="images/right.png" style="margin-top:10px;" /></a>
		    </div>
	    </div>
        <div class="clear" style="width:98%; margin:0 auto; text-align:center;">
            <hr style="height:1px;border:none;border-top:1px solid #f2f2f2;" />    
        </div>   
	    <div class="tac tkBtn">
		    <a href="javascript:;" class="cfff tkBtnL dib" id="okbtn"><img src="images/okbtn.png"  width="184" height="47" /></a>
		    <a href="javascript:;" class="c000 tkBtnR dib" id="unokbtn"><img src="images/cannelbtn.png" width="184" height="47" /></a>        
	    </div>
    </div>
    </div>
    </form>
</body>
</html>
<script>
    var pid = "";
    $("#selectpic").live("click", function () {       
        $("#addImgTextTk").show();
        $("#divDisable").show();
    })
    $(".selli").bind("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "picresouceManage.aspx?type=" + pictype;
    })
    $("#addpic img").live("click", function () {
        var pictype = $("#tb_type").val();
        window.location.href = "picresouceManage.aspx?type=" + pictype;
    })
    $("#unokbtn").bind("click", function () {
        $("div[name='mask']").hide();
        $("#addImgTextTk").hide();
        $("#divDisable").hide();
    });
    $("#okbtn").bind("click", function () {
        $(".timgTextList li").each(function () {
            if (!$(this).find("div[name='mask']").is(":hidden")) {
                pid += $(this).attr("value") + ",";
                $(this).find("div[name='mask']").hide();
                var strhtml = '<input type="hidden" name="td_id" value="' + $(this).attr("value") + '" /><div style="margin-top:119px;"><span style="cursor: pointer;"><img src="images/edit.png" name="pic_edit" /></span><span style="margin-left:3px;cursor: pointer;"><img src="images/del.png" name="pic_del" /></span> </div>';
                $("#selectpic").before("<li class=\"candiv\">" + $(this).html() + strhtml + "</li>");
            }
        });
        $("#addImgTextTk").hide();
        $("#divDisable").hide();
    });
    $("#canbtn").bind("click", function () {
        $(".candiv").remove();
    })
    $("#subbtn").bind("click", function () {
        if (!ispost) {
            ispost = true;
            var Param = $.param({
                pid: pid
            });
            $.ajax({ url: "Ajax.aspx?action=EditPic&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    alert("提交成功");
                    pid = "";
                    $(".candiv").removeClass("candiv");
                    PageData();
                }
            });
        }
    })
    $("img[name='pic_del']").live("click", function () {
        if (confirm('确定要删除吗？')) {
            var pid = $(this).parent().parent().parent().find("input").val();
            var liobj = $(this).parent().parent().parent();
            var Param = $.param({
                pid: pid
            });
            if (!ispost) {
                ispost = true;
                $.ajax({ url: "Ajax.aspx?action=ShowDelPic&t=" + Math.random(),
                    type: "POST",
                    data: Param,
                    dataType: "json",
                    processData: false,
                    error: function (datas) {
                        ispost = false;
                    },
                    success: function (datas) {
                        ispost = false;
                        liobj.remove();
                    }
                });
            }
        }
    })
    $("img[name='pic_edit']").live("click", function () {
        var pid = $(this).parent().parent().parent().find("input").val();
        var Param = $.param({
            pid: pid
        });
        $.ajax({ url: "Ajax.aspx?action=ShowPic&t=" + Math.random(),
            type: "POST",
            data: Param,
            dataType: "json",
            processData: false,
            error: function (datas) {
                ispost = false;
            },
            success: function (datas) {
                ispost = false;               
                var pic = datas.pic.replace("%2f", "/");
                $("#<%=titleTB.ClientID %>").val(datas.title);
                $("#<%=selPic.ClientID %>").val(pic);
                $("#<%=linkurlTB.ClientID %>").val(datas.linkurl);
                $("#<%=islinkTB.ClientID %>").val(datas.islink);
                $("#<%=isclickTB.ClientID %>").val(datas.isclick);
                $("#<%=typeTB.ClientID %>").val(datas.pictype);
            }
        });

        $("#pid").val(pid);
        $("#editImgTextTk").show();
        $("#divDisable").show();
    })
    $("#editcanbtn").bind("click", function () {
        $("#editImgTextTk").hide();
        $("#divDisable").hide();
    })
    $("#<%=isclickTB.ClientID %>").bind("change", function () {
        if ($(this).val() == "1") {
            $("#<%=islinkTB.ClientID %>").attr("disabled", true);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", true);
        }
        else {
            $("#<%=islinkTB.ClientID %>").attr("disabled", false);
            $("#<%=linkurlTB.ClientID %>").attr("disabled", false);
        }
    })  
    $("#editokbtn").bind("click", function () {
        if (!ispost) {
            ispost = true;
            var pid = $("#pid").val();
            var typeTB = $("#<%=typeTB.ClientID %>").val();
            var isclickTB = $("#<%=isclickTB.ClientID %>").val();
            var islinkTB = $("#<%=islinkTB.ClientID %>").val();
            var linkurlTB = $("#<%=linkurlTB.ClientID %>").val();
            var titleTB = $("#<%=titleTB.ClientID %>").val();
            var selPic = $("#<%=selPic.ClientID %>").val();
            if (typeTB == "") {
                $("#msgtxt").text("请选择素材平台");
                ispost = false;
                return
            }
            if (isclickTB == "0") {
                if (linkurlTB == "") {
                    $("#msgtxt").text("请选择链接地址");
                    ispost = false;
                    return
                }
            }
            if (titleTB == "") {
                $("#msgtxt").text("请输入名称备注");
                ispost = false;
                return
            }
            if (selPic == "") {
                $("#msgtxt").text("请上传banner");
                ispost = false;
                return
            }
            var Param = $.param({
                typeTB: typeTB,
                isclickTB: isclickTB,
                islinkTB: islinkTB,
                linkurlTB: linkurlTB,
                titleTB: titleTB,
                selPic: selPic,
                pid: pid
            });
            $.ajax({ url: "Ajax.aspx?action=AddPic&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    $("#msgtxt").text("素材修改成功");
                    setTimeout(function () {
                        $("#editImgTextTk").hide();
                        $("#divDisable").hide();
                        $("#msgtxt").text("");
                        var pictype = $("#tb_type").val();
                        PagesData();
                    }, 1000);
                }
            });
        }
    })
    $("#tb_type").bind("change", function () {
        pictype = $("#tb_type").val();       
        PagesData();
    })
    function PagesData() {
        var Param = $.param({
            pictype: pictype          
        });
        if (!ispost) {
            ispost = true;
            $.ajax({ url: "Ajax.aspx?action=ShowPicList&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    var list = datas.list;                   
                    $('.imgTextList').html("");
                    if (list != null && list.length > 0) {
                        var htmldesc = "";
                        for (var j = 0; j < list.length; j++) {
                            var pictype = list[j].pictype == "0" ? "PC" : "MO";
                            htmldesc += ' <li>'+
                                        '<div class="pic_txt">'+list[j].title+'<input type="hidden" name="td_id" value="'+list[j].pid+'" /></div>'+
                                        ' <div class="pic_pic"><img src="../UpFiles/'+list[j].pic+'" width="236" height="132" /></div>'+
                                        '<div style="margin-top:2px;"><span style="cursor: pointer;"><img src="images/edit.png" name="pic_edit"/></span><span style="margin-left:3px;cursor: pointer;"><img src="images/del.png" name="pic_del" /></span> </div>'+
                                        '<div class="pic_tishi">' + pictype + '</div>' +
                                        '</li> '; 
                        }
                        htmldesc += '<li id="selectpic" style="cursor: pointer;"><div class="pic_txt"></div><img src="images/addpic.jpg" /></li>'; 
                        $('.imgTextList').html(htmldesc);                       
                    }
                    else {
                        var htmldesc = "";
                        htmldesc += '<li id="selectpic" style="cursor: pointer;"><div class="pic_txt"></div><img src="images/addpic.jpg" /></li>';                      
                        $('.imgTextList').html(htmldesc);
                        
                    }
                }
            });
        }
    }
</script>
<script>
    var ispost = false;
    var pageindex = 1, totalpage = 0;
    var pictype = $("#tb_type").val();
    $("#tb_type").bind("change", function () {
        pictype = $("#tb_type").val();
        pageindex = 1, totalpage = 0;
        PageData();
    })
    PageData();
    function PageData() {
        var pictype = $("#tb_type").val();       
        var Param = $.param({
            pictype: pictype,
            pageindex: pageindex
        });
            $.ajax({ url: "Ajax.aspx?action=picpgsource&t=" + Math.random(),
                type: "POST",
                data: Param,
                dataType: "json",
                processData: false,
                error: function (datas) {
                    ispost = false;
                },
                success: function (datas) {
                    ispost = false;
                    var list = datas.list;
                    totalpage = datas.totalpage;
                    $('.timgTextList').html("");
                    if (list != null && list.length > 0) {
                        var htmldesc = "";
                        for (var j = 0; j < list.length; j++) {
                            htmldesc += '<li value="' + list[j].pid + '" style="cursor: pointer;">';
                            htmldesc += '<div class="pic_txt">' + list[j].title + '</div>';
                            htmldesc += '<div class="pic_pic"><img src="../UpFiles/' + list[j].pic + '" width="236" height="132" /></div>';                           
                            var pictype = list[j].pictype == "0" ? "PC" : "MO";
                            htmldesc += '<div class="pic_tishi">' + pictype + '</div>'
                            htmldesc += "<div  name='mask' style='position: relative; width:100%; height:100%; display:none'>" +
					                        "<div class='a appmsg_mask'></div>" +
					                        "<i class='a oh cw icon_card_selected'>已选择</i>" +
				                        "</div>";
                            htmldesc += "</li>";
                        }                      
                        $('.timgTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                    else {
                        var htmldesc = "";
                        htmldesc += '<li id="addpic">';
                        htmldesc += '<div class="pic_txt"></div>';
                        htmldesc += '<img src="images/addpic.jpg" />';
                        htmldesc += '</li>';
                        $('.timgTextList').html(htmldesc);
                        $(".imgPageInfo").html(pageindex + "/" + datas.totalpage);
                    }
                }
            });      
    }
    $(".addImgTextTk .iPagePrev").bind("click", function () {
        if (pageindex != 1) {
            pageindex--;
            PageData();
        }
    });
    $(".addImgTextTk .iPageNext").bind("click", function () {
        if (pageindex != totalpage) {
            pageindex++;
            PageData();
        }
    });
    $(".addImgTextTk .imgPageR").bind("click", function () {
        var selPage = $(".addImgTextTk .imgPageMinp").val();
        if (selPage == "" || selPage == undefined) {
            alert("请输入跳转页数");
        }
        else {
            pageindex = selPage;
            if (pageindex <= totalpage) {
                PageData();
            }
            else {
                alert("索引页码不存在");
            }
        }
    });
    $(".timgTextList li").live("click", function () {
        if ($(this).find("div[name='mask']").is(":hidden")) {
            $(this).find("div[name='mask']").show();
        } else {
            $(this).find("div[name='mask']").hide();
        }
    })
</script>