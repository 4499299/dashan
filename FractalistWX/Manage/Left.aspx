﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../js/prototype.lite.js" type="text/javascript"></script>
    <script src="../js/moo.fx.js" type="text/javascript"></script>
    <script src="../js/moo.fx.pack.js" type="text/javascript"></script>    
    <style type="text/css">
        body
        {
            font: 12px Arial, Helvetica, sans-serif;
            color: #000;
            background-color: #f2f2f2;
            margin: 0px;
        }
        #container
        {
            width: 230px;
        }
        .typea
        {
            color: #017dc5;
            background-color:#fff;
        }
         .typea a
        {
            color: #017dc5;
        }
        H1
        {
            font-size: 16px;
            margin: 0px;
            width: 230px;
            cursor: pointer;
            height: 70px;
            line-height: 70px;
            border-bottom:1px solid #e0e0e0;
        }
        H1 a
        {
            display: block;
            width: 230px;
            color: #9a9999;
            height: 70px;
            line-height: 70px;
            text-decoration: none;
            moz-outline-style: none;  
            text-align: center;
            margin: 0px;
            padding: 0px;
        }
        .content
        {
            width: 230px;
            height: 26px;           
        }
        .content table
        {
             background-color:#fcfcfc;
            border-bottom:1px solid #e0e0e0;
        }
        .MM ul
        {
            list-style-type: none;
            margin: 0px;
            padding: 0px;
            display: block;
        }
        .MM li
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            color: #333333;
            list-style-type: none;
            display: block;
            text-decoration: none;
            height: 37px;
            width: 230px;
            padding-left: 0px;
        }
        .MM
        {
            width: 230px;
            margin: 0px;
            padding: 0px;
            left: 0px;
            top: 0px;
            right: 0px;
            bottom: 0px;
            clip: rect(0px,0px,0px,0px);
        }
        .MM a:link
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            color: #333333;      
            height: 37px;
            width: 230px;
            display: block;
            text-align: center;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
            text-decoration: none;
        }
        .MM a:visited
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            color: #333333;   
            display: block;
            text-align: center;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
        .MM a:active
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            color: #333333;           
            height: 37px;
            width: 230px;
            display: block;
            text-align: center;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
            text-decoration: none;
        }
        .MM a:hover
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            font-weight: bold;
            color: #017dc5;     
            text-align: center;
            display: block;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
        
        .NN a:link
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            font-weight: bold;
            color: #017dc5;           
            text-align: center;
            display: block;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
        .NN a:visited
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            font-weight: bold;
            color: #017dc5;           
            text-align: center;
            display: block;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
        .NN a:active
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            font-weight: bold;
            color: #017dc5;            
            text-align: center;
            display: block;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
        .NN a:hover
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 37px;
            font-weight: bold;
            color: #017dc5;           
            text-align: center;
            display: block;
            margin: 0px;
            padding: 0px;
            height: 37px;
            width: 230px;
            text-decoration: none;
        }
    </style>
    <script type="text/javascript" language="javascript">       
        function changeCss(str) {
            for (i = 1; i <= 10; i++) {                                      
                if (str == i) {                                   
                   document.getElementById("liId" + i).className = 'NN';
                } else {
                    document.getElementById("liId" + i).className = 'MM';
                }
            }            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" height="280" border="0" cellpadding="0" cellspacing="0" bgcolor="#f2f2f2">
        <tr>
            <td valign="top" align="center">
                <div id="container">
                    <h1 class="type typea" onclick="showtype(0)">
                        <a href="javascript:void(0)"><img src="images/iocn1_1.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img1" />首页管理</a></h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                           
                            <tr>
                                <td id="liId1" class="NN" style="padding-top:10px;">
                                    <a href="picshow.aspx" onclick="changeCss(1);" target="main">轮播图管理</a>
                                </td>
                            </tr>
                            <tr>
                                <td id="liId2" class="MM" style="padding-bottom:10px;">
                                    <a href="activitypcshow.aspx" onclick="changeCss(2);" target="main">最新活动管理</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <h1 class="type" onclick="showtype(1)">
                        <a href="javascript:void(0)"><img src="images/iocn2.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img2" />新闻中心</a></h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                           
                            <tr>
                                <td id="liId3" class="MM" style="padding-top:10px;">
                                    <a href="newsmanage.aspx" onclick="changeCss(3);" target="main">新闻管理</a>
                                </td>
                            </tr>
                            <tr>
                                <td id="liId4" class="MM" style="padding-bottom:10px;">
                                    <a href="videomanage.aspx" onclick="changeCss(4);" target="main">视频管理</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <h1 class="type" onclick="showtype(2)">
                        <a href="javascript:void(0)"><img src="images/iocn3.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img3" />产品展示</a>
                    </h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                         
                            <tr>
                                <td id="liId5" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="promanage.aspx" onclick="changeCss(5);" target="main">产品展示</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <h1 class="type" onclick="showtype(3)">
                        <a href="javascript:void(0)"><img src="images/iocn4.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img4" />招聘管理</a>
                    </h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                           
                            <tr>
                                <td id="liId6" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="jobmanage.aspx" onclick="changeCss(6);" target="main">招聘管理</a>
                                </td>
                            </tr>
                            <tr>
                                <td id="liId7" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="joblist.aspx" onclick="changeCss(7);" target="main">人才管理</a>
                                </td>
                            </tr>
                             <tr>
                                <td id="liId8" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="joblist2.aspx" onclick="changeCss(8);" target="main">加盟管理</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                     <h1 class="type" onclick="showtype(4)">
                        <a href="javascript:void(0)"><img src="images/iocn5.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img5" />客户留言</a>
                     </h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                            
                            <tr>
                                <td id="liId9" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="Feedbackmanage.aspx" onclick="changeCss(9);" target="main">客户留言</a>
                                </td>
                            </tr>
                        </table>
                    </div>   
                     <h1 class="type" onclick="showtype(5)">
                        <a href="javascript:void(0)"><img src="images/iocn6.png" width="24" height="24" style="margin-top:-8px; margin-right:8px; vertical-align:middle;" id="img6" />生成网站</a>
                     </h1>
                    <div class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">                            
                            <tr>
                                <td id="liId10" class="MM" style="padding-top:10px;padding-bottom:10px;">
                                    <a href="CreatHtml.aspx" onclick="changeCss(10);" target="main">生成网站</a>
                                </td>
                            </tr>
                        </table>
                    </div>                   
                    <script type="text/javascript">
                        var contents = document.getElementsByClassName('content');
                        var toggles = document.getElementsByClassName('type');                       
                        var myAccordion = new fx.Accordion(
			                toggles, contents, { opacity: true, duration: 400 }
		                );
                        myAccordion.showThisHideOpen(contents[0]);
                        function showtype(id) {
                            document.getElementById("img1").src= "images/iocn1.png";
                            document.getElementById("img2").src= "images/iocn2.png";
                            document.getElementById("img3").src= "images/iocn3.png";
                            document.getElementById("img4").src= "images/iocn4.png";
                            document.getElementById("img5").src= "images/iocn5.png";
                            document.getElementById("img6").src = "images/iocn6.png";
                            var pid = parseInt(id);
                            document.getElementById("img" + (pid + 1)).src = "images/iocn" + (pid + 1) + "_1.png";                          
                            var lilist = document.getElementsByClassName('type');
                            for (var i = 0; i < lilist.length; i++) {
                                var obj = document.getElementsByClassName('type')[i];
                                removeClass(obj, "typea");  
                            }
                            var objthis = document.getElementsByClassName('type')[id];
                            toggleClass(objthis, "typea");  
                        }
                        function hasClass(obj, cls) {  
                            return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));  
                        }  
  
                        function addClass(obj, cls) {  
                            if (!this.hasClass(obj, cls)) obj.className += " " + cls;  
                        }  
  
                        function removeClass(obj, cls) {  
                            if (hasClass(obj, cls)) {  
                                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');  
                                obj.className = obj.className.replace(reg, ' ');  
                            }  
                        }  
  
                        function toggleClass(obj,cls){  
                            if(hasClass(obj,cls)){  
                                removeClass(obj, cls);  
                            }else{  
                                addClass(obj, cls);  
                            }  
                        }  
                    </script>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
