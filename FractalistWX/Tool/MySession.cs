﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FractalistWX.Tool
{
    public class MySession
    {
        public static string ValidateCode
        {
            get
            {
                var context = System.Web.HttpContext.Current;

                var k = System.Web.HttpContext.Current.Request.Cookies["vc"];
                if (k != null)
                {
                    var vc = context.Server.UrlDecode(k.Value);
                    var val = StrUtil.DecriptDES(vc);                 
                    return val;
                }
                else
                {
                    return null;
                }
               
            }
            set
            {
                var context = System.Web.HttpContext.Current;
                var val = StrUtil.EncriptDES(value);
                HttpCookie cookie = new HttpCookie("vc",  context.Server.UrlEncode(val));
                cookie.Expires = DateTime.Now.AddMinutes(20);
                context.Response.Cookies.Add(cookie);

            }
        }

        
    }
}