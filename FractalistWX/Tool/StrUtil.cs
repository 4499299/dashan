﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace FractalistWX.Tool
{
    public class StrUtil
    {

        #region 对称加密DES
        /// <summary>
        /// 对称加密DES
        /// </summary>
        /// <param name="strKey">密钥</param>
        /// <param name="strSrc">加密字串</param>
        /// <returns></returns>
        public static string EncriptDES(string strSrc)
        {
            string strKey = "abc";
            if (strSrc == null)
            {
                return null;
            }
            DES des = new DESCryptoServiceProvider();// DESCryptoServiceProvider();
            des.Key = GetDESKey(strKey);
            des.IV = des.Key;
            MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);

            byte[] bTxt = Encoding.UTF8.GetBytes(strSrc);

            cs.Write(bTxt, 0, bTxt.Length);
            cs.Flush();
            cs.FlushFinalBlock();//不能少！！！y5mIPYebHkc=  y5mIPYebHkc=

            byte[] bEncrypt = ms.ToArray();
            string strResult = Convert.ToBase64String(bEncrypt);
            cs.Close();
            ms.Close();
            return strResult;
        }
        #endregion

        #region 对称解密DES
        /// <summary>
        /// 对称解密
        /// </summary>
        /// <param name="strKey">密钥</param>
        /// <param name="strSrc">解密字串</param>
        /// <returns></returns>
        public static string DecriptDES(string strSrc)
        {
            string strKey = "abc";

            if (strSrc == null)
            {
                return null;
            }
            DES des = new DESCryptoServiceProvider();
            des.Key = GetDESKey(strKey);
            des.IV = des.Key;

            MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);

            byte[] bTemp = Convert.FromBase64String(strSrc);
            string strResult = null;
            try
            {
                cs.Write(bTemp, 0, bTemp.Length);
                cs.FlushFinalBlock();
                byte[] bTxt = ms.ToArray();
                strResult = Encoding.UTF8.GetString(bTxt);
            }
            catch
            {
                throw new Exception("密钥不对");
            }
            finally
            {
                cs.Close();
                ms.Close();
            }
            return strResult;
        }
        #endregion

        #region 获得DES加密所需定长64位密钥
        /// <summary>
        /// 获得DES加密所需定长64位密钥
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static byte[] GetDESKey(string str)
        {
            byte[] bKey = new byte[8];
            byte[] bTemp = Encoding.UTF8.GetBytes(str);
            for (int i = 0; i < 8; i++)
            {
                if (i <= bTemp.Length - 1)
                    bKey[i] = bTemp[i];
                else
                    bKey[i] = (byte)0;
            }
            return bKey;

        }
        #endregion


    }
}