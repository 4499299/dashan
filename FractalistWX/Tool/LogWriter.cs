﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FractalistWX.Tool
{
    public class LogWriter
    {
        private static object asyncLock = new object();

        //申明一个静态的变量，类型为类本身
        private static LogWriter _instance = null;
        //日志文件所在目录
        private static string path = "";
        //日志文件名
        private string FileName = "";
        //		 //日志信息,测试用
        //		 string logInfo = "";
        //写数据流对象
        private StreamWriter sw;
        //将类的构造函数私有化，使得这个类不可以被外界创建
        private LogWriter()
        {
        }
        //提供静态的方法，创建类的实例
        public static LogWriter GetInstance(string path)
        {
            // 把path中的'\'替换成'/'
            path = path.Replace(@"\", @"/");
            LogWriter.path = path;
            if (_instance == null)
            {
                _instance = new LogWriter();
            }
            return _instance;
        }
        //记录日志信息
        public int WriteLog(string logInfo)
        {
            //设置log文件名，log+当前时间
            FileName = "log" + DateTime.Now.ToShortDateString().Replace(@"/", @"-").Replace(@"\", @"-") + ".log";
            //            string[] pathSplit = path.Split(':');
            //			string[] Drives = Directory.GetLogicalDrives();
            //			for(int i = 0; i< Drives.Length;i++)
            //			{
            //				if(Drives[i].Equals(pathSplit[0]))
            //				{
            //				}
            //				else
            //				{
            //				}
            //		     }
            //			string RootPath = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory().ToString());

            //如果路经不存在，生成
            if (!Directory.Exists(path))
            {

                Directory.CreateDirectory(path);
            }
            //把目录设为当前目录
            Directory.SetCurrentDirectory(path);
            //如果log文件不存在，生成文件
            if (!File.Exists(FileName))
            {
                FileStream fsLog = File.Create(FileName);
                fsLog.Close();

            }
            try
            {
                //上锁
                lock (asyncLock)
                {
                    //设置文件属性
                    File.SetAttributes(FileName, FileAttributes.Normal);
                    sw = File.AppendText(FileName);
                    logInfo = logInfo.Trim();
                    sw.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "||" + logInfo);
                    sw.Flush();
                    sw.Close();
                    File.SetAttributes(FileName, FileAttributes.ReadOnly);
                }
                return 0;
            }
            catch (Exception ex)
            {

                string errMessage = "日志错误：" + "||" + logInfo.Trim() + "||" + path + "||" + FileName + "||" + ex.Message;
                System.Diagnostics.EventLog.WriteEntry("LogWriter", errMessage, System.Diagnostics.EventLogEntryType.Error);
                return -1;
            }
        }
        //		public void testlog()
        //		{
        //			while(true)
        //			{
        //				Thread.Sleep(1000);
        //				logInfo = Thread.CurrentThread.Name.ToString();
        //				WriteLog();
        //			}
        //		}	
    }
}