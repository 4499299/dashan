﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FractalistWX.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>大山云南山泉官网管理后台</title>
    <link href="css/login.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <meta http-equiv="X-Frame-Options" content="deny">
    <meta http-equiv="windows-Target" contect="_top">
    <style>
        .formdiv {
            width: 310px;
            height: 330px;
            margin-top: 141px;
            margin-left: 66px;
            float: left;
            text-align: left;
        }

            .formdiv p {
                width: 99%;
                height: 62px;
                line-height: 57px;
            }

            .formdiv input {
                height: 38px;
                margin-top: 6px;
                font-size: 20px;
            }

            .formdiv #CheckBox1 {
                width: 32px;
                margin-top: 8px;
                background-image: url(images/sel.png);
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; height: 676px; background: url(images/bj.jpg); background-repeat: repeat-x;">
            <div style="margin: 0 auto; width: 1157px; height: 676px; background: url(images/login_bj.jpg); background-repeat: no-repeat; text-align: center;">
                <div style="padding-top: 71px;">
                    <img src="images/login_txt.png">
                </div>
                <div style="width: 1157px; height: 320px;">
                    <div style="width: 591px; height: 173px; margin-top: 169px; float: left;">
                        <img src="images/logo.png">
                    </div>
                    <div class="formdiv">
                        <p>
                            <asp:TextBox ID="TextBox_UserName" runat="server" Width="293px" autocomplete="off"></asp:TextBox>
                        </p>
                        <p>
                            <asp:TextBox ID="TextBox_Password" TextMode="Password" runat="server" Width="293px" autocomplete="off"></asp:TextBox>
                        </p>
                        <p>
                            <asp:TextBox ID="validateCode" runat="server" Width="160px"></asp:TextBox>
                            <span style="margin-left: 17px;">
                                <script type="text/javascript">/*<![CDATA[*/
                                    document.write('<a href="#" onclick="document.getElementById(\'ImgPic\').src=\'ValidateImg.aspx?temp=\'+ (new Date().getTime().toString(36)); return false"><img id="ImgPic" border="0" alt="如看不到图片，请点击刷新" src="ValidateImg.aspx?temp=' + (new Date().getTime().toString(36)) + '" width="36%" /></a>');
                                    /*]]>*/</script>
                            </span>
                        </p>
                        <p>
                            <img src="images/sel.png" id="showsel" style="margin-top: 20px;" alt="" />
                        </p>
                        <p>
                            <asp:ImageButton ID="BTN_Login" runat="server" ImageUrl="images/loginbtn.png" OnClick="BTN_Login_Click" />
                            <span style="display:none">debug</span>
                        </p>
                        <p style="margin-top: -30px; _margin-top: -15px">
                            <asp:Label ID="L1" runat="server" ForeColor="Red" Font-Size="12px" Text=""></asp:Label>
                        </p>
                    </div>
                    <div id="checkdiv" style="display: none;">
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="" Font-Size="12px" /></div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
<script language="JavaScript">
    $("#showsel").bind("click", function () {
        if ($(this).attr("src") == "images/sel.png") {
            $("#<%=CheckBox1.ClientID %>").attr("checked", false);
            $(this).attr("src", "images/nosel.png");
        }
        else {
            $("#<%=CheckBox1.ClientID %>").attr("checked", true);
            $(this).attr("src", "images/sel.png");
        }
    })
    function correctPNG() {
        var arVersion = navigator.appVersion.split("MSIE")
        var version = parseFloat(arVersion[1])
        if ((version >= 5.5) && (document.body.filters)) {
            for (var j = 0; j < document.images.length; j++) {
                var img = document.images[j]
                var imgName = img.src.toUpperCase()
                if (imgName.substring(imgName.length - 3, imgName.length) == "PNG") {
                    var imgID = (img.id) ? "id='" + img.id + "' " : ""
                    var imgClass = (img.className) ? "class='" + img.className + "' " : ""
                    var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
                    var imgStyle = "display:inline-block;" + img.style.cssText
                    if (img.align == "left") imgStyle = "float:left;" + imgStyle
                    if (img.align == "right") imgStyle = "float:right;" + imgStyle
                    if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
                    var strNewHTML = "<span " + imgID + imgClass + imgTitle
             + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
             + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
             + "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"
                    img.outerHTML = strNewHTML
                    j = j - 1
                }
            }
        }
    }
</script>
<script>
    (function (window) {
        if (window.location !== window.top.location)
            window.top.location = window.location;
    })(this);
    if (this.top.location !== this.location && (this.top.location = this.location)) {
        window.top.location = window.location;
    }
</script>
<style>
    html {
        visibility: hidden;
    }
</style>
<script>
    if (self == top) {
        document.documentElement.style.visibility = 'visible';
    } else {
        top.location = self.location;
    }
</script>
