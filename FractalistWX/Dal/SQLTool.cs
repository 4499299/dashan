using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;

using FractalistWX.Tool;
using System.Configuration;
using System.Data.SqlClient;

namespace FractalistWX.Dal
{
    public class SQLTool
    {
        static LogWriter log = LogWriter.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/") + "log");
        public static string ConnString = ConfigurationManager.ConnectionStrings["ConnectionStrings1"].ToString();
        public static string QueryPageSql(string SelectFields, string TblName, string FldName, int PageSize, int PageIndex, string OrderType, string whereStr)
        {
            PageIndex = PageIndex + 1;
            string StrTemp = "";
            string StrSql = "";
            string StrOrder = "";
            string BieTable = "Tbltemp";
            if(OrderType.IndexOf(".")>0)
            { 
            BieTable=OrderType.Substring(0,OrderType.IndexOf("."));
            }
            string ordername = FldName.Substring(FldName.LastIndexOf(".")+1);       
            //根据排序方式生成相关代码
            if (OrderType == "asc")
            {
                StrTemp = "> (Select Max(" + ordername + ")";
                StrOrder = " Order By " + FldName + " asc";
            }
            else
            {
                StrTemp = "< (Select Min(" + ordername + ")";
                StrOrder = " Order By " + FldName + " desc";
            }

            //若是第1页则无须复杂的语句
            if (PageIndex == 1)
            {
                StrTemp = "";
                if (whereStr != "")
                    StrTemp = " Where " + whereStr;
                StrSql = "Select Top " + PageSize + " " + SelectFields + " From " + TblName + "" + StrTemp + StrOrder;
            }
            else
            {
                //若不是第1页，构造sql语句
                StrSql = "Select Top " + PageSize + " " + SelectFields + " From " + TblName + " WHERE " + FldName + "" + StrTemp + " From (Select Top " + (PageIndex - 1) * PageSize + " " + FldName + " From " + TblName + "";
                if (whereStr != "")
                    StrSql = StrSql + " Where " + whereStr;
                StrSql = StrSql + StrOrder + ") As " + BieTable + ")";
                if (whereStr != "")
                    StrSql = StrSql + " And " + whereStr;
                StrSql = StrSql + StrOrder;
            }
            //返回sql语句
            return StrSql;
        }

        public static string QueryPageOrderSql(string SelectFields, string TblName, string FldName, string OrderFldName, int PageSize, int PageIndex, string OrderType, string whereStr)
        {
            PageIndex = PageIndex + 1;
            string StrTemp = "";
            string StrTempNotIn = "";
            string StrSql = "";
            string StrOrder = "";
            //根据排序方式生成相关代码
            if (OrderType == "asc")
            {
                StrOrder = " Order By [" + OrderFldName + "] asc";
            }
            else
            {
                StrOrder = " Order By [" + OrderFldName + "] desc";
            }

            //若是第1页则无须复杂的语句
            if (PageIndex == 1)
            {
                StrTemp = "";
                if (whereStr != "")
                    StrTemp = " Where " + whereStr;
                StrSql = "Select Top " + PageSize + " " + SelectFields + " From [" + TblName + "]" + StrTemp + StrOrder;
            }
            else
            {
                StrTemp = " Where " + whereStr;
                StrTempNotIn = StrTemp + " and";
                StrSql = "select top " + PageSize + " " + SelectFields + " from [" + TblName + "]" + StrTempNotIn + " " + FldName + " not in (select top  " + (PageIndex - 1) * PageSize + " " + FldName + " from [" + TblName + "]" + StrTemp + StrOrder + ")" + StrOrder;
            }

            return StrSql;
        }

        //获取首列内容
        public static string GetFirstField(string sql)
        {
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql))
            {
                try
                {
                    if (rdr.Read())
                        return rdr[0].ToString();
                }
                catch (Exception ex)
                {
                    log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                    return "";
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }

            }
            return "";
        }

        //获取多列内容
        public static List<string> GetListField(string sql, int rCount)
        {
            List<string> list = new List<string>();
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql))
            {
                try
                {
                    if (rdr.Read())
                    {
                        for (int i = 0; i < rCount; i++)
                        {
                            list.Add(rdr[i].ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }

            }
            return list;
        }      

        //获取首列内容(string)
        public static string GetFirststringField(string sql)
        {
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql))
            {
                try
                {
                    if (rdr.Read())
                        return rdr.GetString(0);
                }
                catch
                {
                    //log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                    return "";
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }

            }
            return "";
        }
        //获取首列内容(int)
        public static int GetFirstIntField(string sql)
        {
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql))
            {
                try
                {
                    if (rdr.Read())
                        return Convert.ToInt32(rdr[0]);
                }
                catch
                {
                    //log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                    return 0;
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }
            }
            return 0;
        }

        //获取首列内容(int)
        public static int GetFirstIntField(string sql, MySqlParameter[] pars)
        {
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql,pars))
            {
                try
                {
                    if (rdr.Read())
                        return Convert.ToInt32(rdr[0]);
                }
                catch
                {
                    //log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                    return 0;
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }
            }
            return 0;
        }

        //获取首列内容(bool)
        public static bool GetFirstboolField(string sql)
        {
            using (MySqlDataReader rdr = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, sql))
            {
                try
                {
                    if (rdr.Read())
                        return rdr.GetBoolean(0);
                }
                catch
                {
                    //log.WriteLog(ex.Message.Trim() + ex.StackTrace.Trim() + ex.Source.Trim());
                    return false;
                }
                finally
                {
                    if (rdr != null && !rdr.IsClosed)
                        rdr.Close();
                }

            }
            return false;
        }

        //执行SQL
        public static bool ExeSql(string sql)
        {
            int i = MySQLHelper.ExecuteNonQuery(MySQLHelper.connectionString, CommandType.Text, sql);
            if (i > 0)
            {
                return true;
            }
            return false;
        }
      
        //是否存在
        public static bool IsAuth(string sql)
        {
            string re = "";
            re = GetFirstField(sql);
            if (re != "")
            {
                return true;
            }
            return false;
        }
        public static int ExecuteNonQuery(string commandText)
        {
            return MySQLHelper.ExecuteNonQuery(ConnString, CommandType.Text, commandText);
        }
        public static int ExecuteNonQuery(string commandText, MySqlParameter[] pars)
        {
            return MySQLHelper.ExecuteNonQuery(ConnString, CommandType.Text, commandText, pars);
        }
        public static MySqlDataReader ExecuteReader(string commandText)
        {
            return MySQLHelper.ExecuteReader(ConnString, CommandType.Text, commandText);
        }

        public static MySqlDataReader ExecuteReader(string commandText, params MySqlParameter[] pars)
        {
            MySqlDataReader dtsReturn=MySQLHelper.ExecuteReader(ConnString, CommandType.Text, commandText, pars);
            return dtsReturn;
        }
        public static DataTable GetDataTable(string commandText, params MySqlParameter[] pars)
        {
            DataTable dtsReturn = MySQLHelper.GetDataTable(ConnString, CommandType.Text, commandText, pars);
            return dtsReturn;
        }
        public static MySqlDataReader GetList(string commandString, string firstRowNumber, string lastRowNumber, string sortField, string sortType, out int rowCount)
        {
            int pagesize = Convert.ToInt32(lastRowNumber) - Convert.ToInt32(firstRowNumber) + 1;
            int pageindex = Convert.ToInt32(lastRowNumber) / pagesize;

            rowCount = 0;
            string sql = "select count(*) from (" + commandString + ") T;";
            StringBuilder stbCommand = new StringBuilder();
            stbCommand.Append("select * from (select T.* from (" + commandString + ") T) A where RowNumber > " + pagesize + "*(" + pageindex + "-1) order by A." + sortField + " " + sortType + " ,A.Id asc limit " + pagesize);
            object obj = MySQLHelper.ExecuteScalar(MySQLHelper.connectionString, CommandType.Text, sql);
            if (obj != null && obj.ToString() != "")
            {
                rowCount = Convert.ToInt32(obj);
            }
            MySqlDataReader dtsReturn = MySQLHelper.ExecuteReader(MySQLHelper.connectionString, CommandType.Text, stbCommand.ToString());
            return dtsReturn;
        }
    }
}
