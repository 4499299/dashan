﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Configuration;

using FractalistWX.Tool;
using FractalistWX.Dal;
using Newtonsoft.Json;

namespace FractalistWX.Dal
{
    public class Bll
    {
        static LogWriter log = LogWriter.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/") + "log");
        private static readonly RegexOptions regexOptionsI = RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase;
       

        public static string GetHtmlInfo(string htmlStr)
        {
            string re = "";
            re = Regex.Replace(htmlStr, @"<[^>]*?>", "", regexOptionsI);
            if (re.Length > 50)
            {
                re = re.Substring(0, 50)+"...";
            }
            return re;
        }

       
    }
}